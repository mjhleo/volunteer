package com.netmarch.volunteer.web.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.netmarch.volunteer.domain.command.LoginLogCommand;
import com.netmarch.volunteer.domain.LoginLog;
import com.netmarch.volunteer.service.LoginLogService;

import io.swagger.annotations.Api;

/**
 * @author zhuguanming
 * @date 2020/12/7
 */
@RestController
@Api(tags = "登陆日志")
@RequestMapping("/api/loginLogs")
public class LoginLogResource {

    private LoginLogService loginLogService;

    @GetMapping
    public ResponseEntity<PageInfo<LoginLog>> getLoginLogs(@RequestHeader("Authorization") String token, LoginLogCommand loginLogCommand) {
        return ResponseEntity.ok(loginLogService.getLoginLogs(loginLogCommand));
    }

    @Autowired
    public void setLoginLogService(LoginLogService loginLogService) {
        this.loginLogService = loginLogService;
    }
}
