package com.netmarch.volunteer.web.rest;

import javax.annotation.Resource;

import com.netmarch.volunteer.domain.dto.AttendanceCountDTO;
import com.netmarch.volunteer.domain.dto.MissionCountDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.netmarch.volunteer.dao.AttendanceMapper;
import com.netmarch.volunteer.domain.Attendance;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

import java.util.Date;
import java.util.List;

/**
 * @author huangguochen
 * @create 2022/4/29 11:28
 */
@RestController
@Slf4j
@RequestMapping("/api/attendances")
@Api(tags = "考勤管理")
public class AttendanceResource {

    @Resource
    private AttendanceMapper attendanceMapper;

    @GetMapping("{id}/info")
    @ApiOperation("考情详情")
    public ResponseEntity<Attendance> info(@PathVariable Long id) {
        return ResponseEntity.ok(attendanceMapper.selectByPrimaryKey(id));
    }

    @GetMapping("/count/dept")
    @ApiOperation(value = "根据部门统计考勤数量")
    public List<AttendanceCountDTO> getCountByDept(@RequestParam(required = false) Long deptId) {
        return attendanceMapper.getCountByDept(deptId);
    }

    @GetMapping("/count/date")
    @ApiOperation(value = "根据时间统计考勤数量")
    public List<AttendanceCountDTO> getCountByDate(@RequestParam(required = false) Date startDate, @RequestParam(required = false) Date endDate) {
        return attendanceMapper.getCountByEventDate(startDate, endDate);
    }
}
