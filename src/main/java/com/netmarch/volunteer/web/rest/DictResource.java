package com.netmarch.volunteer.web.rest;

import com.github.pagehelper.PageInfo;

import com.netmarch.volunteer.aop.Action;
import com.netmarch.volunteer.dao.DictMapper;
import com.netmarch.volunteer.domain.Dict;
import com.netmarch.volunteer.domain.dto.DictDTO;
import com.netmarch.volunteer.domain.enumeration.DictType;
import com.netmarch.volunteer.mapper.DictDTOMapper;
import com.netmarch.volunteer.service.DictService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author Mr.Wei  2021/6/11 8:58 上午
 */
@RestController
@RequestMapping("/api/dicts")
@Api(value = "字典管理", tags = "字典管理")
public class DictResource {

    @Autowired
    private DictMapper dictMapper;

    @Autowired
    private DictService dictService;

    @GetMapping
    @ApiOperation("字典列表")
    public ResponseEntity<PageInfo<DictDTO>> getDoctors(
            @RequestParam(required = false) String code,
            @RequestParam(required = false) String name,
            @RequestParam(required = false) DictType type,
            @RequestParam(name = "pageNum", defaultValue = "1") int pageNum,
            @RequestParam(name = "pageSize", defaultValue = "10") int pageSize) {
        return ResponseEntity.ok(this.dictService.getDicts(code, name, type, pageNum, pageSize));
    }

    @PostMapping
    @ApiOperation("新增")
    @Action(name = "新增字典")
    public ResponseEntity<Void> insert(@Valid @RequestBody DictDTO dict) {
        this.dictService.addOrUpdate(DictDTOMapper.instance.dictDTOToDict(dict));
        return ResponseEntity.ok().build();
    }

    @PutMapping("/{id}")
    @ApiOperation("修改")
    @Action(name = "修改字典")
    public ResponseEntity<Void> update(@PathVariable Long id, @Valid @RequestBody DictDTO dict) {
        this.dictService.addOrUpdate(DictDTOMapper.instance.dictDTOToDict(dict).id(id));
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @ApiOperation("删除")
    @Action(name = "删除字典")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        this.dictMapper.deleteByPrimaryKey(id);
        return ResponseEntity.ok().build();
    }


    @PostMapping("/findByType")
    @ApiOperation("通过type查询列表")
    public List<Dict> getDictList(@RequestParam(required = false) String type) {
        return this.dictService.getDictList(type);
    }

    @Autowired
    public DictResource(DictMapper dictMapper, DictService dictService) {
        this.dictMapper = dictMapper;
        this.dictService = dictService;
    }
}
