package com.netmarch.volunteer.web.rest;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.github.pagehelper.PageInfo;
import com.netmarch.volunteer.aop.Action;
import com.netmarch.volunteer.dao.PermissionMapper;
import com.netmarch.volunteer.dao.RoleMapper;
import com.netmarch.volunteer.domain.Role;
import com.netmarch.volunteer.domain.dto.RoleDTO;
import com.netmarch.volunteer.service.RoleService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.weekend.WeekendSqls;

/**
 * @author Mr.Wei  2021/4/7 1:46 下午
 */
@RestController
@RequestMapping("/api/roles")
@Api(value = "角色管理", tags = "角色管理")
public class RoleResource {
    private RoleMapper roleMapper;
    private RoleService roleService;
    private PermissionMapper permissionMapper;

    @GetMapping
    @ApiOperation("角色列表")
    public ResponseEntity<PageInfo<Role>> getTiles(
            @RequestParam(required = false) String name,
            @RequestParam(name = "pageNum", defaultValue = "1") int pageNum,
            @RequestParam(name = "pageSize", defaultValue = "10") int pageSize) {
        return ResponseEntity.ok(this.roleService.getRoles(name, pageNum, pageSize));
    }

    @GetMapping("/{id}")
    @ApiOperation("角色详情")
    public ResponseEntity<RoleDTO> getRole(@PathVariable(value = "id") Long id) {
        return ResponseEntity.ok(this.roleService.getRole(id));
    }

    @GetMapping("/all")
    @ApiOperation("获取所有角色")
    public ResponseEntity<List<Role>> getNurseTitles() {
        return ResponseEntity.ok(this.roleMapper.selectByExample(new Example.Builder(Role.class).where(WeekendSqls.<Role>custom()).orderByDesc("createdDate").build()));
    }

    @PostMapping
    @ApiOperation("新增角色")
    @Action(name = "新增角色")
    public ResponseEntity<Role> insert(@Valid @RequestBody RoleDTO roleDto) {
        this.roleService.insert(roleDto);
        return ResponseEntity.ok().build();
    }


    @PutMapping("/{id}")
    @ApiOperation("更新角色")
    @Action(name = "更新角色")
    public ResponseEntity<Role> update(@PathVariable("id") long id, @Valid @RequestBody RoleDTO roleDto) {
        this.roleService.update(roleDto.id(id));
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @ApiOperation("删除角色")
    @Action(name = "删除角色")
    public ResponseEntity<Object> delete(@PathVariable("id") Long id) {
        this.roleService.delete(id);
        return ResponseEntity.ok().build();
    }

    @Autowired
    public RoleResource(RoleMapper roleMapper, RoleService roleService, PermissionMapper permissionMapper) {
        this.roleMapper = roleMapper;
        this.roleService = roleService;
        this.permissionMapper = permissionMapper;
    }
}
