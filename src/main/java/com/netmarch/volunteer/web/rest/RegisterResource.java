package com.netmarch.volunteer.web.rest;

import com.github.pagehelper.PageInfo;
import com.netmarch.volunteer.aop.Action;
import com.netmarch.volunteer.config.ApplicationProperties;
import com.netmarch.volunteer.dao.RegisterMapper;
import com.netmarch.volunteer.domain.Dept;
import com.netmarch.volunteer.domain.Dict;
import com.netmarch.volunteer.domain.Menu;
import com.netmarch.volunteer.domain.Register;
import com.netmarch.volunteer.domain.command.RegisterQueryCommand;
import com.netmarch.volunteer.domain.dto.*;
import com.netmarch.volunteer.domain.enumeration.DictType;
import com.netmarch.volunteer.domain.enumeration.FinishStatus;
import com.netmarch.volunteer.mapper.RegisterDTOMapper;
import com.netmarch.volunteer.service.RegisterService;
import com.netmarch.volunteer.service.util.POIUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;

@RestController
@RequestMapping("/api/register")
@Api(value = "志愿者报名审核", tags = "志愿者报名审核")
public class RegisterResource {
    @Autowired
    private RegisterService registerService;
    @Autowired
    private RegisterMapper registerMapper;
    @Autowired
    private ApplicationProperties applicationProperties;


    @PostMapping("/toRegister")
    @ApiOperation("志愿者注册")
    public ResponseEntity<Register> toRegister(@Valid @RequestBody RegisterDTO registerDTO) {
        registerService.toRegister(RegisterDTOMapper.instance.registerDTOToRegister(registerDTO));
        return ResponseEntity.ok().build();
    }

    @GetMapping
    @ApiOperation("志愿者注册列表")
    public ResponseEntity<PageInfo<Register>> getRegisters(
            RegisterQueryCommand registerQueryCommand,
            @RequestHeader("Authorization") String token,
            @RequestParam(name = "pageNum", defaultValue = "1") int pageNum,
            @RequestParam(name = "pageSize", defaultValue = "10") int pageSize) {
        if(FinishStatus.FINISHED.equals(registerQueryCommand.getFinishStatus())){
            return ResponseEntity.ok(registerService.getTrainingRegisters(registerQueryCommand, pageNum, pageSize,token));
        }
        return ResponseEntity.ok(registerService.getRegisters(registerQueryCommand, pageNum, pageSize));
    }


    @PostMapping("/check")
    @ApiOperation("志愿者注册审核")
    public ResponseEntity<Register> check(@RequestHeader("Authorization") String token,@Valid @RequestBody RegisterCheckDTO registerCheckDTO) {
        registerService.check(registerCheckDTO,token);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/batchCheck")
    @ApiOperation("志愿者注册审核多选")
    public ResponseEntity<Register> batchCheck(@RequestHeader("Authorization") String token,@Valid @RequestBody RegisterCheckDTO registerCheckDTO) {
        registerService.batchCheck(registerCheckDTO,token);
        return ResponseEntity.ok().build();
    }


    @PostMapping("/findDetialByCondition")
    @ApiOperation("根据条件查询志愿者信息")
    public ResponseEntity<Register> findDetailByCondition(@Valid @RequestBody RegisterQueryCommand registerQueryCommand) {
        return ResponseEntity.ok(registerService.findDetailByCondition(registerQueryCommand));
    }

    @GetMapping("/{id}")
    @ApiOperation("注册详情")
    public ResponseEntity<Register> getRegisterDetail(@PathVariable(value = "id") String registerId) {
        return ResponseEntity.ok(registerMapper.selectByPrimaryKey(registerId));
    }

    @DeleteMapping("/{id}")
    @Action(name = "删除注册信息")
    public ResponseEntity<Void> delete(@PathVariable("id") Long registerId) {
        registerMapper.deleteByPrimaryKey(registerId);
        return ResponseEntity.status(HttpStatus.OK).build();
    }


    //审核通过人员才能 岗前培训，分配至对应单位
    @PostMapping("/arrange")
    @ApiOperation("分配单位")
    public ResponseEntity<Register> arrange(@RequestHeader("Authorization") String token,@Valid @RequestBody RegisterArrangeDTO registerArrangeDTO) {
        registerService.arrange(registerArrangeDTO,token);
        return ResponseEntity.ok().build();
    }

    //培训结果状态修改
    @PostMapping("/batchTrainResult")
    @ApiOperation("岗前培训状态修改")
    public ResponseEntity<Register> batchTrainResult(@RequestHeader("Authorization") String token,@Valid @RequestBody RegisterTrainCheckDTO registerTrainCheckDTO) {
        registerService.batchTrainResult(registerTrainCheckDTO,token);
        return ResponseEntity.ok().build();
    }

    //岗前培训审核
    @PostMapping("/trainCheck")
    @ApiOperation("岗前培训审核")
    public ResponseEntity<Register> trainCheck(@RequestHeader("Authorization") String token,@Valid @RequestBody RegisterTrainCheckDTO registerTrainCheckDTO) {
        registerService.trainCheck(registerTrainCheckDTO,token);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/batchTrainCheck")
    @ApiOperation("岗前培训批量审核")
    public ResponseEntity<Register> batchTrainCheck(@RequestHeader("Authorization") String token,@Valid @RequestBody RegisterTrainCheckDTO registerTrainCheckDTO) {
        registerService.batchTrainCheck(registerTrainCheckDTO,token);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/excelTrainingTemplate")
    @ApiOperation(value="获取导入模板",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE )
    ResponseEntity<byte[]> excelTrainingTemplate() throws IOException, IllegalAccessException{
        String fileName = "training_import_samples.xlsx".toString();
        ClassPathResource classPathResource = new ClassPathResource("static/" + fileName);
        Workbook workbook  = null;
        if (fileName.endsWith("xlsx")) {
            workbook = new XSSFWorkbook(classPathResource.getInputStream());
        } else if (fileName.endsWith("xls")) {
            workbook = new HSSFWorkbook(classPathResource.getInputStream());
        }
        Sheet hdSheet = workbook.getSheetAt(0);

        //第2列,单位
//        List<Dept> depts = deptMapper.selectAll();
//        List<String> deptList=depts.stream().map(Dept::getName).collect(Collectors.toList());
//        String[] deptArr = deptList.toArray(new String[deptList.size()]);
//        POIUtils.setBoxs(hdSheet,deptArr,10000,1,1);
//        hdSheet.addValidationData(POIUtils.setDataValidation(hdSheet, deptArr,true ,1, hdSheet.getPhysicalNumberOfRows(), 1 ,1));

        return POIUtils.getResponseEntity(workbook,fileName);
    }


    @PostMapping("/import")
    @ApiOperation("导入岗前培训培训信息")
    public ResponseEntity importPerson(@RequestHeader("Authorization") String token,@RequestParam("file") MultipartFile file) {
        registerService.importTrainInfor(file);
        return ResponseEntity.ok().build();
    }


}
