package com.netmarch.volunteer.web.rest;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.netmarch.volunteer.aop.Action;
import com.netmarch.volunteer.config.ApplicationProperties;
import com.netmarch.volunteer.exception.ApiException;
import com.netmarch.volunteer.exception.ErrorVM;
import com.netmarch.volunteer.service.util.FileUploadUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 */
@RestController
@RequestMapping("/api/file")
@Api(value = "文件上传", tags = "文件上传")
public class FileResource {

    @Autowired
    private ApplicationProperties applicationProperties;

    @PostMapping("/addPic")
    @Action(name = "上传文件")
    @ApiOperation("上传文件")
    public ResponseEntity<String> addPic(@RequestHeader("Authorization") String token, @RequestParam("file") MultipartFile file) {
        if (null == file) {
            throw new ApiException(new ErrorVM(-20001, "请上传相关文件!!"));
        }
        String uri = null;
        try {
            uri = FileUploadUtil.uploadFile(file, applicationProperties.getUploadFileMaxSize(), applicationProperties.getUploadFileAddress());
            uri =uri.replaceAll("\\\\","\\/");
        } catch (IOException e) {
            throw new ApiException(new ErrorVM(-20001, "照片上传文件!!"));
        }
        return ResponseEntity.ok(uri);
    }


}
