package com.netmarch.volunteer.web.rest;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.github.pagehelper.PageInfo;
import com.netmarch.volunteer.aop.Action;
import com.netmarch.volunteer.config.ApplicationProperties;
import com.netmarch.volunteer.dao.UserMapper;
import com.netmarch.volunteer.domain.User;
import com.netmarch.volunteer.domain.command.UserQueryCommand;
import com.netmarch.volunteer.domain.dto.LoginDTO;
import com.netmarch.volunteer.domain.dto.PasswordModifyDTO;
import com.netmarch.volunteer.domain.dto.UserDTO;
import com.netmarch.volunteer.domain.dto.UserInfoDTO;
import com.netmarch.volunteer.service.UserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @author Mr.Wei  2020/9/22 2:18 下午
 */
@RestController
@RequestMapping("/api/users")
@Api(value = "账号管理", tags = "账号管理")
public class UserResource {
    private UserService userService;
    private ApplicationProperties applicationProperties;
    private UserMapper userMapper;

    @GetMapping
    @ApiOperation("账号列表")
    public ResponseEntity<PageInfo<User>> getAdmins(
        UserQueryCommand userQueryCommand,
        @RequestHeader("Authorization") String token,
        @RequestParam(name = "pageNum", defaultValue = "1") int pageNum,
        @RequestParam(name = "pageSize", defaultValue = "10") int pageSize) {
        return ResponseEntity.ok(this.userService.getUsers(userQueryCommand, pageNum, pageSize));
    }

    @GetMapping("/all")
    @ApiOperation("所有工作人员")
    public ResponseEntity<List<User>> getUserList() {
        return ResponseEntity.ok(userService.getAllActiveUsers());
    }

    @GetMapping("/info")
    @ApiOperation("账号详情")
    public ResponseEntity<UserInfoDTO> getUserInfo(@RequestHeader("Authorization") String token) {
        return ResponseEntity.ok(userService.getUserInfo(token).initUrl(applicationProperties.getFilePrefix()));
    }

    @PostMapping
    @Action(name = "新增用户")
    @ApiOperation("新增账户")
    public ResponseEntity<User> insert(@RequestHeader("Authorization") String token, @Valid @RequestBody UserDTO userDTO) {
        this.userService.addOrUpdateUser(userDTO);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/{id}")
    @ApiOperation("更新账户")
    @Action(name = "更新账户")
    public ResponseEntity<User> update(@RequestHeader("Authorization") String token, @PathVariable("id") long id, @Valid @RequestBody UserDTO userDTO) {
        userService.addOrUpdateUser(userDTO.id(id));
        return ResponseEntity.ok().build();
    }

    @PutMapping("/{id}/reset")
    @Action(name = "重置账户密码")
    @ApiOperation("重置账户密码")
    public ResponseEntity<User> reset(@RequestHeader("Authorization") String token, @PathVariable("id") long id) {
        userService.reset(id);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/modifyPassword")
    @ApiOperation("当前账户修改密码")
    @Action(name = "修改密码")
    public ResponseEntity<Void> modifyPassword(@RequestHeader("Authorization") String token, @Valid @RequestBody PasswordModifyDTO modifyPasswordDTO) {
        this.userService.modifyPassword(modifyPasswordDTO, token);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/{id}/activated")
    @ApiOperation("修改用户状态（可用/不可用）")
    @Action(name = "修改用户状态")
    public ResponseEntity<Object> activated(@RequestHeader("Authorization") String token, @PathVariable("id") Long id, @RequestParam("activated") Boolean activated) {
        this.userMapper.updateByPrimaryKeySelective(new User(id, activated));
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @ApiOperation("删除账户")
    @Action(name = "删除账户")
    public ResponseEntity<Object> delete(@RequestHeader("Authorization") String token, @PathVariable("id") Long id) {
        this.userMapper.deleteByPrimaryKey(id);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/login")
    @ApiOperation("用户登陆")
    public ResponseEntity<String> login(@RequestBody LoginDTO loginDTO) {
        return ResponseEntity.ok(this.userService.login(loginDTO, this.applicationProperties.getJwtSecret(), this.applicationProperties.getTokenExpire()));
    }

    @PostMapping("/logout")
    @ApiOperation("用户登出")
    @Action(name = "用户登出")
    public ResponseEntity<Void> logout(@RequestHeader("Authorization") String token) {
        this.userService.logout(token);
        return ResponseEntity.ok().build();
    }

    @Autowired
    public UserResource(UserService userService, ApplicationProperties applicationProperties, UserMapper userMapper) {
        this.userService = userService;
        this.applicationProperties = applicationProperties;
        this.userMapper = userMapper;
    }
}
