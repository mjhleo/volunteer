package com.netmarch.volunteer.web.rest;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.netmarch.volunteer.dao.EntityLogMapper;
import com.netmarch.volunteer.domain.EntityLog;
import com.netmarch.volunteer.domain.enumeration.EntityAction;
import com.netmarch.volunteer.service.EntityLogService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * <操作日志>
 *
 * @author Huangguochen
 * @create 2020/11/24 20:05
 */
@RestController
@RequestMapping("/api/entityLogs")
@Api(value = "数据库日志接口", tags = "数据库日志接口")
public class EntityLogResource {
    private EntityLogService entityLogService;
    private EntityLogMapper entityLogMapper;

    @GetMapping
    @ApiOperation("数据库日志查询")
    public ResponseEntity<PageInfo<EntityLog>> getLogs(
        @RequestHeader("Authorization") String token,
        @RequestParam(name = "action", required = false) EntityAction action,
        @RequestParam(name = "startDate", required = false) Date startDate,
        @RequestParam(name = "endDate", required = false) Date endDate,
        @RequestParam(name = "pageNum", defaultValue = "1") int pageNum,
        @RequestParam(name = "pageSize", defaultValue = "10") int pageSize) {
        return ResponseEntity.ok(this.entityLogService.getEntityLogs(action, startDate, endDate, pageNum, pageSize));
    }

    @GetMapping("/{id}")
    @ApiOperation("数据库日志详情")
    public ResponseEntity<EntityLog> getLog(@RequestHeader("Authorization") String token, @PathVariable("id") long id) {
        return ResponseEntity.ok(this.entityLogMapper.selectByPrimaryKey(id));
    }

    @Autowired
    public EntityLogResource(EntityLogService entityLogService, EntityLogMapper entityLogMapper) {
        this.entityLogService = entityLogService;
        this.entityLogMapper = entityLogMapper;
    }
}
