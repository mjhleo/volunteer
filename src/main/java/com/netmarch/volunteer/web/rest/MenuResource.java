package com.netmarch.volunteer.web.rest;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.netmarch.volunteer.aop.Action;
import com.netmarch.volunteer.dao.MenuMapper;
import com.netmarch.volunteer.domain.Menu;
import com.netmarch.volunteer.domain.dto.MenuDTO;
import com.netmarch.volunteer.domain.enumeration.MenuType;
import com.netmarch.volunteer.service.MenuService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * <菜单>
 *
 * @author Huangguochen
 * @create 2020/12/4 14:01
 */
@RestController
@RequestMapping("/api/menus")
@Api(tags = "菜单管理")
public class MenuResource {


    private MenuService menuService;
    private MenuMapper menuMapper;

    @GetMapping("/all")
    @ApiOperation("所有菜单")
    public ResponseEntity<List<MenuDTO>> getMenus() {
        return ResponseEntity.ok(menuService.getMenus(null, MenuType.SYSTEM));
    }

    @GetMapping("/admin")
    @ApiOperation("当前用户菜单")
    public ResponseEntity<List<MenuDTO>> getMenus(@RequestHeader("Authorization") String token) {
        return ResponseEntity.ok(menuService.getMenus(token, MenuType.SYSTEM));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Menu> getMenu(@PathVariable(value = "id") String menuId) {
        return ResponseEntity.ok(menuMapper.selectByPrimaryKey(menuId));
    }

    @PostMapping
    @Action(name = "新增菜单")
    public ResponseEntity<Void> insert(@Valid @RequestBody Menu menu) {
        menuMapper.insert(menu);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @PutMapping("/{id}")
    @Action(name = "修改菜单")
    public ResponseEntity<Void> update(@PathVariable("id") Long menuId, @Valid @RequestBody Menu menu) {
        menuMapper.updateByPrimaryKeySelective(menu.id(menuId));
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @DeleteMapping("/{id}")
    @Action(name = "删除菜单")
    public ResponseEntity<Void> delete(@PathVariable("id") Long systemId) {
        menuMapper.deleteByPrimaryKey(systemId);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @Autowired
    public void setMenuService(MenuService menuService) {
        this.menuService = menuService;
    }

    @Autowired
    public void setMenuMapper(MenuMapper menuMapper) {
        this.menuMapper = menuMapper;
    }
}
