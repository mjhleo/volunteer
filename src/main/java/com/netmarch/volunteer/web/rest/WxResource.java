package com.netmarch.volunteer.web.rest;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.github.pagehelper.PageInfo;
import com.netmarch.volunteer.domain.SystemMessage;
import com.netmarch.volunteer.domain.UserVolunteer;
import com.netmarch.volunteer.domain.command.MissionCommand;
import com.netmarch.volunteer.domain.command.SystemMessageQueryCommand;
import com.netmarch.volunteer.domain.dto.AttendanceEndDTO;
import com.netmarch.volunteer.domain.dto.AttendanceStartDTO;
import com.netmarch.volunteer.domain.dto.MenuDTO;
import com.netmarch.volunteer.domain.dto.MissionQryDto;
import com.netmarch.volunteer.service.AttendanceService;
import com.netmarch.volunteer.service.MissionService;
import com.netmarch.volunteer.service.SystemMessageService;
import com.netmarch.volunteer.service.WxService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;

/**
 * <微信相关接口>
 *
 * @author huangguochen
 * @create 2022/4/27 13:58
 */

@RestController()
@RequestMapping("/api/wx")
@Api(tags = "微信相关接口")
@RequiredArgsConstructor
public class WxResource {

    private final WxService wxService;
    private final SystemMessageService systemMessageService;
    private final AttendanceService attendanceService;
    private final MissionService missionService;


    @GetMapping("/login")
    @ApiOperation("微信登录")
    ResponseEntity<String> login(@RequestParam String token){
        return ResponseEntity.ok(wxService.login(token));
    }

    @GetMapping("/menus")
    @ApiOperation("菜单权限")
    ResponseEntity<List<MenuDTO>> menus(@RequestHeader("Authorization") String token){
        return ResponseEntity.ok(wxService.getMenus(token));
    }

    @GetMapping("/info")
    @ApiOperation("账号详情")
    public ResponseEntity<UserVolunteer> getUserInfo(@RequestHeader("Authorization") String token) {
        return ResponseEntity.ok(wxService.getUserVolunteerInfo(token));
    }

    @GetMapping("/notice")
    @ApiOperation("通知")
    public ResponseEntity<PageInfo<SystemMessage>> getSystemMessagesCurAdmin(SystemMessageQueryCommand systemMessageQueryCommand,
                                                                             @RequestParam(name = "pageNum", defaultValue = "1") int pageNum,
                                                                             @RequestParam(name = "pageSize", defaultValue = "10") int pageSize) {
        return ResponseEntity.ok(systemMessageService.getSystemMessages(systemMessageQueryCommand, pageNum, pageSize));
    }

    @GetMapping("/missions")
    @ApiOperation("任务工单")
    public ResponseEntity<PageInfo<MissionQryDto>> getList(
            MissionCommand missionCommand,
            @RequestParam(name = "pageNum", defaultValue = "1") int pageNum,
            @RequestParam(name = "pageSize", defaultValue = "10") int pageSize) {
        return ResponseEntity.ok(missionService.getList(missionCommand, pageNum, pageSize));
    }

    @PostMapping("/attendance/startMission")
    @ApiOperation("考勤管理-开始任务")
    public ResponseEntity<Void> start(@Valid @RequestBody AttendanceStartDTO dto, @RequestHeader("Authorization") String token) {
        attendanceService.addAttendance(dto, token);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/attendance/endMission")
    @ApiOperation("考勤管理-结束任务")
    public ResponseEntity<Void> end(@Valid @RequestBody AttendanceEndDTO dto) {
        attendanceService.finishAttendance(dto);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/address/{lat}/{lng}")
    @ApiOperation("根据经纬度解析地址信息")
    public ResponseEntity<String> tencentGetLonAndLat(@PathVariable String lat, @PathVariable String lng) {
        return ResponseEntity.ok(wxService.tencentGetLonAndLat(lat, lng));
    }


}