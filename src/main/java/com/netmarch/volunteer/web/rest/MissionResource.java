package com.netmarch.volunteer.web.rest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import com.netmarch.volunteer.domain.command.MissionEventCommand;
import com.netmarch.volunteer.domain.dto.MissionCountDtoByEvent;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.github.pagehelper.PageInfo;
import com.netmarch.volunteer.dao.DeptMapper;
import com.netmarch.volunteer.domain.Dept;
import com.netmarch.volunteer.domain.Dict;
import com.netmarch.volunteer.domain.command.MissionCommand;
import com.netmarch.volunteer.domain.dto.MissionCountDTO;
import com.netmarch.volunteer.domain.dto.MissionQryDto;
import com.netmarch.volunteer.domain.enumeration.DictType;
import com.netmarch.volunteer.service.DictService;
import com.netmarch.volunteer.service.MissionService;
import com.netmarch.volunteer.service.util.POIUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


@RestController
@RequestMapping("/api/mission")
@Api(value = "任务管理", tags = "任务管理")
public class MissionResource {

    @Autowired
    private DeptMapper deptMapper;

    @Autowired
    private DictService dictService;

    @Autowired
    private MissionService missionService;

    @GetMapping("/count/dept")
    @ApiOperation(value = "根据部门统计任务数量")
    public List<MissionCountDTO> getCountByDept(@RequestParam(required = false) Long deptId) {
        return missionService.getCountByDept(deptId);
    }

    @GetMapping("/count/date")
    @ApiOperation(value = "根据时间统计任务数量")
    public List<MissionCountDTO> getCountByDate(@RequestParam(required = false) Date startTime, @RequestParam(required = false) Date endTime) {
        return missionService.getCountByEventDate(startTime, endTime);
    }


    @GetMapping("/excelTemplate")
    @ApiOperation(value="获取导入模板",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE )
    ResponseEntity<byte[]> downloadExcelTemplate(Long deptId) throws IOException, IllegalAccessException{
        String fileName = "person_import_samples.xlsx".toString();
        ClassPathResource classPathResource = new ClassPathResource("static/" + fileName);
        Workbook workbook  = null;
        if (fileName.endsWith("xlsx")) {
            workbook = new XSSFWorkbook(classPathResource.getInputStream());
        } else if (fileName.endsWith("xls")) {
            workbook = new HSSFWorkbook(classPathResource.getInputStream());
        }
        Sheet hdSheet = workbook.getSheetAt(0);
        //第2列,单位
        Dept dept = deptMapper.selectByPrimaryKey(deptId);
        List<Dept> depts = new ArrayList<>();
        depts.add(dept);
        List<String> deptList=depts.stream().map(Dept::getName).collect(Collectors.toList());
        String[] deptArr = deptList.toArray(new String[deptList.size()]);
        POIUtils.setBoxs(hdSheet,deptArr,10000,1,1);
        hdSheet.addValidationData(POIUtils.setDataValidation(hdSheet, deptArr,true ,1, hdSheet.getPhysicalNumberOfRows(), 1 ,1));
        //第5列,区镇
        List<Dict> dicts = dictService.getDictList(DictType.DISTRICT_TOWN.toString());
        List<String> dictList=dicts.stream().map(Dict::getName).collect(Collectors.toList());
        String[] dictArr = dictList.toArray(new String[dictList.size()]);
        POIUtils.setBoxs(hdSheet,dictArr,10000,4,4);
        hdSheet.addValidationData(POIUtils.setDataValidation(hdSheet, dictArr,true ,1, hdSheet.getPhysicalNumberOfRows(), 4 ,4));
        return POIUtils.getResponseEntity(workbook,fileName);
    }

    @PostMapping("/import")
    @ApiOperation("导入人员")
    public ResponseEntity importPerson(@RequestHeader("Authorization") String token, @RequestParam(required = false) Boolean validated, @RequestParam(required = true) Long eventId,@RequestParam(required = true)Long deptId ,MultipartFile file) {
        missionService.importPerson(token, Objects.isNull(validated) ? Boolean.TRUE : validated, eventId,deptId,file);
        return ResponseEntity.ok().build();
    }

    @GetMapping
    @ApiOperation("任务列表")
    public ResponseEntity<PageInfo<MissionQryDto>> getList(
             MissionCommand missionCommand,
            @RequestParam(name = "pageNum", defaultValue = "1") int pageNum,
            @RequestParam(name = "pageSize", defaultValue = "10") int pageSize) {
        return ResponseEntity.ok(missionService.getList(missionCommand, pageNum, pageSize));
    }

    @GetMapping("/getMissionCount")
    @ApiOperation("根据事件获取各单位数据")
    public ResponseEntity<PageInfo<MissionCountDtoByEvent>> getMissionCountByEvent(
            MissionEventCommand command,
            @RequestParam(name = "pageNum", defaultValue = "1") int pageNum,
            @RequestParam(name = "pageSize", defaultValue = "10") int pageSize) {
        return ResponseEntity.ok(missionService.getMissionCountByEvent(command, pageNum, pageSize));
    }


}
