package com.netmarch.volunteer.web.rest;

import com.github.pagehelper.PageInfo;
import com.netmarch.volunteer.config.ApplicationProperties;
import com.netmarch.volunteer.dao.PersonnelMapper;
import com.netmarch.volunteer.domain.Personnel;
import com.netmarch.volunteer.domain.command.PersonnelQueryCommand;
import com.netmarch.volunteer.domain.dto.PersonnelDTO;
import com.netmarch.volunteer.service.PersonnelService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/personnel")
@Api(value = "人员管理", tags = "人员管理")
public class PersonnelResource {
    @Autowired
    private PersonnelService personnelService;

    @Autowired
    private PersonnelMapper personnelMapper;

    @Autowired
    private ApplicationProperties applicationProperties;


    @GetMapping
    @ApiOperation("人员管理列表")
    public ResponseEntity<PageInfo<Personnel>> getPersonnelList(
            PersonnelQueryCommand personnelQueryCommand,
            @RequestHeader("Authorization") String token,
            @RequestParam(name = "pageNum", defaultValue = "1") int pageNum,
            @RequestParam(name = "pageSize", defaultValue = "10") int pageSize) {

        return ResponseEntity.ok(personnelService.getPersonnelList(personnelQueryCommand, pageNum, pageSize,token));
    }


    @PostMapping("/toExitJob")
    @ApiOperation("退岗操作")
    public ResponseEntity<Personnel> toExitJob(@RequestHeader("Authorization") String token, @Valid @RequestBody PersonnelDTO personnelDTO) {
        personnelService.toExitJob(personnelDTO,token);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/toOnJob")
    @ApiOperation("复岗操作")
    public ResponseEntity<Personnel> toOnJob(@RequestHeader("Authorization") String token, @Valid @RequestBody PersonnelDTO personnelDTO) {
        personnelService.toOnJob(personnelDTO,token);
        return ResponseEntity.ok().build();
    }

    //在职人员分配
    @PostMapping("/arrange")
    @ApiOperation("在职人员分配单位")
    public ResponseEntity<Personnel> arrange(@RequestHeader("Authorization") String token,@Valid @RequestBody PersonnelDTO personnelDTO) {
        personnelService.arrange(personnelDTO,token);
        return ResponseEntity.ok().build();
    }

}
