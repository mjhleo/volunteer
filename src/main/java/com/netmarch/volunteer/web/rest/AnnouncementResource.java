package com.netmarch.volunteer.web.rest;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.github.pagehelper.PageInfo;
import com.netmarch.volunteer.aop.Action;
import com.netmarch.volunteer.dao.AnnouncementMapper;
import com.netmarch.volunteer.domain.Announcement;
import com.netmarch.volunteer.domain.command.AnnouncementQueryCommand;
import com.netmarch.volunteer.domain.dto.AnnouncementDTO;
import com.netmarch.volunteer.service.AnnouncementService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;


@RestController
@RequestMapping("/api/announcements")
@Api(tags = "音视频管理")
@RequiredArgsConstructor
public class AnnouncementResource {

    private final AnnouncementMapper announcementMapper;
    private final AnnouncementService announcementService;

    @GetMapping
    @ApiOperation("列表")
    public ResponseEntity<PageInfo<Announcement>> getAnnouncements(
            AnnouncementQueryCommand announcementQueryCommand,
            @RequestParam(name = "pageNum", defaultValue = "1") int pageNum,
            @RequestParam(name = "pageSize", defaultValue = "10") int pageSize) {
        return ResponseEntity.ok(this.announcementService.getAnnouncements(announcementQueryCommand, pageNum, pageSize));
    }

    @PostMapping
    @ApiOperation("新增")
    @Action(name = "新增公告")
    public ResponseEntity<Void> insert(@Valid @RequestBody AnnouncementDTO dto) {
        this.announcementService.addOrUpdate(dto);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/{id}")
    @ApiOperation("修改")
    @Action(name = "修改公告")
    public ResponseEntity<Void> update(@PathVariable Long id, @Valid @RequestBody AnnouncementDTO dto) {
        this.announcementService.addOrUpdate(dto.id(id));
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @ApiOperation("删除")
    @Action(name = "删除公告")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        this.announcementService.delete(id);
        return ResponseEntity.ok().build();
    }

}
