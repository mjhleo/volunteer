package com.netmarch.volunteer.web.rest;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;

import javax.imageio.ImageIO;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.netmarch.volunteer.config.ApplicationProperties;
import com.netmarch.volunteer.config.Constants;
import com.netmarch.volunteer.exception.ApiException;
import com.netmarch.volunteer.service.util.QRCodeUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Huangguochen
 */
@RestController
@Slf4j
@RequestMapping("/api/app")
@Api(tags = "app下载管理")
public class AppResource {
    private ApplicationProperties applicationProperties;

    @GetMapping("/qrcode")
    @ApiOperation("app下载二维码")
    public ResponseEntity<String> code() {
        String apkUrl = getApkUrl();
        BufferedImage qrCode = QRCodeUtil.getQrCode(apkUrl, 200);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            assert qrCode != null;
            ImageIO.write(qrCode, "png", out);
        } catch (IOException e) {
            log.error("generate code error! error message:{}", ExceptionUtils.getStackTrace(e));
            throw new ApiException(ApiException.E_API_GENERATE_CODE_ERROR);
        }
        byte[] bytes = out.toByteArray();
        Base64.Encoder encoder = Base64.getEncoder();
        return ResponseEntity.ok(Constants.IMAGE_BASE64_HEAD + encoder.encodeToString(bytes).trim());
    }

    @GetMapping("/download")
    @ApiOperation("app下载链接")
    public ResponseEntity<String> download() {
        return ResponseEntity.ok(getApkUrl());
    }

    public String getApkUrl() {
        return applicationProperties.getApkDownloadAddress() + "volunteer.apk";
    }

    @Autowired
    public void setApplicationProperties(ApplicationProperties applicationProperties) {
        this.applicationProperties = applicationProperties;
    }
}
