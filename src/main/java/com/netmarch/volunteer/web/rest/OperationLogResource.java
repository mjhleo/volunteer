package com.netmarch.volunteer.web.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.netmarch.volunteer.domain.command.OperationLogCommand;
import com.netmarch.volunteer.domain.OperationLog;
import com.netmarch.volunteer.service.OperationLogService;

import io.swagger.annotations.Api;

/**
 * @author zhuguanming
 * @date 2020/12/8
 */
@RestController
@Api(tags = "操作日志")
@RequestMapping("/api/operationLogs")
public class OperationLogResource {

    private OperationLogService operationLogService;

    @GetMapping
    public ResponseEntity<PageInfo<OperationLog>> getLoginLogs(@RequestHeader("Authorization") String token, OperationLogCommand command) {
        return ResponseEntity.ok(operationLogService.getOperationLogs(command));
    }

    @Autowired
    public void setLoginLogService(OperationLogService operationLogService) {
        this.operationLogService = operationLogService;
    }
}
