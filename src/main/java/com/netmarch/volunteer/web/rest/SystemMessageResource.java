package com.netmarch.volunteer.web.rest;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.netmarch.volunteer.aop.Action;
import com.netmarch.volunteer.dao.SystemMessageMapper;
import com.netmarch.volunteer.domain.SystemMessage;
import com.netmarch.volunteer.domain.enumeration.SystemMessageState;
import com.netmarch.volunteer.service.SystemMessageService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;

/**
 * @author huangguochen
 * @create 2022/4/28 13:54
 */
@RestController
@Api(tags = "系统消息")
@RequestMapping("/api/systemMessages")
@RequiredArgsConstructor
public class SystemMessageResource {

    private final SystemMessageService systemMessageService;
    private final SystemMessageMapper systemMessageMapper;


    @GetMapping("/{id}")
    @ApiOperation("系统消息详情")
    public ResponseEntity<SystemMessage> getSystemMessage(@PathVariable(value = "id") Long systemMessageId){
        return ResponseEntity.ok(systemMessageMapper.selectByPrimaryKey(systemMessageId));
    }


    @PostMapping("/{id}/{messageState}")
    @ApiOperation("更新消息状态(已读)")
    @Action(name = "查阅系统消息")
    public ResponseEntity<Void> changeMessageState(@PathVariable("id") Long id, @PathVariable("messageState") SystemMessageState messageState) {
        systemMessageMapper.updateByPrimaryKeySelective(new SystemMessage(id, messageState));
        return ResponseEntity.ok().build();
    }


    @GetMapping("/unReadMessageNum")
    @ApiOperation("获取未读消息数量")
    public ResponseEntity<Integer> unReadMessageNum(@RequestHeader("Authorization") String token) {
        return ResponseEntity.ok(systemMessageService.unReadMessageNum(token));
    }

}
