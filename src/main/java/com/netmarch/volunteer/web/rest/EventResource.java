package com.netmarch.volunteer.web.rest;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.github.pagehelper.PageInfo;
import com.netmarch.volunteer.aop.Action;
import com.netmarch.volunteer.dao.EventMapper;
import com.netmarch.volunteer.domain.Event;
import com.netmarch.volunteer.domain.command.EventQueryCommand;
import com.netmarch.volunteer.domain.dto.EventDTO;
import com.netmarch.volunteer.service.EventService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;

/**
 * @author huangguochen
 * @create 2022/4/27 22:09
 */
@RestController
@RequestMapping("/api/events")
@Api(value = "事件管理", tags = "事件管理")
@RequiredArgsConstructor
public class EventResource {

    private final EventMapper eventMapper;
    private final EventService eventService;

    @GetMapping
    @ApiOperation("事件列表")
    public ResponseEntity<PageInfo<Event>> getEvents(
            EventQueryCommand eventQueryCommand,
            @RequestParam(name = "pageNum", defaultValue = "1") int pageNum,
            @RequestParam(name = "pageSize", defaultValue = "10") int pageSize) {
        return ResponseEntity.ok(this.eventService.getEvents(eventQueryCommand, pageNum, pageSize));
    }

    @PostMapping
    @ApiOperation("新增")
    @Action(name = "新增事件")
    public ResponseEntity<Void> insert(@Valid @RequestBody EventDTO dto) {
        this.eventService.addOrUpdate(dto);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/{id}")
    @ApiOperation("根据id查询")
    public ResponseEntity<EventDTO> getOne(@PathVariable("id") Long id) {
        return ResponseEntity.ok(this.eventService.getEventDTO(id));
    }

    @PutMapping("/{id}")
    @ApiOperation("修改")
    @Action(name = "修改事件")
    public ResponseEntity<Void> update(@PathVariable Long id, @Valid @RequestBody EventDTO dto) {
        this.eventService.addOrUpdate(dto.id(id));
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @ApiOperation("删除")
    @Action(name = "删除事件")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        this.eventService.delete(id);
        return ResponseEntity.ok().build();
    }

}
