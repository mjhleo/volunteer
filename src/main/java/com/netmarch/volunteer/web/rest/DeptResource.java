package com.netmarch.volunteer.web.rest;

import com.github.pagehelper.PageInfo;
import com.netmarch.volunteer.aop.Action;
import com.netmarch.volunteer.dao.DeptMapper;
import com.netmarch.volunteer.dao.DictMapper;
import com.netmarch.volunteer.domain.Dept;
import com.netmarch.volunteer.domain.dto.DictDTO;
import com.netmarch.volunteer.domain.enumeration.DictType;
import com.netmarch.volunteer.mapper.DictDTOMapper;
import com.netmarch.volunteer.service.DeptService;
import com.netmarch.volunteer.service.DictService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@RestController
@RequestMapping("/api/dept")
@Api(value = "机构管理", tags = "机构管理")
public class DeptResource {
    @Autowired
    private DeptService deptService;

    @Autowired
    private DeptMapper deptMapper;

    @GetMapping
    @ApiOperation("机构列表")
    public ResponseEntity<PageInfo<Dept>> getDepts(

            @RequestParam(required = false) String name,
            @RequestParam(name = "pageNum", defaultValue = "1") int pageNum,
            @RequestParam(name = "pageSize", defaultValue = "10") int pageSize) {
        return ResponseEntity.ok(deptService.getDepts(name,pageNum, pageSize));
    }

    @PostMapping
    @ApiOperation("新增")
    @Action(name = "新增机构")
    public ResponseEntity<Void> insert(@Valid @RequestBody Dept dept) {
        deptMapper.insert(dept);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/{id}")
    @ApiOperation("修改")
    @Action(name = "修改机构")
    public ResponseEntity<Void> update(@PathVariable Long id, @Valid @RequestBody Dept dept) {
        deptService.update(dept.id(id));
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @ApiOperation("删除")
    @Action(name = "删除机构")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        deptMapper.deleteByPrimaryKey(id);
        return ResponseEntity.ok().build();
    }

}
