package com.netmarch.volunteer.web.rest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.netmarch.volunteer.aop.Action;
import com.netmarch.volunteer.config.ApplicationProperties;
import com.netmarch.volunteer.domain.dto.UserVolunteerLoginDTO;
import com.netmarch.volunteer.domain.dto.UserVolunteerRegisterDTO;
import com.netmarch.volunteer.exception.ApiException;
import com.netmarch.volunteer.exception.ErrorVM;
import com.netmarch.volunteer.service.UserVolunteerService;
import com.netmarch.volunteer.service.util.RandomValidateCodeUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;

/**
 * @author huangguochen
 * @create 2022/5/5 9:32
 */
@RestController
@RequestMapping("/api/volunteers")
@Api(tags = "志愿者用户管理")
@RequiredArgsConstructor
public class UserVolunteerResource {
    private final UserVolunteerService userVolunteerService;
    private final ApplicationProperties applicationProperties;

    @PostMapping("/register")
    @Action(name = "用户注册")
    @ApiOperation("用户注册")
    public ResponseEntity<Void> insert(@Valid @RequestBody UserVolunteerRegisterDTO dto) {
        this.userVolunteerService.insert(dto);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/login")
    @ApiOperation("用户登陆")
    public ResponseEntity<String> login(@Valid @RequestBody UserVolunteerLoginDTO loginDTO, HttpServletRequest request) {
        return ResponseEntity
            .ok(this.userVolunteerService.login(loginDTO, request, this.applicationProperties.getJwtSecret(), this.applicationProperties.getTokenExpire()));
    }

    @GetMapping(value = "/verify")
    @ApiOperation("获取验证码")
    public void getVerify(HttpServletRequest request, HttpServletResponse response) {

        try {

            //设置相应类型,告诉浏览器输出的内容为图片
            response.setContentType("image/jpeg");

            //设置响应头信息，告诉浏览器不要缓存此内容
            response.setHeader("Pragma", "No-cache");
            response.setHeader("Cache-Control", "no-cache");
            response.setDateHeader("Expire", 0);

            RandomValidateCodeUtil.getRandomCode(request, response);
        } catch (Exception e) {

            throw new ApiException(new ErrorVM(-10001, "验证码获取失败!!"));

        }
    }
}
