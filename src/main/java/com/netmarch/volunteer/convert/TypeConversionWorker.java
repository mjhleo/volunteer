package com.netmarch.volunteer.convert;

import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.mapstruct.Named;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;


/**
 * <Mapping通用转换>
 *
 * @author huangguochen
 * @create 2021/6/17 14:33
 */

@Component
@Named("TypeConversionWorker")
public class TypeConversionWorker {


    @Named("toStringList")
    public List<String> toStringList(String str) {
        if (StringUtils.isEmpty(str)) {
            return Collections.emptyList();
        }
        return JSON.parseArray(str, String.class);
    }


    @Named("toObjJsonString")
    public String toObjJsonString(Object obj) {
        if (ObjectUtils.isEmpty(obj)) {
            return null;
        }
        return JSON.toJSONString(obj);
    }

    @Named("emptyString2Null")
    public String emptyString2Null(String str) {
        return  StringUtils.isEmpty(str) ? null : str;
    }

}
