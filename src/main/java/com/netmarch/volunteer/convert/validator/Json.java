package com.netmarch.volunteer.convert.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * @author Mr.Wei  2021/4/1 11:15 上午
 */
@Target({ElementType.ANNOTATION_TYPE, ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = JsonValidator.class)
public @interface Json {
    String message() default "not json";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
