package com.netmarch.volunteer.convert.validator;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONException;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @author Mr.Wei  2021/4/1 11:16 上午
 */
public class JsonValidator implements ConstraintValidator<Json, Object> {
    @Override
    public void initialize(Json constraintAnnotation) {
        // Do nothing
    }

    @Override
    public boolean isValid(Object o, ConstraintValidatorContext constraintValidatorContext) {
        if (null == o) {
            return true;
        }
        try {
            JSON.parse(o.toString());
            return true;
        } catch (JSONException e) {
            return false;
        }
    }
}
