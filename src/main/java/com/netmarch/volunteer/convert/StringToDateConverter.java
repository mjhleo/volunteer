package com.netmarch.volunteer.convert;

import java.text.ParseException;
import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * @author glacier on 2019/3/26 10:13
 */
@Component
public class StringToDateConverter implements Converter<String, Date> {
    @Override
    public Date convert(String source) {
        if (org.apache.commons.lang3.StringUtils.isEmpty(source)) {
            return null;
        }
        try {
            if (source.matches("^\\d+$")) {
                Long date = new Long(source);
                return new Date(date);
            }
            if (source.length() <= 10) {
                return DateUtils.parseDate(source, "yyyy-MM-dd");
            } else {
                return DateUtils.parseDate(source, "yyyy-MM-dd HH:mm:ss");
            }
        } catch (ParseException e) {
            return null;
        }
    }
}
