package com.netmarch.volunteer.service;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.netmarch.volunteer.client.WxClient;
import com.netmarch.volunteer.config.ApplicationProperties;
import com.netmarch.volunteer.config.Constants;
import com.netmarch.volunteer.dao.RegisterMapper;
import com.netmarch.volunteer.domain.UserVolunteer;
import com.netmarch.volunteer.domain.dto.MenuDTO;
import com.netmarch.volunteer.domain.dto.UserVolunteerLoginDTO;
import com.netmarch.volunteer.domain.enumeration.MenuType;
import com.netmarch.volunteer.domain.enumeration.Platform;
import com.netmarch.volunteer.exception.ApiException;
import com.netmarch.volunteer.exception.ErrorVM;
import com.netmarch.volunteer.service.util.JwtUtil;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

/**
 * @author huangguochen
 * @create 2022/4/27 14:02
 */

@Service
@Log4j2
@RequiredArgsConstructor
public class WxService {
     private final UserService userService;
     private final WxClient wxClient;
     private final UserVolunteerService userVolunteerService;
     private final ApplicationProperties applicationProperties;
     private final RegisterMapper registerMapper;
     private final MenuService menuService;

     @Transactional(rollbackFor = Exception.class)
     public String login(String token) {
         String res = wxClient.getUserInfo(token);
         log.info("微信登录返回：{}", res);
         if (StringUtils.isEmpty(res)) {
             throw new ApiException(new ErrorVM(-10001, "获取用户信息失败"));
         }
         JSONObject jsonObject = JSON.parseObject(res);
         if (jsonObject.getIntValue("code") != 0) {
             throw new ApiException(new ErrorVM(-10001, jsonObject.getString("message")));
         }
         JSONObject data = jsonObject.getJSONObject("rsp");
         String idCard = data.getString("idCard");
         String phone = data.getString("phone");
         UserVolunteer volunteer = userVolunteerService.findByUserName(phone);
         if (Objects.isNull(volunteer)) {
             userVolunteerService.create(UserVolunteer.builder().username(phone).idcard(idCard).build());
         } else if (StringUtils.isBlank(volunteer.getPassword())) {
             String salt = new SecureRandomNumberGenerator().nextBytes().toHex();
             userVolunteerService.update(UserVolunteer.builder().id(volunteer.getId()).password(new Md5Hash(Constants.DEFAULT_ZERO, salt, 2).toHex()).salt(salt).build());
         }
         return userService.volounterLogin(UserVolunteerLoginDTO.builder().username(phone).password(Constants.DEFAULT_ZERO).build(), Platform.VOLUNTEER_WECHAT,
             this.applicationProperties.getJwtSecret(), this.applicationProperties.getTokenExpire());
     }

     public List<MenuDTO> getMenus(String token) {
         String username = JwtUtil.getUsername(token);
         UserVolunteer volunteer = userVolunteerService.findByUserName(username);
         if (Objects.isNull(volunteer)) {
             throw new ApiException(new ErrorVM(-10002, "用户不存在"));
         }
         Integer state = registerMapper.getcheckState(volunteer.getIdcard());
         if (Objects.nonNull(state) && state == Constants.PASS) {
             return menuService.getMenus("", MenuType.VOLUNTEER);
         }
         return Collections.emptyList();
     }

    public UserVolunteer getUserVolunteerInfo(String token) {
        return userVolunteerService.findByUserName(JwtUtil.getUsername(token));
    }

    public String tencentGetLonAndLat(String lat, String lng) {
        if (StringUtils.isEmpty(lat) || StringUtils.isEmpty(lng)) {
            throw new ApiException(new ErrorVM(-10003, "经纬度不能为空"));
        }
        String res = wxClient.getTencentaddress(lat + "," + lng, applicationProperties.getMapKey());
        if (StringUtils.isEmpty(res)) {
            throw new ApiException(new ErrorVM(-10004, "获取地址信息失败"));
        }
        JSONObject jsonObject = JSON.parseObject(res);
        if (jsonObject.getIntValue("status") != 0) {
            throw new ApiException(new ErrorVM(-10004, jsonObject.getString("message")));
        }
        JSONObject data = jsonObject.getJSONObject("result");
        return data.getString("address");
    }
}
