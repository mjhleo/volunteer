package com.netmarch.volunteer.service;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import com.google.common.collect.Lists;
import com.netmarch.volunteer.config.Constants;
import com.netmarch.volunteer.dao.RegisterMapper;
import com.netmarch.volunteer.dao.SystemMessageMapper;
import com.netmarch.volunteer.domain.Register;
import com.netmarch.volunteer.domain.SystemMessage;
import com.netmarch.volunteer.domain.command.SystemMessageQueryCommand;
import com.netmarch.volunteer.domain.dto.RegisterCheckDTO;
import com.netmarch.volunteer.domain.dto.SystemMessageDTO;
import com.netmarch.volunteer.domain.enumeration.MessageEnum;
import com.netmarch.volunteer.domain.enumeration.SystemMessageState;
import com.netmarch.volunteer.mapper.SystemMessageDTOMapper;
import com.netmarch.volunteer.service.util.JwtUtil;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.weekend.WeekendSqls;

/**
 * @author huangguochen
 * @create 2022/4/28 13:11
 */
@Service
public class SystemMessageService{

    @Resource
    private SystemMessageMapper systemMessageMapper;
    @Resource
    private RegisterMapper registerMapper;

    public PageInfo<SystemMessage> getSystemMessages(SystemMessageQueryCommand query, int pageNum, int pageSize) {
        PageMethod.startPage(pageNum, pageSize);
        Example example = new Example(SystemMessage.class);
        example.setOrderByClause("created_date desc");
        Example.Criteria criteria = example.createCriteria();
        if (Objects.nonNull(query.getMessageType())) {
            criteria.andEqualTo("messageType", query.getMessageType());
        }
        if (Objects.nonNull(query.getMessageState())) {
            criteria.andEqualTo("messageState", query.getMessageState());
        }
        if (Objects.nonNull(query.getReceiveUnitId())) {
            criteria.andEqualTo("receiveUnitId", query.getReceiveUnitId());
        }
        if (Objects.nonNull(query.getReceivePhone())) {
            criteria.andEqualTo("receivePhone", query.getReceivePhone());
        }
        return new PageInfo<>(this.systemMessageMapper.selectByExample(example));
    }

    public Integer unReadMessageNum(String token) {
        return systemMessageMapper.selectCountByExample(new Example.Builder(SystemMessage.class)
            .where(WeekendSqls.<SystemMessage> custom().andEqualTo(SystemMessage::getReceivePhone, JwtUtil.getUsername(token))
                .andEqualTo(SystemMessage::getMessageState, SystemMessageState.UNREAD))
            .build());
    }


    public void sendRegisterMessage(RegisterCheckDTO dto) {
        Integer checkState = dto.getCheckState();
        List<Register> registers;
        if (Objects.nonNull(dto.getId())) {
            registers = getRegisters(Collections.singletonList(dto.getId()));
        } else {
            registers = getRegisters(dto.getIds());
        }
        List<SystemMessageDTO> dtos = Lists.newArrayListWithCapacity(registers.size());
        registers.forEach(message -> {
            SystemMessageDTO build = SystemMessageDTO.builder().receivePhone(message.getPhone()).build();
            dtos.add(build.init(MessageEnum.REGISTRATION_RESULT,
                new String[] { Objects.equals(Constants.PASS, checkState) ? "通过" : "未通过, 审核意见：" + dto.getCheckAdvice() }));
        });
        insert(dtos);
    }
    
    public List<Register> getRegisters(List<Long> ids) {
        Example example = new Example.Builder(Register.class).where(WeekendSqls.<Register> custom().andIn(Register::getId, ids)).build();
        return registerMapper.selectByExample(example);
    }

    public void sendMissionMessage(Set<String> phones) {
        List<SystemMessageDTO> dtos = Lists.newArrayListWithCapacity(phones.size());
        phones.forEach(phone -> dtos.add(SystemMessageDTO.builder().receivePhone(phone).build().init(MessageEnum.TASK_NOTICE, null)));
        insert(dtos);
    }

    public Long sendNoticeMessage(SystemMessageDTO dto) {
        SystemMessage systemMessage = SystemMessageDTOMapper.INSTANCE.dto2Domain(dto);
        systemMessageMapper.insertSelective(systemMessage.init());
        return systemMessage.getId();
    }
    

    public void insert(List<SystemMessageDTO> systemMessageDTOList) {
        List<SystemMessage> systemMessages = SystemMessageDTOMapper.INSTANCE.dto2DomainList(systemMessageDTOList);
        systemMessages.forEach(SystemMessage::init);
        this.systemMessageMapper.insertList(systemMessages);
    }

    public void delete(Long messageId) {
        this.systemMessageMapper.deleteByPrimaryKey(messageId);
    }
}
