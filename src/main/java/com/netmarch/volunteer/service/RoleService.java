package com.netmarch.volunteer.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.netmarch.volunteer.dao.MenuMapper;
import com.netmarch.volunteer.dao.PermissionMapper;
import com.netmarch.volunteer.dao.RoleMapper;
import com.netmarch.volunteer.dao.UserMapper;
import com.netmarch.volunteer.domain.Permission;
import com.netmarch.volunteer.domain.Role;
import com.netmarch.volunteer.domain.User;
import com.netmarch.volunteer.domain.dto.PermissionDTO;
import com.netmarch.volunteer.domain.dto.RoleDTO;
import com.netmarch.volunteer.exception.ApiException;
import com.netmarch.volunteer.exception.ErrorVM;
import com.netmarch.volunteer.mapper.RoleDTOMapper;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.weekend.WeekendSqls;

/**
 * @author Mr.Wei  2021/4/7 1:47 下午
 */
@Service
public class RoleService {
    private RoleMapper roleMapper;
    private MenuMapper menuMapper;
    private PermissionMapper permissionMapper;
    private UserMapper userMapper;

    public PageInfo<Role> getRoles(String name, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        Example example = new Example(Role.class);
        Example.Criteria criteria = example.createCriteria();
        if (StringUtils.isNotBlank(name)) {
            criteria.andLike("name", "%" + name + "%");
        }
        return new PageInfo<>(this.roleMapper.selectByExample(example));
    }

    public RoleDTO getRole(Long roleId) {
        Role role = roleMapper.selectByPrimaryKey(roleId);
        RoleDTO roleDto = RoleDTOMapper.INSTANCE.domain2DTO(role);
        List<PermissionDTO> leafMenus = menuMapper.selectLeafMenus(roleId);
        return roleDto.initAuthDTO(leafMenus);
    }

    @Transactional(rollbackFor = Exception.class)
    public void insert(RoleDTO roleDto) {
        Role role = RoleDTOMapper.INSTANCE.dTO2Domain(roleDto);
        roleMapper.insert(role);
        insertPermission(roleDto.getAuth(), role.getId());
    }

    private void insertPermission(List<PermissionDTO> auths, Long roleId) {
        List<Permission> permissionList = new ArrayList<>();
        for (PermissionDTO auth : auths) {
            permissionList.add(new Permission(roleId, auth.getMenuId(), auth.getAuth()));
        }
        if (!CollectionUtils.isEmpty(permissionList)) {
            permissionMapper.insertList(permissionList);
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public void update(RoleDTO roleDto) {
        Role role = RoleDTOMapper.INSTANCE.dTO2Domain(roleDto);
        int i = roleMapper.updateByPrimaryKeySelective(role);
        if (i > 0 && isUpdated(roleDto.getAuth(), role.getId())) {
            permissionMapper.delete(new Permission(roleDto.getId()));
            insertPermission(roleDto.getAuth(), role.getId());
        }
    }

    public Boolean isUpdated(List<PermissionDTO> newAuth, Long roleId) {
        List<PermissionDTO> oldAuth = menuMapper.selectLeafMenus(roleId);
        if (oldAuth.size() != newAuth.size()) {
            return Boolean.TRUE;
        }
        oldAuth.addAll(newAuth);
        long count = oldAuth.stream().map(PermissionDTO::getMenuId).distinct().count();
        return newAuth.size() != count;
    }

    @Transactional(rollbackFor = Exception.class)
    public void delete(Long id) {
        Example example = new Example.Builder(User.class)
            .where(WeekendSqls.<User> custom().andEqualTo(User::getRoleId, id).andEqualTo(User::getActivated, Boolean.TRUE)).build();
        int count = this.userMapper.selectCountByExample(example);
        if (count > 0) {
            throw new ApiException(new ErrorVM(-10001, "目前还存在用户使用该角色, 暂时无法删除!!"));
        }
        this.roleMapper.deleteByPrimaryKey(id);
        this.permissionMapper.delete(new Permission(id));
    }

    @Autowired
    public void setRoleMapper(RoleMapper roleMapper) {
        this.roleMapper = roleMapper;
    }

    @Autowired
    public void setMenuMapper(MenuMapper menuMapper) {
        this.menuMapper = menuMapper;
    }

    @Autowired
    public void setPermissionMapper(PermissionMapper permissionMapper) {
        this.permissionMapper = permissionMapper;
    }

    @Autowired
    public void setUserMapper(UserMapper userMapper) {
        this.userMapper = userMapper;
    }
}
