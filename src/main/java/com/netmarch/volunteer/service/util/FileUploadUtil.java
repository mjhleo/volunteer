package com.netmarch.volunteer.service.util;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.netmarch.volunteer.exception.ApiException;
import com.netmarch.volunteer.exception.ErrorVM;

import net.logstash.logback.encoder.org.apache.commons.lang3.time.DateFormatUtils;

/**
 * <文件上传工具类>
 *
 * @author Huangguochen
 * @create 2020/10/21 10:11
 */
public class FileUploadUtil {

    private FileUploadUtil() {
        throw new IllegalStateException();
    }

    /**
     * 创建附件（MultipartFile格式）
     */
    public static String uploadFile(MultipartFile file, int fileMaxSize, String filePath) throws IOException {
        if (file == null) {
            throw new ApiException(ApiException.E_FILE_UPLOAD_ERROR);
        }

        if (fileMaxSize <= 0) {
            fileMaxSize = 10;
        }
        // 创建目录
        Path root = Paths.get(filePath);
        try {
            Files.createDirectories(root);
        } catch (IOException e) {
            throw new ApiException(ApiException.E_FILE_CREATE_ERROR);
        }

        String uploadName = file.getOriginalFilename();
        String[] strArray = uploadName.split("\\.");
        int suffixIndex = strArray.length - 1;
        String generate = UUIDBase64Util.generate();
        String exe = strArray[suffixIndex];
        String name = generate +"."+ exe;
        long size = file.getSize();
        if (size > fileMaxSize * 1024 * 1024) {
            throw  new ApiException(new ErrorVM(-10107, "单文件大小不能超过" + fileMaxSize + "M"));
        }
        String subPath = DateFormatUtils.format(new Date(), "yyyy-MM-dd");
        Path path = Paths.get(filePath, subPath);
        if (!path.toFile().exists()) {
            try {
                Files.createDirectory(path);
            } catch (IOException e1) {
                throw new ApiException(ApiException.E_FILE_CREATE_ERROR);
            }
        }
        assert name != null;
        String pelativePath = new File(subPath, name).toString();
        Files.write(Paths.get(filePath + pelativePath), file.getBytes());
        return pelativePath;
    }


    public static String uploadFiles(MultipartFile[] files, Integer uploadFileMaxSize, String uploadFileAddress) {
        if (Objects.nonNull(files) && files.length > 0) {
            List<String> urls = new ArrayList<>(files.length);
            for (MultipartFile file : files) {
                try {
                    urls.add(uploadFile(file, uploadFileMaxSize, uploadFileAddress));
                } catch (IOException e) {
                    throw new ApiException(ApiException.E_FILE_UPLOAD_ERROR);
                }
            }
            return StringUtils.join(urls, ",");
        }
        return "";
    }


    public static void download(String uri, String name, HttpServletResponse response, String uploadFileAddress) {
        String downloadFileName;
        if (StringUtils.isNotBlank(name)) {
            downloadFileName = name;
        } else {
            downloadFileName = StringUtils.substringAfterLast(uri, "/");
        }
        Path path = Paths.get(uploadFileAddress, uri);
        if (!path.toFile().exists()) {
            throw new ApiException(ApiException.E_FILE_DOWNLOAD_NULL);
        }
        try (OutputStream outputStream = response.getOutputStream()) {
            response.setContentType("application/octet-stream");
            response.setHeader("content-type", "application/octet-stream");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(downloadFileName, "UTF-8"));
            //在响应头里添加 Access-Control-Expose-Headers,暴露此响应头
            response.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
            Files.copy(path, response.getOutputStream());
        } catch (Exception e) {
            throw new ApiException(ApiException.E_FILE_DOWNLOAD_ERROR);
        }
    }
}
