package com.netmarch.volunteer.service.util;

import java.util.Objects;

import javax.annotation.CheckForNull;

import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.SignatureVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.netmarch.volunteer.domain.enumeration.Platform;
import com.netmarch.volunteer.domain.enumeration.RoleType;

public class JwtUtil {
    private JwtUtil() {
        throw new IllegalStateException();
    }

    public static boolean verify(String token, String secret) {
        try {
            Algorithm algorithm = Algorithm.HMAC256(secret);
            JWTVerifier verifier = JWT.require(algorithm).build();
            verifier.verify(token);
            return true;
        } catch (JWTDecodeException | SignatureVerificationException exception) {
            return false;
        }
    }

    public static Long getId(String token) {
        return Long.parseLong(getClaim(token, "id"));
    }


    @CheckForNull
    public static String getUsername(String token) {
        return getClaim(token, "username");
    }

    @CheckForNull
    public static RoleType getRoleType(String token) {
        String roleType = getClaim(token, "roleType");
        return StringUtils.isBlank(roleType) ? null : EnumUtils.getEnum(RoleType.class, roleType);
    }
    
    public static String getName(String token) {
        return getClaim(token, "name");
    }

    public static Platform getPlatform(String token) {
        return Platform.valueOf(getClaim(token, "platform"));
    }

    private static String getClaim(String token, String name) {
        try {
            DecodedJWT jwt = JWT.decode(token);
            return jwt.getClaim(name).asString();
        } catch (JWTDecodeException e) {
            return "";
        }
    }

    public static String create(Long id, Platform platform, String name, RoleType roleType, String otherName, String username, String secret) {
        Algorithm algorithm = Algorithm.HMAC256(secret);
        return JWT.create().withClaim("id", String.valueOf(id)).withClaim("name", name).withClaim("platform", platform.toString())
            .withClaim("roleType", Objects.isNull(roleType) ? "" : roleType.toString()).withClaim("otherName", Objects.isNull(otherName) ? "" : otherName).withClaim("username", username)
            .withClaim("createTime", System.currentTimeMillis()).sign(algorithm);
    }
}
