package com.netmarch.volunteer.service.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 *
 * 获取Spring Bean的工具类，必须在Spring容器启动后才能使用，
 *
 *
 */
@Slf4j
@Component
public class SpringBeanHelper implements ApplicationContextAware {
	private static ApplicationContext appContext;

	@Override
	public void setApplicationContext(ApplicationContext appContext) {
		if (null == SpringBeanHelper.appContext) {
			setContext(appContext);
		}
	}

	public static synchronized void setContext(ApplicationContext appContext){
		SpringBeanHelper.appContext = appContext;
	}

	/**
	 *
	 * @param name
	 * @return 如果异常或找不到则返回null
	 */
	public static Object getBean(String name) {
		try {
			return appContext.getBean(name);
		} catch (Exception e) {
			log.error("获取SpringBean(Name:" + name + ")失败：" + e.getMessage());
			return null;
		}
	}

	public static ApplicationContext getApplicationContext() {
		return appContext;
	}

	public static <T> T getBean(Class<T> clazz){
		return getApplicationContext().getBean(clazz);
	}

	/**
	 * @param name
	 * @param requiredType
	 * @return 如果异常或找不到则返回null
	 */
	public static <T> T getBean(String name, Class<T> requiredType) {
		try {
			return getApplicationContext().getBean(name, requiredType);
		} catch (Exception e) {
			log.error("获取SpringBean(Name:" + name + "/Class:" + requiredType.getName() + ")失败：" + e.getMessage());
			return null;
		}
	}

	/**
	 * 获取某一类的所有bean
	 *
	 * @param type
	 * @return 如果异常或找不到则返回null
	 */
	public static <T> Map<String, T> getBeansByType(Class<T> type) {
		try {
			return getApplicationContext().getBeansOfType(type);
		} catch (Exception e) {
			log.error("获取SpringBeansMap(Class:" + type.getName() + ")失败：" + e.getMessage());
			return null;
		}
	}

	/**
	 * 获取某一类的所有bean
	 *
	 * @param type
	 *            bean的类型
	 * @param includeNonSingletons
	 *            是否允许非单例
	 * @param allowEagerInit
	 *            是否初始化lazy-init的bean
	 * @return 如果异常或找不到则返回null
	 */
	public static <T> Map<String, T> getBeansByType(Class<T> type, boolean includeNonSingletons,
			boolean allowEagerInit) {
		try {
			return getApplicationContext().getBeansOfType(type, includeNonSingletons, allowEagerInit);
		} catch (Exception e) {
			log.error("获取SpringBeansMap(Class:" + type.getName() + ")失败：" + e.getMessage());
			return null;
		}

	}



}
