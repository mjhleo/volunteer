package com.netmarch.volunteer.service.util;


import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.apache.commons.lang3.StringUtils;

import com.netmarch.volunteer.exception.ApiException;
import com.netmarch.volunteer.exception.ErrorVM;


/**
 * <参数校验>
 *
 * @author Huangguochen
 * @create 2020/10/23 11:42
 */
public class FacadeInputValidator {

    private FacadeInputValidator() {
        throw new IllegalStateException();
    }

    private static ValidatorFactory factory = Validation.buildDefaultValidatorFactory();

    public static <T> void validate(T t, Class<?>... classes) {
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<T>> violationSet = validator.validate(t, classes);
        String errorMessage = violationSet.stream().map(ConstraintViolation::getMessageTemplate).collect(Collectors.joining(","));
        if (StringUtils.isNotEmpty(errorMessage)) {
            throw new ApiException(new ErrorVM(-1000, errorMessage));
        }
    }
}
