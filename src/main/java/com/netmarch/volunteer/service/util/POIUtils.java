package com.netmarch.volunteer.service.util;

import com.netmarch.volunteer.annotation.IgnoreField;
import com.netmarch.volunteer.exception.ApiException;
import com.netmarch.volunteer.exception.ErrorVM;
import io.swagger.annotations.ApiModelProperty;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.poi.hssf.usermodel.DVConstraint;
import org.apache.poi.hssf.usermodel.HSSFDataValidation;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.*;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.util.*;

/**
 * @author xiehui on 2019/8/2
 */
@Slf4j
public class POIUtils {

    private static final String EXCEL_CELL_FORMAT_ERROR = "在excel中第%s行的%s格式错误!";


    public static <T> Workbook createExcel(List<T> list, Boolean locked) throws IllegalAccessException {
        if (!CollectionUtils.isEmpty(list)) {
            XSSFWorkbook workbook = new XSSFWorkbook();
            Class<?> clazz = list.get(0).getClass();
            createSheet(workbook, clazz.getSimpleName(), list, locked, false);
            return workbook;
        }
        return null;
    }

    public static List<Field> getFields(Class clazz, boolean superBefore) {
        Class tempClass = clazz;
        List<Field> fields = new ArrayList<>();
        while (null != tempClass) {
            Field[] declaredFields = tempClass.getDeclaredFields();
            List<Field> tempFields = new LinkedList<>();
            for (Field declaredField : declaredFields) {
                if (!declaredField.isAnnotationPresent(IgnoreField.class)) {
                    tempFields.add(declaredField);
                }
            }
            if (clazz != tempClass && superBefore) {
                tempFields.addAll(fields);
                fields = tempFields;
            } else {
                fields.addAll(tempFields);
            }
            tempClass = tempClass.getSuperclass();
        }
        return fields;
    }

    public static <T> void createSheet(XSSFWorkbook workbook, String sheetName, List<T> list, Boolean locked, boolean superBefore) throws IllegalAccessException {
        if (!CollectionUtils.isEmpty(list)) {
            Class<?> clazz = list.get(0).getClass();
            XSSFSheet sheet = workbook.createSheet(sheetName);
            List<Field> fields = getFields(clazz, superBefore);
            Field[] fieldsArr = new Field[fields.size()];
            Field[] declaredFields = fields.toArray(fieldsArr);
            XSSFRow title = sheet.createRow(0);
            XSSFCellStyle cellStyle = workbook.createCellStyle();
            CreationHelper creationHelper = workbook.getCreationHelper();
            cellStyle.setDataFormat(creationHelper.createDataFormat().getFormat("yyyy-MM-dd HH:mm:ss"));
            cellStyle.setLocked(!locked);
            XSSFCellStyle lockstyle = workbook.createCellStyle();
            lockstyle.setLocked(locked);
            XSSFCellStyle unlockstyle = workbook.createCellStyle();
            unlockstyle.setLocked(!locked);
            sheet.setDefaultColumnWidth(10);
            for (int i = 0; i < list.size(); i++) {
                XSSFRow row = sheet.createRow(i + 1);
                T t = list.get(i);
                for (int j = 0; j < declaredFields.length; j++) {
                    Field declaredField = declaredFields[j];
                    declaredField.setAccessible(true);
                    if (i == 0) {
                        ApiModelProperty annotation = declaredField.getAnnotation(ApiModelProperty.class);
                        XSSFCell cell = title.createCell(j);
                        if (annotation != null) {
                            cell.setCellValue(annotation.value());
                        } else {
                            cell.setCellValue(declaredField.getName());
                        }
                        if (declaredField.getType() == Boolean.class) {
                            setBoxs(sheet, list.size(), j, j);
                        } else if (declaredField.getType() == Date.class) {
                            setDate(sheet, list.size(), j, j);
                        }
                    }
                    XSSFCell cell = row.createCell(j);
                    cell.setCellValue(declaredField.get(t) == null ? "" : declaredField.get(t).toString());
                    if (declaredField.getType() == Date.class) {
                        cell.setCellStyle(cellStyle);
                    } else {
                        if (declaredField.get(t) != null) {
                            cell.setCellStyle(lockstyle);
                        } else {
                            cell.setCellStyle(unlockstyle);
                        }
                    }
                }
            }
            if (locked) {
                sheet.protectSheet("1qa2ws3ed4rf5tg");
                sheet.enableLocking();
            }
        }
    }

    public static void setBoxs(Sheet sheet, String[] dataList,Integer lastRow, Integer firstCol, Integer lastCol) {
        XSSFDataValidationHelper dvHelper = new XSSFDataValidationHelper((XSSFSheet) sheet);
        XSSFDataValidationConstraint dvConstraint = (XSSFDataValidationConstraint) dvHelper
                .createExplicitListConstraint(dataList);
        CellRangeAddressList addressList = new CellRangeAddressList(1, lastRow, firstCol, lastCol);
        XSSFDataValidation validation = (XSSFDataValidation) dvHelper.createValidation(dvConstraint, addressList);
        validation.setSuppressDropDownArrow(true);
        validation.createPromptBox("提示", "请从下拉列表中选择是或否");
        validation.setShowErrorBox(true);
        sheet.addValidationData(validation);
    }

    public static void setBoxs(XSSFSheet sheet, Integer lastRow, Integer firstCol, Integer lastCol) {
        final String[] DATA_LIST = new String[]{"是", "否",};
        XSSFDataValidationHelper dvHelper = new XSSFDataValidationHelper((XSSFSheet) sheet);
        XSSFDataValidationConstraint dvConstraint = (XSSFDataValidationConstraint) dvHelper
                .createExplicitListConstraint(DATA_LIST);
        CellRangeAddressList addressList = new CellRangeAddressList(1, lastRow, firstCol, lastCol);
        XSSFDataValidation validation = (XSSFDataValidation) dvHelper.createValidation(dvConstraint, addressList);
        validation.setSuppressDropDownArrow(true);
        validation.createPromptBox("提示", "请从下拉列表中选择是或否");
        validation.setShowErrorBox(true);
        sheet.addValidationData(validation);
    }

    // 日期格式限制
    public static void setDate(XSSFSheet sheet, Integer lastRow, Integer firstCol, Integer lastCol) {
        CellRangeAddressList addressList = new CellRangeAddressList(1, lastRow, firstCol, lastCol);
        XSSFDataValidationHelper dvHelper = new XSSFDataValidationHelper(sheet);
        XSSFDataValidationConstraint dvConstraint = (XSSFDataValidationConstraint) dvHelper
                .createDateConstraint(DVConstraint.OperatorType.BETWEEN, "1900-01-01",
                        "5000-01-01", "yyyy-mm-dd");
        XSSFDataValidation validation = (XSSFDataValidation) dvHelper.createValidation(dvConstraint, addressList);
        validation.setSuppressDropDownArrow(true);
        validation.createPromptBox("输入提示", "请填写日期,格式:yyyy-mm-dd HH:MM:dd");
        // 设置输入错误提示信息
        validation.createErrorBox("日期格式错误提示", "你输入的日期格式不符合'yyyy-mm-dd'格式规范，请重新输入！");
        validation.setShowPromptBox(true);
        sheet.addValidationData(validation);
    }

    private static Workbook getReadWorkBookType(MultipartFile file) {
        String filePath = file.getOriginalFilename();
        InputStream is = null;
        Workbook workbook = null;
        try {
            is = file.getInputStream();
            if (filePath.toLowerCase().endsWith("xlsx")) {
                workbook = new XSSFWorkbook(is);
            } else if (filePath.toLowerCase().endsWith("xls")) {
                workbook = new HSSFWorkbook(is);
            } else {
                //  抛出自定义的业务异常
                throw new ApiException(ApiException.E_EXCEL_FOMAT_ERROR);
            }
        } catch (Exception e) {
            log.error(ExceptionUtils.getStackTrace(e));
            throw new ApiException(ApiException.E_EXCEL_FOMAT_ERROR);
        } finally {
            IOUtils.closeQuietly(is);
        }
        return workbook;
    }







    public static Map<Integer, Object> readExcel(MultipartFile file, Class clazz,int startRowNum) throws
            IllegalAccessException, InstantiationException {
        Map<Integer, Object> map = new HashMap<>();
        Workbook workbook = getReadWorkBookType(file);
        if (workbook == null) {
            throw new ApiException(ApiException.E_EXCEL_FOMAT_ERROR);
        }
        Sheet sheet = workbook.getSheetAt(0);
        Field[] fields = clazz.getDeclaredFields();
        for (int i = startRowNum; i <= sheet.getLastRowNum(); i++) {
            Row titles = sheet.getRow(0);
            Row row = sheet.getRow(i);
            Object obj = clazz.newInstance();
            Boolean isFalse = false;
            fal:
            for (int j = 0; j < row.getLastCellNum(); j++) {
                Cell title = titles.getCell(j);
                Cell cell = row.getCell(j);
                String titleValue = title.getStringCellValue();
                Boolean flag = true;
                fa:
                for (int x = 0; x < fields.length; x++) {
                    Field field = fields[x];
                    field.setAccessible(true);
                    ApiModelProperty annotation = field.getAnnotation(ApiModelProperty.class);
                    //ExcelFieldType annotation1 = field.getAnnotation(ExcelFieldType.class);
                    String match = null;
                    if (annotation != null) {
                        match = annotation.value();
                    } else {
                        match = field.getName();
                    }
                    if (match.equals(titleValue)) {
                        Class<?> fieldClass = field.getType();
                        Double numericCellValue = null;
                        String stringCellValue = null;
                        if (cell != null) {
                            if (cell.getCellType() == CellType.NUMERIC) {
                                numericCellValue = cell.getNumericCellValue();
                            } else {
                                stringCellValue = cell.getStringCellValue();
                            }
                        }
                        if (fieldClass == Double.class) {
                            if (numericCellValue == null && StringUtils.isEmpty(stringCellValue)) {
                                throw new ApiException(new ErrorVM(300014, String.format(EXCEL_CELL_FORMAT_ERROR, i + 1, titleValue)));
                            }
                            String doubleResult = numericCellValue != null ? numericCellValue.toString() : StringUtils.isBlank(stringCellValue) ? "0.0" : stringCellValue;
                            if (!doubleResult.matches("^\\d+(\\.\\d+)?")) {
                                throw new ApiException(new ErrorVM(300014, String.format(EXCEL_CELL_FORMAT_ERROR, i + 1, titleValue)));
                            }
                            field.set(obj, Double.valueOf(doubleResult));
                        } else if (fieldClass == Integer.class) {
                            if (numericCellValue == null && StringUtils.isEmpty(stringCellValue)) {
                                throw new ApiException(new ErrorVM(300014, String.format(EXCEL_CELL_FORMAT_ERROR, i + 1, titleValue)));
                            }
                            String intResult = numericCellValue != null ? numericCellValue.toString() : StringUtils.isBlank(stringCellValue) ? "0" : stringCellValue;
                            if (intResult.contains(".")) {
                                intResult = intResult.substring(0, intResult.indexOf("."));
                            }
                            if (!intResult.matches("^\\d+?")) {
                                throw new ApiException(new ErrorVM(300014, String.format(EXCEL_CELL_FORMAT_ERROR, i + 1, titleValue)));
                            }
                            field.set(obj, Integer.valueOf(intResult));
                        } else if (fieldClass == Date.class) {
                            if (stringCellValue != null) {
                                throw new ApiException(new ErrorVM(300014, String.format(EXCEL_CELL_FORMAT_ERROR, i + 1, titleValue)));
                            }
                            field.set(obj, numericCellValue == null ? null : org.apache.poi.ss.usermodel.DateUtil.getJavaDate(numericCellValue));
                        } else {
                            field.set(obj, stringCellValue);
                        }
                        flag = false;
                        break fa;
                    }
                }
                if (flag) {
                    throw new ApiException(ApiException.E_EXCEL_FOMAT_ERROR);
                }
            }
            if (!isFalse) {
                map.put(i + 1, obj);
            }
        }
        return map;
    }

    public static ResponseEntity<byte[]> getResponseEntity(Workbook workbook, String name) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        if (workbook == null) {
            throw new ApiException(new ErrorVM(30001, "生成Excel错误"));
        }
        workbook.write(byteArrayOutputStream);
        HttpHeaders headers = new HttpHeaders();
        //下载显示的文件名，解决中文名称乱码问题
        String fileName = null;
        try {
            fileName = new String(String.format(name).getBytes("UTF-8"), "iso-8859-1");
        } catch (UnsupportedEncodingException e) {
            log.error(ExceptionUtils.getStackTrace(e));
        }
        //通知浏览器以attachment（下载方式)
        headers.setContentDispositionFormData("Content-Disposition", fileName);
        //application/octet-stream ： 二进制流数据（最常见的文件下载）。
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        return new ResponseEntity<>(byteArrayOutputStream.toByteArray(), headers, HttpStatus.CREATED);
    }


    public static HSSFDataValidation setDataValidation(String strFormula,
                                                        int firstRow, int endRow, int firstCol, int endCol) {
        // 设置数据有效性加载在哪个单元格上。四个参数分别是：起始行、终止行、起始列、终止列
        CellRangeAddressList regions = new CellRangeAddressList(firstRow, endRow, firstCol, endCol);
        DVConstraint constraint = DVConstraint.createFormulaListConstraint(strFormula);
        HSSFDataValidation dataValidation = new HSSFDataValidation(regions,constraint);

        dataValidation.createErrorBox("Error", "Error");
        dataValidation.createPromptBox("", null);

        return dataValidation;
    }

    /**
     *
     * @Title: setDataValidation
     * @Description: 下拉列表元素不多的情况(255以内的下拉)
     * @param @param sheet
     * @param @param textList
     * @param @param showErrorBox  是否允许在序列之外自己输入,true有提示则不能自己输；反之可以手输
     * @param @param firstRow
     * @param @param endRow
     * @param @param firstCol
     * @param @param endCol
     * @param @return
     * @return DataValidation
     * @throws
     */
    public static DataValidation setDataValidation(Sheet sheet, String[] textList, boolean showErrorBox,int firstRow, int endRow, int firstCol, int endCol) {

        DataValidationHelper helper = sheet.getDataValidationHelper();
        //加载下拉列表内容
        DataValidationConstraint constraint = helper.createExplicitListConstraint(textList);
        //DVConstraint constraint = new DVConstraint();
        constraint.setExplicitListValues(textList);

        //设置数据有效性加载在哪个单元格上。四个参数分别是：起始行、终止行、起始列、终止列
        CellRangeAddressList regions = new CellRangeAddressList((short) firstRow, (short) endRow, (short) firstCol, (short) endCol);

        //数据有效性对象
        DataValidation data_validation = helper.createValidation(constraint, regions);
        //DataValidation data_validation = new DataValidation(regions, constraint);

        data_validation.setSuppressDropDownArrow(false);
        data_validation.createPromptBox("提示", "请选择");
        data_validation.setShowPromptBox(true);
        data_validation.setShowErrorBox(showErrorBox);
        return data_validation;
    }

}
