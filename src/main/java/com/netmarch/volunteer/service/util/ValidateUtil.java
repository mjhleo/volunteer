package com.netmarch.volunteer.service.util;

import org.springframework.util.CollectionUtils;

import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.util.Collection;

/**
 *
 * 验证工具类
 *
 * @author TontoZhou
 *
 */
public class ValidateUtil {

	private ValidateUtil() {
		throw new IllegalStateException("ValidateUtil class");
	}

	/**
	 * 判断一个数组中的全部数字是否在另一个数组中，或判断一个数是否在一个数组中
	 *
	 * @param value
	 *            可以是一个{@link Number}的实现类，或者数组
	 * @param array
	 * @return
	 */
	public static boolean validContainInt(Object value, int[] array) {

		if (value != null && array != null) {
			Class<?> clazz = value.getClass();
			if (Number.class.isAssignableFrom(clazz)) {
				Number num = (Number) value;
				int val = num.intValue();
				for (int i : array)
					if (i == val) {
						return true;
					}
			} else if (clazz.isArray()) {
				int len = Array.getLength(value);
				return  getArrayResult(value, array, len);
			} else if (Collection.class.isAssignableFrom(clazz)) {
				Collection<?> coll = (Collection<?>) value;
				return getCollectionResult(array, coll);
			}

		}

		return false;
	}

	private static boolean getCollectionResult(int[] array, Collection<?> coll) {
		if (CollectionUtils.isEmpty(coll)) {
			return false;
		}
		for (Object obj : coll) {
			if (!validContainInt(obj, array)) {
				return false;
			}
		}
		return true;
	}

	private static boolean getArrayResult(Object value, int[] array, int len) {
		if (len == 0)
			return false;
		for (int i = 0; i < len; i++) {
			Object obj = Array.get(value, i);
			if (!validContainInt(obj, array)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 验证长度是否范围内
	 *
	 * @param max
	 * @param min
	 * @param inclusive
	 *            是否边界包含，闭区间
	 * @param value
	 * @return
	 */
	public static boolean validLengthRange(int max, int min, boolean inclusive, Object value) {
		int length = value.toString().length();
		return inclusive ? (length <= max && length >= min) : (length < max && length > min);
	}

	/**
	 * 验证长度是否大于
	 *
	 * @param max
	 * @param inclusive
	 * @param value
	 * @return
	 */
	public static boolean validGreatLength(int max, boolean inclusive, Object value) {
		int length = value.toString().length();
		return inclusive ? length <= max : length < max;
	}

	/**
	 * 验证长度是否小于
	 *
	 * @param min
	 * @param inclusive
	 * @param value
	 * @return
	 */
	public static boolean validLessLength(int min, boolean inclusive, Object value) {
		int length = value.toString().length();
		return inclusive ? length >= min : length > min;
	}

	/**
	 * 验证数值是否范围内
	 *
	 * @param max
	 * @param min
	 * @param inclusive
	 * @param value
	 * @return
	 */
	public static boolean validDigitRange(BigDecimal max, BigDecimal min, boolean inclusive, Object value) {
		try {
			BigDecimal val = new BigDecimal(value.toString());
			int maxResult = val.compareTo(max);
			int minResult = val.compareTo(min);
			return inclusive ? (maxResult <= 0 && minResult >= 0) : (maxResult < 0 && minResult > 0);
		} catch (NumberFormatException e) {
			return false;
		}
	}

	/**
	 * 判断值是否大于最大
	 *
	 * @param max
	 * @param inclusive
	 * @param value
	 * @return
	 */
	public static boolean validGreatNumber(BigDecimal max, boolean inclusive, Object value) {
		try {
			int comparisonResult = new BigDecimal(value.toString()).compareTo(max);
			return inclusive ? comparisonResult <= 0 : comparisonResult < 0;
		} catch (NumberFormatException nfe) {
			return false;
		}
	}

	/**
	 * 判断值是否小于最小
	 *
	 * @param min
	 * @param inclusive
	 * @param value
	 * @return
	 */
	public static boolean validLessNumber(BigDecimal min, boolean inclusive, Object value) {
		try {
			int comparisonResult = new BigDecimal(value.toString()).compareTo(min);
			return inclusive ? comparisonResult >= 0 : comparisonResult > 0;
		} catch (NumberFormatException nfe) {
			return false;
		}
	}
}
