package com.netmarch.volunteer.service.util;
import org.springframework.http.*;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import java.io.IOException;
public class HttpRestUtils {
    /**
     * http post
     * */
    public static String post(String url, MultiValueMap<String, String> params) throws IOException {
        return  httpRestClient(url, HttpMethod.POST, params);
    }
 
    /**
     * http get
     * */
    public static String get(String url, MultiValueMap<String, String> params) throws IOException {
        return  httpRestClient(url, HttpMethod.GET, params);
    }
 
    /**
     * HttpMethod  post/get
     * */
    private static String httpRestClient(String url, HttpMethod method, MultiValueMap<String, String> params){
        SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
        requestFactory.setConnectTimeout(10*1000);
        requestFactory.setReadTimeout(10*1000);
        RestTemplate client = new RestTemplate(requestFactory);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<MultiValueMap<String, String>>(params, headers);
        ResponseEntity<String> response = null;
        try{
            response = client.exchange(url, HttpMethod.POST, requestEntity, String.class);
            System.out.println("response="+response);
            return response.getBody();
        }
        catch (HttpClientErrorException e){
            e.printStackTrace();
            return "";
        }
        catch (Exception e) {
            return "";
        }

    }
}