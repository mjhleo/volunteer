package com.netmarch.volunteer.service.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

/**
 * 转化值工具类
 *
 * @author TontoZhou
 *
 */
public class ParseUtil {
	private ParseUtil() {
		throw new IllegalStateException();
	}

	private static final Map<String, ThreadLocal<SimpleDateFormat>> THREAD_LOCAL_MAP = new HashMap<>();

	private static ThreadLocal<SimpleDateFormat> getThreadLocal(final String format) {

		ThreadLocal<SimpleDateFormat> threadLocal = THREAD_LOCAL_MAP.get(format);

		if (null == threadLocal) {
			synchronized (THREAD_LOCAL_MAP) {
				threadLocal = THREAD_LOCAL_MAP.computeIfAbsent(format, s -> ThreadLocal.withInitial(() -> new SimpleDateFormat(format)));
			}
		}

		return threadLocal;

	}

	/**
	 * 通过线程变量创建并获取安全的{@link SimpleDateFormat}
	 * @param format
	 * @return
	 */
	public static SimpleDateFormat getThreadSafeFormat(String format) {
		return getThreadLocal(format).get();
	}

	/**
	 *
	 * @param str
	 * @param type
	 * @return
	 */
	public static Date parseDateString(String str, String pattern) {
		Date date = null;
		if (StringUtils.isNotBlank(str) && StringUtils.isNotBlank(pattern) ) {
			try {
				date =  getThreadSafeFormat(pattern).parse(str);
			} catch (ParseException e1) {
				return null;
			}
		}
		return date;
	}
}
