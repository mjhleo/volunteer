package com.netmarch.volunteer.service.util;


import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.read.listener.ReadListener;
import com.alibaba.excel.util.ListUtils;
import com.alibaba.fastjson.JSON;
import com.netmarch.volunteer.dao.DeptMapper;
import com.netmarch.volunteer.dao.DictMapper;
import com.netmarch.volunteer.dao.MissionMapper;
import com.netmarch.volunteer.domain.Dept;
import com.netmarch.volunteer.domain.Dict;
import com.netmarch.volunteer.domain.Mission;
import com.netmarch.volunteer.domain.dto.MissionImportDTO;
import com.netmarch.volunteer.domain.enumeration.DictType;
import com.netmarch.volunteer.exception.ApiException;
import com.netmarch.volunteer.exception.ErrorVM;
import com.netmarch.volunteer.exception.FieldErrorVM;
import com.netmarch.volunteer.service.SystemMessageService;

import lombok.extern.slf4j.Slf4j;

/**
 * 模板的读取类
 *
 * @author Jiaju Zhuang
 */
// 有个很重要的点 DemoDataListener 不能被spring管理，要每次读取excel都要new,然后里面用到spring可以构造方法传进去
@Slf4j
public class UploadDataListener implements ReadListener<MissionImportDTO> {
    /**
     * 每隔5条存储数据库，实际使用中可以100条，然后清理list ，方便内存回收
     */
    private static final int BATCH_COUNT = 500;
    private List<MissionImportDTO> cachedDataList = ListUtils.newArrayListWithExpectedSize(BATCH_COUNT);
    private List<FieldErrorVM> errors = ListUtils.newArrayListWithExpectedSize(BATCH_COUNT);
    private MissionMapper missionMapper = SpringBeanHelper.getBean(MissionMapper.class);
    private DictMapper dictMapper = SpringBeanHelper.getBean(DictMapper.class);
    private DeptMapper deptMapper = SpringBeanHelper.getBean(DeptMapper.class);
    private SystemMessageService systemMessageService = SpringBeanHelper.getBean(SystemMessageService.class);
    private String token;
    private Boolean validated;
    private Long eventId;


    public UploadDataListener(String token, Boolean validated,Long eventId) {
        this.token = token;
        this.validated = validated;
        this.eventId = eventId;
    }

    /**
     * 这个每一条数据解析都会来调用
     *
     * @param data    one row value. Is is same as {@link AnalysisContext#readRowHolder()}
     * @param context
     */
    @Override
    public void invoke(MissionImportDTO data, AnalysisContext context) {
        log.info("解析到一条数据:{}", JSON.toJSONString(data));
        int rouNumber = context.readRowHolder().getRowIndex() + 1;
        cachedDataList.add(data);
        // 达到BATCH_COUNT了，需要去存储一次数据库，防止数据几万条数据在内存，容易OOM
        if (cachedDataList.size() >= BATCH_COUNT) {
            saveData();
            // 存储完成清理 list
            cachedDataList = ListUtils.newArrayListWithExpectedSize(BATCH_COUNT);
            errors = ListUtils.newArrayListWithExpectedSize(BATCH_COUNT);
        }
    }

    /**
     * 所有数据解析完成了 都会来调用
     *
     * @param context
     */
    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {
        // 这里也要保存数据，确保最后遗留的数据也存储到数据库
        saveData();
        log.info("所有数据解析完成！");
        if (CollectionUtils.isNotEmpty(errors)) {
            throw new ApiException(new ErrorVM(-10001, "", errors));
        }
    }

    /**
     * 加上存储数据库
     */
    private void saveData() {
        log.info("{}条数据，开始存储数据库！", cachedDataList.size());
        try {
            List<Dict> dictList = dictMapper.findByType(DictType.DISTRICT_TOWN.toString());
            Map<String,String> dictMap = dictList.stream().collect(Collectors.toMap(Dict::getName,Dict::getCode,(k1, k2)->k2));
            List<Dept> deptList = deptMapper.selectAll();
            Map<String,Long> deptMap = deptList.stream().collect(Collectors.toMap(Dept::getName,Dept::getId,(k1, k2)->k2));
            if (!CollectionUtils.isEmpty(cachedDataList)) {
                Set<String> phones = new HashSet<>(cachedDataList.size());
                for (int i = 0; i < cachedDataList.size(); i++) {
                    MissionImportDTO missionImportDTO = cachedDataList.get(i);
                    Mission mission=new Mission();
                    mission.setName(missionImportDTO.getName());
                    mission.setPhone(missionImportDTO.getPhone());
                    mission.setEvent_id(eventId);
                    mission.setTown_id(dictMap.get(missionImportDTO.getTown()));
                    mission.setDept_id(deptMap.get(missionImportDTO.getDeptName()));
                    mission.setState(Boolean.FALSE);
                    phones.add(missionImportDTO.getPhone());
                    missionMapper.insert(mission);
                }
                systemMessageService.sendMissionMessage(phones);
            }
        } catch (Exception e) {
            log.error(ExceptionUtils.getStackTrace(e));
            throw new ApiException(ApiException.E_EXCEL_UPLOAD_ERROR);
        }
        log.info("存储数据库成功！");
    }
    
}
