package com.netmarch.volunteer.service.util;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import org.apache.commons.lang3.time.DateUtils;

import com.netmarch.volunteer.config.Constants;

import lombok.extern.slf4j.Slf4j;

/**
 * @author zhuguanming
 * @date 2020/11/9
 */
@Slf4j
public class DateFormatUtil {

    private DateFormatUtil() {
        throw new IllegalStateException("DateFormatUtil class");
    }

    public static final Long DAY_TIME = 24 * 60 * 60 * 1000L;

    public static Date getOneMonthBefore() {
        return getStartDateForDay(DateUtils.addMonths(new Date(), -1));
    }

    public static Date getOneWeekBefore() {
        return getStartDateForDay(DateUtils.addDays(new Date(), -7));
    }

    public static Date getAddDayDate(Date date, int num) {
        return getStartDateForDay(DateUtils.addDays(date, num));
    }

    public static Date getStartAddDayDate(int num) {
        return getStartDateForDay(DateUtils.addDays(new Date(), num));
    }

    public static Date getEndAddDayDate(int num) {
        return getEndDateForDay(DateUtils.addDays(new Date(), num));
    }

    public static Date getAddMonthDate(int num) {
        return getStartDateForDay(DateUtils.addMonths(new Date(), num));
    }

    /**
     * 一天的开始时间
     */
    public static Date getStartDateForDay(Date date) {
        date = DateUtils.setHours(date, 0);
        date = DateUtils.setMinutes(date, 0);
        date = DateUtils.setSeconds(date, 0);
        return DateUtils.setMilliseconds(date, 0);
    }

    /**
     * 一天的结束时间
     */
    public static Date getEndDateForDay(Date date) {
        date = DateUtils.setHours(date, 23);
        date = DateUtils.setMinutes(date, 59);
        date = DateUtils.setSeconds(date, 59);
        return DateUtils.setMilliseconds(date, 999);
    }

    /**
     * 获取指定月份的第一天的第一秒  默认上个月
     *
     * @param date 时间
     * @return
     */
    public static Date getMonthFirstDay(Date date) {
        if (date == null) {
            date = getOneMonthBefore();
        }
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        instance.set(Calendar.DATE, 1);
        return getStartDateForDay(instance.getTime());
    }

    /**
     * 获取指定月份的最后一天最后一秒 默认上个月
     *
     * @param date 时间
     * @return
     */
    public static Date getMonthLastDay(Date date) {
        if (date == null) {
            date = getOneMonthBefore();
        }
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        instance.add(Calendar.MONTH, 1);
        instance.add(Calendar.DATE, -instance.get(Calendar.DAY_OF_MONTH));

        return getEndDateForDay(instance.getTime());
    }

    /**
     * 获取区间内日数据
     *
     * @param startDate
     * @param endDate
     * @return
     */
    public static List<Date> getDayBetween(Date startDate, Date endDate) {
        List<Date> days = new ArrayList<>();
        while (endDate.compareTo(startDate) >= 0) {
            days.add(startDate);
            startDate = DateUtils.addDays(startDate, 1);
        }
        return days;
    }

    public static Date getYearFirstMonthDay(Date date) {
        return getMonthFirstDay(DateUtils.setMonths(date, 0));
    }

    public static Date getYearLastMonthDay(Date date) {
        return getMonthLastDay(DateUtils.setMonths(date, 11));
    }

    public static Date getLastYearFirstMonthDay() {
        Date date = DateUtils.addYears(new Date(), -1);
        date = DateUtils.setMonths(date, 0);
        return date;
    }

    public static Date getLastYearLastMonthDay() {
        Date date = DateUtils.addYears(new Date(), -1);
        date = DateUtils.setMonths(date, 11);
        return date;
    }

    public static int getDayBetweenDate(Date startDay, Date endDay) {
        return (int) ((DateFormatUtil.getStartDateForDay(endDay).getTime() - DateFormatUtil.getStartDateForDay(startDay).getTime()) / DAY_TIME);
    }

    public static Date getDatePlusWeek(Date date, int week) {
        return new Date(date.getTime() + week * 7 * DAY_TIME);
    }

    public static int getWeek(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
        return 0 == w ? 7 : w;
    }


    public static Date getDateOfThisWeek(int week) {
        if (Objects.isNull(week) || week > 7 || week < 1) {
            return null;
        }
        Calendar c = Calendar.getInstance();
        int dayofweek = c.get(Calendar.DAY_OF_WEEK) - 1;
        if (dayofweek == 0)
            dayofweek = 7;
        c.add(Calendar.DATE, -dayofweek + week);
        return c.getTime();
    }

    public static Date getDateOfNextWeek(int week) {
        if (Objects.isNull(week) || week > 7 || week < 1) {
            return null;
        }
        Calendar c = Calendar.getInstance();
        int dayofweek = c.get(Calendar.DAY_OF_WEEK) - 1;
        if (dayofweek == 0)
            dayofweek = 7;
        c.add(Calendar.DATE, -dayofweek + week +7);
        return c.getTime();
    }


    public static Date getDatePlusDay(Date date, int day) {
        return new Date(date.getTime() + day * DAY_TIME);
    }

    public static boolean isEffectiveDate(Date nowTime, String startTime, String endTime) {
        Date start = ParseUtil.parseDateString(startTime, Constants.CONSTANT_DATETIME_FORMAT);
        Date end = ParseUtil.parseDateString(endTime, Constants.CONSTANT_DATETIME_FORMAT);
        return isEffectiveDate(nowTime, start, end);
    }

    public static boolean isEffectiveDate(Date nowTime, Date startTime, Date endTime) {
        if (nowTime.getTime() == startTime.getTime()) {
            return true;
        }

        Calendar date = Calendar.getInstance();
        date.setTime(nowTime);

        Calendar begin = Calendar.getInstance();
        begin.setTime(startTime);

        Calendar end = Calendar.getInstance();
        end.setTime(endTime);

        return date.after(begin) && date.before(end);
    }
}
