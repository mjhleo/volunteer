package com.netmarch.volunteer.service.util;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.locks.ReentrantLock;

/**
 * author:zhuguanming
 * date:2020/12/24
 */
public class ReentrantLockUtils {

    private ReentrantLockUtils() {
    }

    /**
     * 显示锁集合
     */
    protected static final ConcurrentMap<Object, ReentrantLock> LOCK_MAP = new ConcurrentHashMap<>();

    public static ReentrantLock  getLock(Object code){
        ReentrantLock lock = new ReentrantLock();
        //如果同步集合中已经存在锁，则直接返回已经存在的锁
        ReentrantLock preLock = LOCK_MAP.putIfAbsent(code,lock);
        //如果添加成功则使用新锁
        return preLock == null ? lock : preLock;
    }

    public static void lock(Object code){
        getLock(code).lock();
    }

    public static void unlock(Object code){
        ReentrantLock lock = getLock(code);
        if(lock.isHeldByCurrentThread()){
            lock.unlock();
        }
    }
}
