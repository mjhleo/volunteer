package com.netmarch.volunteer.service.util;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import lombok.extern.slf4j.Slf4j;

import java.awt.image.BufferedImage;
import java.util.EnumMap;

/**
 * @author Mr.Wei on 2018/6/29
 */
@Slf4j
public class QRCodeUtil {

    protected  static final EnumMap<EncodeHintType, Object> HINTS = new EnumMap<>(EncodeHintType.class);

    static {
        //编码
        HINTS.put(EncodeHintType.CHARACTER_SET, "UTF-8");
        //边框距
        HINTS.put(EncodeHintType.MARGIN, 0);
    }

    private QRCodeUtil() {
        throw new IllegalStateException();
    }

    public static BufferedImage getQrCode(String url, Integer width) {
        try {
            BitMatrix bitMatrix = new MultiFormatWriter().encode(url, BarcodeFormat.QR_CODE, width, width, HINTS);
            return MatrixToImageWriter.toBufferedImage(bitMatrix);
        } catch (WriterException e) {
            log.error("生成二维码失败：{}", e.getMessage());
        }
        return null;
    }
}
