package com.netmarch.volunteer.service.util;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.ExcessiveAttemptsException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.subject.Subject;

import com.netmarch.volunteer.exception.ApiException;
import com.netmarch.volunteer.shiro.CaptchaUsernamePasswordToken;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Mr.Wei  2020/9/22 2:06 下午
 */
@Slf4j
public class SubjectUtil {
    private SubjectUtil() {
        throw new IllegalStateException();
    }

    public static Subject getSubject(String username, String password, String realmName) {
        CaptchaUsernamePasswordToken token = new CaptchaUsernamePasswordToken(username, password.toCharArray(), realmName);
        Subject subject = SecurityUtils.getSubject();
        try {
            subject.login(token);
        } catch (UnknownAccountException e) {
            log.warn("getSubject UnknownAccountException exception: {}", e.getMessage());
            throw new ApiException(ApiException.E_LOGIN_UNKNOWN_ACCOUNT);
        } catch (IncorrectCredentialsException e) {
            log.warn("getSubject IncorrectCredentialsException exception: {}", e.getMessage());
            throw new ApiException(ApiException.E_LOGIN_INCORRECT_CREDENTIAL);
        } catch (LockedAccountException e) {
            log.warn("getSubject LockedAccountException exception: {}", e.getMessage());
            throw new ApiException(ApiException.E_LOGIN_LOCKED_ACCOUNT);
        } catch (ExcessiveAttemptsException e) {
            log.warn("getSubject ExcessiveAttemptsException exception: {}", e.getMessage());
            throw new ApiException(ApiException.E_LOGIN_EXCESSIVE_ATTEMPTS);
        } catch (AuthenticationException e) {
            log.warn("getSubject AuthenticationException exception: {}", e.getMessage());
            throw new ApiException(ApiException.E_LOGIN_EXCEPTION);
        }
        return subject;
    }
}
