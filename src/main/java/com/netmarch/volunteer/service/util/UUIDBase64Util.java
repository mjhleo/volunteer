package com.netmarch.volunteer.service.util;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.UUID;

/**
 * @author Mr.Wei on 2018/3/30.
 */
public class UUIDBase64Util {

    private UUIDBase64Util() {
        throw new IllegalStateException("UUIDBase64Util class");
    }

    private static final  Base64.Decoder DECODER = Base64.getDecoder();
    private static final Base64.Encoder ENCODER = Base64.getEncoder();

    public static String generate() {
        UUID uuid = UUID.randomUUID();
        byte[] uuidArr = asByteArray(uuid);
        String s = ENCODER.encodeToString(uuidArr);
        return s.replace("=", "").replace("/", "_").replace("+", "-");
    }

    private static byte[] asByteArray(UUID uuid) {
        long msb = uuid.getMostSignificantBits();
        long lsb = uuid.getLeastSignificantBits();
        byte[] buffer = new byte[16];

        for (int i = 0; i < 8; i++) {
            buffer[i] = (byte) (msb >>> 8 * (7 - i));
        }
        for (int i = 8; i < 16; i++) {
            buffer[i] = (byte) (lsb >>> 8 * (7 - i));
        }
        return buffer;
    }

    public static String decode(String str) {
        try {
            return new String(DECODER.decode(str + "==="), StandardCharsets.UTF_8);
        } catch (Exception e) {
            return null;
        }
    }
}
