package com.netmarch.volunteer.service.util;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.AntPathMatcher;

/**
 * @author Mr.Wei on 2019-05-07.
 */
public class PathUtil {
    private PathUtil() {
        throw new IllegalStateException();
    }

    public static Boolean match(String[] patterns, String uri) {
        for (String pattern : patterns) {
            if (match(pattern, uri)) {
                return true;
            }
        }
        return false;
    }

    public static Boolean match(String pattern, String uri) {
        if (StringUtils.isNotEmpty(pattern)) {
            String[] paramArrays = pattern.split(",");
            for (String param : paramArrays) {
                if (new AntPathMatcher().match(param, uri)) {
                    return true;
                }
            }
        }
        return false;
    }

}
