package com.netmarch.volunteer.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.netmarch.volunteer.config.Constants;
import com.netmarch.volunteer.dao.PersonnelMapper;
import com.netmarch.volunteer.domain.Personnel;
import com.netmarch.volunteer.domain.Register;
import com.netmarch.volunteer.domain.command.PersonnelQueryCommand;
import com.netmarch.volunteer.domain.dto.PersonnelDTO;
import com.netmarch.volunteer.domain.dto.UserInfoDTO;
import com.netmarch.volunteer.domain.enumeration.RoleType;
import com.netmarch.volunteer.exception.ApiException;
import com.netmarch.volunteer.service.util.JwtUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.Date;
import java.util.List;

@Service
@Slf4j
public class PersonnelService {

    @Autowired
    private PersonnelMapper personnelMapper;

    @Autowired
    private UserService userService;

    public PageInfo<Personnel> getPersonnelList(PersonnelQueryCommand personnelQueryCommand, int pageNum, int pageSize,String token) {
        PageHelper.startPage(pageNum, pageSize);
        Example example = new Example(Personnel.class);
        Example.Criteria criteria = example.createCriteria();

        if (StringUtils.isNotBlank(personnelQueryCommand.getName())) {
            criteria.andLike("name", "%" + personnelQueryCommand.getName() + "%");
        }
        if (StringUtils.isNotBlank(personnelQueryCommand.getMajor())) {
            criteria.andLike("major", "%" + personnelQueryCommand.getMajor() + "%");
        }
        if (StringUtils.isNotBlank(personnelQueryCommand.getAddress())) {
            criteria.andLike("address", "%" + personnelQueryCommand.getAddress() + "%");
        }
        if (StringUtils.isNotBlank(personnelQueryCommand.getIdnumber())) {
            criteria.andEqualTo("idnumber", personnelQueryCommand.getIdnumber());
        }
        criteria.andEqualTo("isOnJob", personnelQueryCommand.getIsOnJob());

        RoleType type = JwtUtil.getRoleType(token);
        if(type.equals(RoleType.ADMIN)){
            //获取单位信息
            UserInfoDTO userInfoDTO =  userService.getUserInfo(token);
            criteria.andEqualTo("arrangeUnit", userInfoDTO.getDeptId());

        }else{
            if (StringUtils.isNotBlank(personnelQueryCommand.getTrainingUnit())) {
                criteria.andEqualTo("arrangeUnit", personnelQueryCommand.getTrainingUnit());
            }
        }



        if (StringUtils.isNotBlank(personnelQueryCommand.getPhone())) {
            criteria.andEqualTo("phone", personnelQueryCommand.getPhone());
        }

        // 入职时间
        if (personnelQueryCommand.getOnJobStartDate()!=null) {
            criteria.andGreaterThanOrEqualTo("hireDate", personnelQueryCommand.getOnJobStartDate());
        }
        if (personnelQueryCommand.getOnJobEndDate()!=null) {
            criteria.andLessThanOrEqualTo("hireDate", personnelQueryCommand.getOnJobEndDate());
        }

        //退岗时间
        if (personnelQueryCommand.getExitStartDate()!=null) {
            criteria.andGreaterThanOrEqualTo("exitDate", personnelQueryCommand.getExitStartDate());
        }
        if (personnelQueryCommand.getExitEndDate()!=null) {
            criteria.andLessThanOrEqualTo("exitDate", personnelQueryCommand.getExitEndDate());
        }

        return new PageInfo<>(this.personnelMapper.selectByExample(example));
    }

    public void toExitJob(PersonnelDTO personnelDTO, String token) {
        List<Long> ids = personnelDTO.getIds();
        String username = JwtUtil.getUsername(token);
        personnelDTO.setLastModifiedDate(new Date());
        personnelDTO.setLastModifiedBy(username);
        personnelDTO.setExitDate(new Date());
        personnelDTO.setIsOnJob(false);
        if(StringUtils.isBlank(personnelDTO.getExitReason())){
            throw new ApiException(ApiException.EXIT_REASON);
        }
        int i = personnelMapper.batchExitJob(ids,personnelDTO);
    }

    public void toOnJob(PersonnelDTO personnelDTO, String token) {
        List<Long> ids = personnelDTO.getIds();
        String username = JwtUtil.getUsername(token);
        personnelDTO.setLastModifiedDate(new Date());
        personnelDTO.setLastModifiedBy(username);
        personnelDTO.setHireDate((new Date()));
        personnelDTO.setIsOnJob(true);
        if(StringUtils.isBlank(personnelDTO.getHireReason())){
            throw new ApiException(ApiException.HIRE_REASON);
        }
        int i = personnelMapper.batchOnJob(ids,personnelDTO);
    }

    @Transactional(rollbackFor = Exception.class)
    public void arrange(PersonnelDTO personnelDTO, String token) {

        String username = JwtUtil.getUsername(token);
        List<Long> ids = personnelDTO.getIds();
        if(personnelDTO.getArrangeUnit()==null){
            throw new ApiException(ApiException.UNIT);
        }
        personnelDTO.setLastModifiedBy(username);
        personnelDTO.setLastModifiedDate(new Date());
        int i = personnelMapper.arrange(ids,personnelDTO);

    }
}
