package com.netmarch.volunteer.service;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.netmarch.volunteer.dao.MenuMapper;
import com.netmarch.volunteer.dao.UserMapper;
import com.netmarch.volunteer.domain.Menu;
import com.netmarch.volunteer.domain.User;
import com.netmarch.volunteer.domain.dto.MenuDTO;
import com.netmarch.volunteer.domain.enumeration.MenuType;
import com.netmarch.volunteer.mapper.MenuDTOMapper;
import com.netmarch.volunteer.service.util.JwtUtil;


/**
 * @author zhuguanming
 * @date 2020/10/12
 */
@Service
public class MenuService {

    private MenuMapper menuMapper;
    private UserMapper userMapper;

    public List<MenuDTO> getMenus(String token, MenuType type) {
        Long roleId = null;
        if (StringUtils.isNotEmpty(token)) {
            User user = userMapper.selectByPrimaryKey(JwtUtil.getId(token));
            roleId = user.getRoleId();
        }
        List<Menu> menuList = Objects.equals(MenuType.SYSTEM, type) ? menuMapper.selectMenus(roleId) : menuMapper.selectVolunteerMenus();
        List<MenuDTO> allMenus = MenuDTOMapper.INSTANCE.domain2DTOList(menuList);
        List<MenuDTO> topMenus = getChildrenMenus(allMenus, Long.valueOf("0"));
        return getTreeMenus(allMenus, topMenus);
    }

    /**
     * 获取当前菜单的子菜单集合
     */
    private List<MenuDTO> getChildrenMenus(List<MenuDTO> menuList, Long parentId) {
        return menuList.stream().filter(menu -> Objects.equals(menu.getPid(), parentId)).sorted(Comparator.comparing(MenuDTO::getSort)).collect(Collectors.toList());
    }

    /**
     * 获取树形菜单
     */
    private List<MenuDTO> getTreeMenus(List<MenuDTO> allMenus, List<MenuDTO> curMenus) {
        return curMenus.stream().map(
                menu -> {
                    menu.setChildren(getTreeMenus(allMenus, getChildrenMenus(allMenus, menu.getId())));
                    return menu;
                }
        ).collect(Collectors.toList());
    }

    @Autowired
    public void setMenuMapper(MenuMapper menuMapper) {
        this.menuMapper = menuMapper;
    }

    @Autowired
    public void setUserMapper(UserMapper userMapper) {
        this.userMapper = userMapper;
    }
}
