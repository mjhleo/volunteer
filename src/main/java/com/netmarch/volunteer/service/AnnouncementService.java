package com.netmarch.volunteer.service;

import java.util.Objects;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import com.netmarch.volunteer.dao.AnnouncementMapper;
import com.netmarch.volunteer.domain.Announcement;
import com.netmarch.volunteer.domain.command.AnnouncementQueryCommand;
import com.netmarch.volunteer.domain.dto.AnnouncementDTO;
import com.netmarch.volunteer.domain.dto.SystemMessageDTO;
import com.netmarch.volunteer.domain.enumeration.AnnouncementType;
import com.netmarch.volunteer.domain.enumeration.MessageEnum;
import com.netmarch.volunteer.mapper.AnnouncementDTOMapper;

import tk.mybatis.mapper.entity.Example;

/**
 * @author huangguochen
 * @create 2022/4/28 16:31
 */
@Service
public class AnnouncementService{

    @Resource
    private AnnouncementMapper announcementMapper;

    @Resource
    private SystemMessageService systemMessageService;

    public PageInfo<Announcement> getAnnouncements(AnnouncementQueryCommand query, int pageNum, int pageSize) {
        PageMethod.startPage(pageNum, pageSize);
        Example example = new Example(Announcement.class);
        example.setOrderByClause("created_date desc");
        Example.Criteria criteria = example.createCriteria();
        if (StringUtils.isNotBlank(query.getTitle())) {
            criteria.andLike("title", "%" + query.getTitle() + "%");
        }
        if (Objects.nonNull(query.getType())) {
            criteria.andEqualTo("type", query.getType());
        }
        return new PageInfo<>(this.announcementMapper.selectByExample(example));
    }

    @Transactional(rollbackFor = Exception.class)
    public void addOrUpdate(AnnouncementDTO dto) {
        Announcement announcement = AnnouncementDTOMapper.instance.dTO2Domain(dto);
        if (Objects.isNull(announcement.getId())) {
            if (Objects.equals(AnnouncementType.NOTICE, dto.getType())) {
                Long messageId = systemMessageService
                    .sendNoticeMessage(new SystemMessageDTO().init(MessageEnum.EPIDEMIC_NOTICE, new String[] { dto.getContent() }));
                announcement.setMessageId(messageId);
            }
            this.announcementMapper.insert(announcement);
        } else {
            this.announcementMapper.updateByPrimaryKeySelective(announcement);
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public void delete(Long id) {
        Announcement announcement = this.announcementMapper.selectByPrimaryKey(id);
        if (Objects.nonNull(announcement.getMessageId())) {
            systemMessageService.delete(announcement.getMessageId());
        }
        this.announcementMapper.deleteByPrimaryKey(id);
    }
}
