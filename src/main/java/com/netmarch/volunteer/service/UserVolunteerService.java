package com.netmarch.volunteer.service;

import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.netmarch.volunteer.config.Constants;
import com.netmarch.volunteer.dao.UserVolunteerMapper;
import com.netmarch.volunteer.domain.UserVolunteer;
import com.netmarch.volunteer.domain.dto.UserVolunteerLoginDTO;
import com.netmarch.volunteer.domain.dto.UserVolunteerRegisterDTO;
import com.netmarch.volunteer.domain.enumeration.Platform;
import com.netmarch.volunteer.exception.ApiException;
import com.netmarch.volunteer.exception.ErrorVM;

import lombok.extern.log4j.Log4j2;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.weekend.WeekendSqls;

/**
 * @author huangguochen
 * @create 2022/4/27 16:45
 */
@Service
@Log4j2
public class UserVolunteerService{

    @Resource
    private UserVolunteerMapper userVolunteerMapper;
    @Resource
    private UserService userService;

    public void create(UserVolunteer volunteer) {
        String salt = new SecureRandomNumberGenerator().nextBytes().toHex();
        volunteer.setSalt(salt);
        volunteer.setPassword(new Md5Hash(StringUtils.isEmpty(volunteer.getPassword()) ? Constants.DEFAULT_ZERO : volunteer.getPassword(), salt, 2).toHex());
        volunteer.setCreatedDate(new Date());
        volunteer.setIdcard(volunteer.getIdcard().toUpperCase());
        this.userVolunteerMapper.insert(volunteer);
    }

    public void update(UserVolunteer volunteer) {
        if (Objects.nonNull(volunteer.getId())) {
            this.userVolunteerMapper.updateByPrimaryKeySelective(volunteer);
        }
    }
    
    public UserVolunteer findByUserName(String userName) {
        Example example = new Example.Builder(UserVolunteer.class).where(WeekendSqls.<UserVolunteer> custom().andEqualTo(UserVolunteer::getUsername, userName))
            .build();
        List<UserVolunteer> userVolunteers = userVolunteerMapper.selectByExample(example);
        if (CollectionUtils.isNotEmpty(userVolunteers) && userVolunteers.size() > 1) {
            log.info("志愿者手机号：{} 重复，请检查数据库",  userName);
            throw new ApiException(new ErrorVM(-10001, "用户信息错误"));
        }
        return CollectionUtils.isNotEmpty(userVolunteers) ? userVolunteers.get(0) : null;
    }

    @Transactional(rollbackFor = Exception.class)
    public void insert(UserVolunteerRegisterDTO dto) {
        UserVolunteer volunteer = findByUserName(dto.getUsername());
        if (Objects.isNull(volunteer)) {
            create(UserVolunteer.builder().idcard(dto.getIdcard()).username(dto.getUsername()).password(dto.getPassword()).build());
        } else if (StringUtils.isNotBlank(volunteer.getUserPassword())) {
            throw new ApiException(new ErrorVM(-10002, "手机号已被注册"));
        } else {
            String salt = new SecureRandomNumberGenerator().nextBytes().toHex();
            String password = new Md5Hash(dto.getPassword(), salt, 2).toHex();
            update(UserVolunteer.builder().id(volunteer.getId()).userPassword(password).userSalt(salt).build());
        }
    }


    public boolean checkVerify(String verifyInput, HttpSession session) {
        boolean b = true;
        try {

            //从session中获取随机数
            String inputStr = verifyInput;

            String random = (String) session.getAttribute("RANDOMVALIDATECODEKEY");

            if (random == null || "".equals(random) || !random.equalsIgnoreCase(inputStr)) {
                b = false;
            }
        } catch (Exception e) {
            b = false;
        }
        return b;
    }

    public String login(UserVolunteerLoginDTO dto, HttpServletRequest request, String jwtSecret, Integer tokenExpire) {
        if (!checkVerify(dto.getCode(), request.getSession())) {
            throw new ApiException(new ErrorVM(-10003, "验证码错误"));
        }
        return userService.volounterLogin(dto, Platform.VOLUNTEER_PC, jwtSecret, tokenExpire);
    }
}
