package com.netmarch.volunteer.service;

import java.util.Objects;

import javax.annotation.Resource;

import org.apache.commons.lang3.BooleanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.netmarch.volunteer.dao.AttendanceMapper;
import com.netmarch.volunteer.dao.MissionMapper;
import com.netmarch.volunteer.domain.Attendance;
import com.netmarch.volunteer.domain.Mission;
import com.netmarch.volunteer.domain.dto.AttendanceEndDTO;
import com.netmarch.volunteer.domain.dto.AttendanceStartDTO;
import com.netmarch.volunteer.exception.ApiException;
import com.netmarch.volunteer.exception.ErrorVM;
import com.netmarch.volunteer.service.util.JwtUtil;

/**
 * @author huangguochen
 * @create 2022/4/28 21:50
 */
@Service
public class AttendanceService{

    @Resource
    private AttendanceMapper attendanceMapper;
    @Resource
    private MissionMapper missionMapper;

    @Transactional(rollbackFor = Exception.class)
    public void addAttendance(AttendanceStartDTO dto, String token) {
        Mission mission = this.missionMapper.selectByPrimaryKey(dto.getMissionId());
        if (BooleanUtils.isTrue(mission.getState())) {
            throw new ApiException(new ErrorVM(-10001, "任务已完成"));
        }
        String phone = JwtUtil.getUsername(token);
        if (!Objects.equals(phone, mission.getPhone())) {
            throw new ApiException(new ErrorVM(-10002, "任务接收者手机号与当前用户手机号不一致"));
        }
        Attendance attendance = Attendance.builder().startAddress(dto.getStartAddress()).startDate(
                dto.getStartDate()).startUri(dto.getStartUri()).build();

        int i = attendanceMapper.insertSelective(attendance);

        if (i > 0) {
            missionMapper.updateByPrimaryKeySelective(new Mission().attendance(attendance.getId(), dto.getMissionId()));
        }

    }

    @Transactional(rollbackFor = Exception.class)
    public void finishAttendance(AttendanceEndDTO dto) {
        Attendance attendance = Attendance.builder().id(dto.getAttendanceId()).endAddress(dto.getEndAddress()).endDate(dto.getEndDate()).endUri(dto.getEndUri())
            .build();
        int i = attendanceMapper.updateByPrimaryKeySelective(attendance);
        if (i > 0) {
            missionMapper.updateByPrimaryKeySelective(new Mission().finish(dto.getMissionId()));
        }

    }



}
