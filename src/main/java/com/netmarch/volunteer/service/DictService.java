package com.netmarch.volunteer.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import com.netmarch.volunteer.dao.DictMapper;
import com.netmarch.volunteer.domain.Dict;
import com.netmarch.volunteer.domain.dto.DictDTO;
import com.netmarch.volunteer.domain.enumeration.DictType;
import com.netmarch.volunteer.exception.ApiException;
import com.netmarch.volunteer.mapper.DictDTOMapper;
import com.netmarch.volunteer.service.util.PageInfoUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.weekend.WeekendSqls;

import java.util.List;
import java.util.Objects;

/**
 * @author Mr.Wei  2021/6/11 9:01 上午
 */
@Service
public class DictService {

    @Autowired
    private DictMapper dictMapper;

    public PageInfo<DictDTO> getDicts(String code, String name, DictType type, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        Example example = new Example(Dict.class);
        Example.Criteria criteria = example.createCriteria();
        if (StringUtils.isNotBlank(code)) {
            criteria.andLike("code", "%" + code + "%");
        }
        if (StringUtils.isNotBlank(name)) {
            criteria.andLike("name", "%" + name + "%");
        }
        if (null != type) {
            criteria.andEqualTo("type", type);
        }
        List<Dict> dicts = this.dictMapper.selectByExample(example);
        List<DictDTO> dtos = DictDTOMapper.instance.dictsToDictDTOs(dicts);
        return PageInfoUtil.pageInfo2PageInfoVo(new PageInfo<>(dicts), dtos);
    }

    public List<Dict> getDictList(String type) {


        List<Dict> dicts = dictMapper.findByType(type);
        return dicts;
    }


    @Transactional(rollbackFor = Exception.class)
    public void addOrUpdate(Dict dict) {
        String code = dict.getCode();
        if (Objects.isNull(dict.getId())) {
            if (isExistByCode(code)) {
                throw new ApiException(ApiException.E_CODE_REPEAT);
            }
            this.dictMapper.insert(dict);
        } else {
            Long id = dict.getId();
            Dict oldDict = this.dictMapper.selectByPrimaryKey(id);
            boolean equals = Objects.equals(dict.getCode(), oldDict.getCode());
            if (!equals && isExistByCode(code)) {
                throw new ApiException(ApiException.E_CODE_REPEAT);
            }
            this.dictMapper.updateByPrimaryKeySelective(dict);
        }
    }

    public boolean isExistByCode(String code) {
        Example example = new Example.Builder(Dict.class).where(
                WeekendSqls.<Dict>custom().andEqualTo(Dict::getCode, code)).build();
        int i = this.dictMapper.selectCountByExample(example);
        return i > 0;
    }

    @Autowired
    public DictService(DictMapper dictMapper) {
        this.dictMapper = dictMapper;
    }


}
