package com.netmarch.volunteer.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.netmarch.volunteer.dao.DeptMapper;
import com.netmarch.volunteer.domain.Dept;
import com.netmarch.volunteer.domain.Dict;
import com.netmarch.volunteer.service.util.PageInfoUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;


@Service
public class DeptService {
    @Autowired
    private DeptMapper deptMapper;

    public PageInfo<Dept> getDepts(String name, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        Example example = new Example(Dept.class);
        Example.Criteria criteria = example.createCriteria();
        if (StringUtils.isNotBlank(name)) {
            criteria.andLike("name", "%" + name + "%");
        }
        List<Dept> depts = deptMapper.selectByExample(example);
        return PageInfoUtil.listToPageInfo(depts,pageNum,pageSize);
    }

    public void update(Dept dept) {
        deptMapper.updateByPrimaryKeySelective(dept);
    }
}
