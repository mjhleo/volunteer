package com.netmarch.volunteer.service;

import java.util.Date;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.netmarch.volunteer.dao.LoginLogMapper;
import com.netmarch.volunteer.domain.LoginLog;
import com.netmarch.volunteer.domain.command.LoginLogCommand;
import com.netmarch.volunteer.service.util.DateFormatUtil;
import com.netmarch.volunteer.service.util.HttpContextUtils;
import com.netmarch.volunteer.service.util.IPUtils;
import com.netmarch.volunteer.service.util.JwtUtil;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.weekend.WeekendSqls;

/**
 * @author zhuguanming
 * @date 2020/12/7
 */
@Service
public class LoginLogService {

    private LoginLogMapper loginLogMapper;

    public PageInfo<LoginLog> getLoginLogs(LoginLogCommand loginLogCommand) {
        PageHelper.startPage(loginLogCommand.getPageNum(), loginLogCommand.getPageSize(),"login_date desc");
        return new PageInfo<>(loginLogMapper.getLoginLogs(loginLogCommand));
    }

    public void insert(String token) {
        LoginLog loginLog = getTodayLoginLog(JwtUtil.getId(token));
        if (Objects.nonNull(loginLog)) {
            this.loginLogMapper.updateByPrimaryKeySelective(new LoginLog().update(loginLog.getId(), new Date()));
        } else {
            this.loginLogMapper.insert(new LoginLog(JwtUtil.getId(token), JwtUtil.getPlatform(token), JwtUtil.getName(token), JwtUtil.getUsername(token),
                IPUtils.getIpAddr(HttpContextUtils.getHttpServletRequest())));
        }
    }

    /**
     * 获取今天登录日志
     *
     * @param id id
     * @return {@link Long}
     */
    public LoginLog getTodayLoginLog(Long id) {
        Date start = DateFormatUtil.getStartDateForDay(new Date());
        Date end = DateFormatUtil.getEndDateForDay(new Date());
        Example example = new Example.Builder(LoginLog.class)
                .where(WeekendSqls.<LoginLog> custom().andEqualTo(LoginLog::getUserId, id).andBetween(LoginLog::getLoginDate, start, end)).build();
        return this.loginLogMapper.selectOneByExample(example);
    }

    @Autowired
    public void setLoginLogMapper(LoginLogMapper loginLogMapper) {
        this.loginLogMapper = loginLogMapper;
    }
}
