package com.netmarch.volunteer.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.netmarch.volunteer.dao.OperationLogMapper;
import com.netmarch.volunteer.domain.OperationLog;
import com.netmarch.volunteer.domain.command.OperationLogCommand;
import com.netmarch.volunteer.service.util.DateFormatUtil;

/**
 * @author zhuguanming
 * @date 2020/12/8
 */
@Service
public class OperationLogService {

    private OperationLogMapper operationLogMapper;

    public PageInfo<OperationLog> getOperationLogs(OperationLogCommand command) {
        PageHelper.startPage(command.getPageNum(), command.getPageSize(),"operation_date desc");
        return new PageInfo<>(operationLogMapper.getOperationLogs(command));
    }

    @Scheduled(cron = "0 10 2 ? * *")
    public void delete() {
        this.operationLogMapper.deleteLogExceed7Day(DateFormatUtil.getStartAddDayDate(-7));
    }

    @Autowired
    public void setOperationLogMapper(OperationLogMapper operationLogMapper) {
        this.operationLogMapper = operationLogMapper;
    }
}
