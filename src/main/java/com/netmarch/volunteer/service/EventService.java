package com.netmarch.volunteer.service;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import com.netmarch.volunteer.dao.EventDeptMapper;
import com.netmarch.volunteer.dao.EventMapper;
import com.netmarch.volunteer.domain.Event;
import com.netmarch.volunteer.domain.EventDept;
import com.netmarch.volunteer.domain.command.EventQueryCommand;
import com.netmarch.volunteer.domain.dto.EventDTO;
import com.netmarch.volunteer.exception.ApiException;
import com.netmarch.volunteer.exception.ErrorVM;
import com.netmarch.volunteer.mapper.EventDTOMapper;

import tk.mybatis.mapper.entity.Example;

/**
 * @author huangguochen
 * @create 2022/4/27 22:04
 */
@Service
public class EventService{

    @Resource
    private EventMapper eventMapper;

    @Resource
    private EventDeptMapper eventDeptMapper;

    
    public PageInfo<Event> getEvents(EventQueryCommand query, int pageNum, int pageSize) {
        PageMethod.startPage(pageNum, pageSize);
        Example example = new Example(Event.class);
        example.setOrderByClause("release_date desc");
        Example.Criteria criteria = example.createCriteria();
        if (StringUtils.isNotBlank(query.getName())) {
            criteria.andLike("name", "%" + query.getName() + "%");
        }
        if (Objects.nonNull(query.getStartDate())) {
            criteria.andGreaterThanOrEqualTo("releaseDate", query.getStartDate());
        }
        if (Objects.nonNull(query.getEndDate())) {
            criteria.andLessThanOrEqualTo("releaseDate", query.getEndDate());
        }
        return new PageInfo<>(this.eventMapper.selectByExample(example));
    }

    @Transactional(rollbackFor = Exception.class)
    public void addOrUpdate(EventDTO dto) {
        Event event = EventDTOMapper.instance.eventDTOToEvent(dto);
        if (Objects.isNull(event.getId())) {
            this.eventMapper.insert(event);
            insertDept(dto.getDepts(), event.getId());
        } else {
            int i = this.eventMapper.updateByPrimaryKeySelective(event);
            if (i > 0) {
                eventDeptMapper.delete(new EventDept(event.getId()));
                insertDept(dto.getDepts(), event.getId());
            }
        }
    }

    private void insertDept(List<EventDept> depts, Long eventId) {
        Event event = this.eventMapper.selectByPrimaryKey(eventId);
        Long assureNumber = event.getAssureNumber();
        Set<Long> list = new HashSet<>(depts.size());
        Long total = 0L;
        if (CollectionUtils.isNotEmpty(depts)) {
            for (EventDept dept : depts) {
                dept.init(eventId);
                list.add(dept.getDeptId());
                total += dept.getChargeNumber();
            }
            if (list.size() != depts.size()) {
                throw new ApiException(new ErrorVM(-10001, "部门id不能重复"));
            }
            if (total > assureNumber) {
                throw new ApiException(new ErrorVM(-10002, "各单位负责人数之和不能大于目前事件的保障人数"));
            }
            eventDeptMapper.insertList(depts);
        }
    }


    public void delete(Long id) {
        this.eventMapper.deleteByPrimaryKey(id);
        eventDeptMapper.delete(new EventDept(id));
    }

    public EventDTO getEventDTO(Long id) {
        Event event = this.eventMapper.selectByPrimaryKey(id);
        if (Objects.isNull(event)) {
            throw new ApiException(new ErrorVM(-10001, "事件不存在"));
        }
        List<EventDept> depts = this.eventDeptMapper.select(new EventDept(id));
        return EventDTOMapper.instance.eventToEventDTO(event).depts(depts);
    }
}
