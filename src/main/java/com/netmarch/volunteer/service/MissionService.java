package com.netmarch.volunteer.service;

import java.util.Date;
import java.util.List;

import com.netmarch.volunteer.domain.command.MissionEventCommand;
import com.netmarch.volunteer.domain.dto.MissionCountDtoByEvent;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.excel.EasyExcel;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.netmarch.volunteer.dao.MissionMapper;
import com.netmarch.volunteer.domain.Mission;
import com.netmarch.volunteer.domain.command.MissionCommand;
import com.netmarch.volunteer.domain.dto.MissionCountDTO;
import com.netmarch.volunteer.domain.dto.MissionImportDTO;
import com.netmarch.volunteer.domain.dto.MissionQryDto;
import com.netmarch.volunteer.exception.ApiException;
import com.netmarch.volunteer.service.util.PageInfoUtil;
import com.netmarch.volunteer.service.util.UploadDataListener;

import lombok.extern.slf4j.Slf4j;
import tk.mybatis.mapper.entity.Example;

@Slf4j
@Service
public class MissionService {

    @Autowired
    private MissionMapper missionMapper;


    public void importPerson(String token, Boolean validated, Long eventId, Long deptId, MultipartFile file) {
        if (file == null) {
            throw new ApiException(ApiException.E_FILE_UPLOAD_ERROR);
        }
        try {
            Example example = new Example(Mission.class);
            Example.Criteria criteria = example.createCriteria();
            criteria.andEqualTo("event_id",eventId);
            criteria.andEqualTo("dept_id",deptId);
            missionMapper.deleteByExample(example);
            //导入
            EasyExcel.read(file.getInputStream(), MissionImportDTO.class, new UploadDataListener(token, validated,eventId)).sheet().headRowNumber(1).doRead();
        } catch (Exception e) {
            log.error(ExceptionUtils.getStackTrace(e));
            if (e instanceof ApiException) {
                ApiException exception = (ApiException) e;
                throw exception;
            } else {
                throw new ApiException(ApiException.E_EXCEL_UPLOAD_ERROR);
            }
        }
    }

    public PageInfo<MissionQryDto> getList(MissionCommand missionCommand, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<MissionQryDto> list = missionMapper.getList(missionCommand);
        return PageInfoUtil.listToPageInfo(list,pageNum,pageSize);
    }

    public List<MissionCountDTO> getCountByDept(Long deptId) {
        return missionMapper.getCountByDept(deptId);
    }

    public List<MissionCountDTO> getCountByEventDate(Date startTime, Date endTime) {
        return missionMapper.getCountByEventDate(startTime, endTime);
    }

    public PageInfo<MissionCountDtoByEvent> getMissionCountByEvent(MissionEventCommand command, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<MissionCountDtoByEvent> list = missionMapper.getMissionCountByEvent(command);
        return PageInfoUtil.listToPageInfo(list,pageNum,pageSize);
    }
}
