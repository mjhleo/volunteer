package com.netmarch.volunteer.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.commons.math3.exception.NullArgumentException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.netmarch.volunteer.config.Constants;
import com.netmarch.volunteer.dao.PersonnelMapper;
import com.netmarch.volunteer.dao.RegisterMapper;
import com.netmarch.volunteer.domain.Personnel;
import com.netmarch.volunteer.domain.Register;
import com.netmarch.volunteer.domain.command.RegisterQueryCommand;
import com.netmarch.volunteer.domain.dto.RegisterArrangeDTO;
import com.netmarch.volunteer.domain.dto.RegisterCheckDTO;
import com.netmarch.volunteer.domain.dto.RegisterTrainCheckDTO;
import com.netmarch.volunteer.domain.dto.RegisterTrainImportDTO;
import com.netmarch.volunteer.domain.dto.UserInfoDTO;
import com.netmarch.volunteer.domain.enumeration.RoleType;
import com.netmarch.volunteer.exception.ApiException;
import com.netmarch.volunteer.service.util.ImportExcelUtil;
import com.netmarch.volunteer.service.util.JwtUtil;

import lombok.extern.slf4j.Slf4j;
import tk.mybatis.mapper.entity.Example;

@Service
@Slf4j
public class RegisterService {

    @Autowired
    private RegisterMapper registerMapper;

    @Autowired
    private PersonnelMapper personnelMapper;

    @Autowired
    private UserService userService;

    @Autowired
    private SystemMessageService systemMessageService;

    public PageInfo<Register> getRegisters(RegisterQueryCommand registerQueryCommand, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        Example example = new Example(Register.class);
        Example.Criteria criteria = example.createCriteria();
        if (StringUtils.isNotBlank(registerQueryCommand.getName())) {
            criteria.andLike("name", "%" + registerQueryCommand.getName() + "%");
        }
        if (StringUtils.isNotBlank(registerQueryCommand.getMajor())) {
            criteria.andLike("major", "%" + registerQueryCommand.getMajor() + "%");
        }
        if (StringUtils.isNotBlank(registerQueryCommand.getAddress())) {
            criteria.andLike("address", "%" + registerQueryCommand.getAddress() + "%");
        }
        if (StringUtils.isNotBlank(registerQueryCommand.getIdnumber())) {
            criteria.andEqualTo("idnumber", registerQueryCommand.getIdnumber());
        }
        if (StringUtils.isNotBlank(registerQueryCommand.getTrainingUnit())) {
            criteria.andEqualTo("trainingUnit", registerQueryCommand.getTrainingUnit());
        }
        if (StringUtils.isNotBlank(registerQueryCommand.getCheckState())) {
            if(registerQueryCommand.getCheckState().equals(Constants.UNCHECK)){//传2过来是未审核

                criteria.andIsNull("checkState");
            }else{
                criteria.andEqualTo("checkState", registerQueryCommand.getCheckState());
            }

        }
        if (StringUtils.isNotBlank(registerQueryCommand.getPhone())) {
            criteria.andEqualTo("phone", registerQueryCommand.getPhone());
        }
        return new PageInfo<>(this.registerMapper.selectByExample(example));
    }

    public boolean isExistRegister(String idnumber){
        Example example = new Example(Register.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("idnumber", idnumber);
        List<Register> register = registerMapper.selectByExample(example);
        if(register!=null&&register.size()>0){//已存在
            return true;
        }else{
            return false;
        }
    }

    public boolean isExistRegisterByPhone(String phone){
        Example example = new Example(Register.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("phone", phone);
        List<Register> register = registerMapper.selectByExample(example);
        if(register!=null&&register.size()>0){//已存在
            return true;
        }else{
            return false;
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public void toRegister(Register register) {
        //如果是注册
        if (Objects.isNull(register.getId())) {
            //判断身份证号是否已存在且手机号是否存在
            if(!isExistRegister(register.getIdnumber())&&!isExistRegisterByPhone(register.getPhone())){
                register.setCreatedBy(register.getPhone());
                this.registerMapper.insert(register);
            }else{
                throw new ApiException(ApiException.E_REGISTER_EXIST);
            }

        } else {//如果是修改
            Long id = register.getId();
            Register oldRegister = this.registerMapper.selectByPrimaryKey(id);
            boolean equals = Objects.equals(register.getIdnumber(), oldRegister.getIdnumber());
            if (equals) {//如果身份证号没变
                this.registerMapper.updateByPrimaryKeySelective(register);
            }else{
                //如果身份证号变了
                //判断身份证号是否已存在
                if (Objects.equals(Constants.NOT_PASS, register.getCheckState())) {
                    register.setCheckState(null);
                }
                if(!isExistRegister(register.getIdnumber())){
                    this.registerMapper.updateByPrimaryKey(register);
                }else{
                    throw new ApiException(ApiException.E_REGISTER_EXIST);
                }
            }
        }
    }

    public void check(RegisterCheckDTO registerCheckDTO,String token) {
        //审核操作
        if(registerCheckDTO.getCheckState()!=Constants.PASS&&registerCheckDTO.getCheckState()!=Constants.NOT_PASS){
            throw new ApiException(ApiException.PASS_OR_NOT);
        }

        String username = JwtUtil.getUsername(token);
        Register register = this.registerMapper.selectByPrimaryKey(registerCheckDTO.getId());
        register.setCheckAdvice(registerCheckDTO.getCheckAdvice());
        register.setCheckState(registerCheckDTO.getCheckState());
        register.setCheckDate(new Date());
        register.setCheckBy(username);
        this.registerMapper.updateByPrimaryKeySelective(register);
        //发送消息通知
        systemMessageService.sendRegisterMessage(registerCheckDTO);
    }


    public void batchCheck(RegisterCheckDTO registerCheckDTO,String token) {

        if(registerCheckDTO.getCheckState()!=Constants.PASS&&registerCheckDTO.getCheckState()!=Constants.NOT_PASS){
            throw new ApiException(ApiException.PASS_OR_NOT);
        }

        List<Long> ids = registerCheckDTO.getIds();
        String username = JwtUtil.getUsername(token);
        registerCheckDTO.setCheckBy(username);
        registerCheckDTO.setCheckDate(new Date());
        int i = registerMapper.batchUpdateCheckState(ids,registerCheckDTO);
        //发送消息通知
        systemMessageService.sendRegisterMessage(registerCheckDTO);
    }


    //审核通过人员才能 岗前培训，分配至对应单位
    @Transactional(rollbackFor = Exception.class)
    public void arrange(RegisterArrangeDTO registerArrangeDTO,String token) {

        String username = JwtUtil.getUsername(token);
        List<Long> ids = registerArrangeDTO.getIds();
        if(registerArrangeDTO.getTrainingUnit()==null){
            throw new ApiException(ApiException.UNIT);
        }
        if(ids!=null&&ids.size()>0&&registerArrangeDTO!=null){
            for(Long id:ids){
                //1.查询审核是否通过
                Register register = this.registerMapper.selectByPrimaryKey(id);
                if(register!=null&& Constants.PASS==register.getCheckState()){
                    //如果通过，根据id去更新信息
                    register.setTrainingUnit(registerArrangeDTO.getTrainingUnit());
                    register.setTrainingBy(username);
                    register.setTrainingDate(new Date());
                    this.registerMapper.updateByPrimaryKeySelective(register);
                }else{
                    //审核未通过的不能进行分配
                    throw new ApiException(ApiException.E_NOT_PASS);
                }
            }
        }

    }


    public PageInfo<Register> getTrainingRegisters(RegisterQueryCommand registerQueryCommand, int pageNum, int pageSize,String token) {
        PageHelper.startPage(pageNum, pageSize);
        Example example = new Example(Register.class);
        Example.Criteria criteria = example.createCriteria();
        if (StringUtils.isNotBlank(registerQueryCommand.getName())) {
            criteria.andLike("name", "%" + registerQueryCommand.getName() + "%");
        }
        if (StringUtils.isNotBlank(registerQueryCommand.getTrainingContent())) {
            criteria.andLike("trainingContent", "%" + registerQueryCommand.getTrainingContent() + "%");
        }

        if (StringUtils.isNotBlank(registerQueryCommand.getTrainingState())) {
            if(registerQueryCommand.getTrainingState().equals(Constants.UNCHECK)){//传2过来是未审核

                criteria.andIsNull("trainingState");
            }else{
                criteria.andEqualTo("trainingState", registerQueryCommand.getTrainingState());
            }

        }

        if (StringUtils.isNotBlank(registerQueryCommand.getIdnumber())) {
            criteria.andEqualTo("idnumber", registerQueryCommand.getIdnumber());
        }
        if (StringUtils.isNotBlank(registerQueryCommand.getPhone())) {
            criteria.andEqualTo("phone", registerQueryCommand.getPhone());
        }

        RoleType type = JwtUtil.getRoleType(token);
        if(type.equals(RoleType.ADMIN)){
            //获取单位信息
            UserInfoDTO userInfoDTO =  userService.getUserInfo(token);
            //根据userId查询user信息

            criteria.andEqualTo("trainingUnit", userInfoDTO.getDeptId());

        }else{
            if (StringUtils.isNotBlank(registerQueryCommand.getTrainingUnit())) {
                criteria.andEqualTo("trainingUnit", registerQueryCommand.getTrainingUnit());
            }else{
                criteria.andIsNotNull("trainingUnit");
            }
        }

        return new PageInfo<>(this.registerMapper.selectByExample(example));
    }

    public Register findDetailByCondition( RegisterQueryCommand registerQueryCommand ) {
        Example example = new Example(Register.class);
        Example.Criteria criteria = example.createCriteria();

        if (StringUtils.isNotBlank(registerQueryCommand.getPhone())) {
            criteria.andEqualTo("phone", registerQueryCommand.getPhone());
        }

        return  registerMapper.selectOneByExample(example);
    }

    @Transactional(rollbackFor = Exception.class)
    public void trainCheck(RegisterTrainCheckDTO registerTrainCheckDTO,String token) {
        //培训审核操作

        if(registerTrainCheckDTO.getTrainingState()!=Constants.PASS&&registerTrainCheckDTO.getTrainingState()!=Constants.NOT_PASS){
            throw new ApiException(ApiException.PASS_OR_NOT);
        }


        String username = JwtUtil.getUsername(token);

        Register register = this.registerMapper.selectByPrimaryKey(registerTrainCheckDTO.getId());

        register.setTrainingAdvice(registerTrainCheckDTO.getTrainingAdvice());
        register.setTrainingContent(registerTrainCheckDTO.getTrainingContent());
        register.setTrainingBy(username);
        register.setTrainingDate(new Date());

        if(register.getTrainingState()!=null&&register.getTrainingState()==Constants.PASS){//如果培训审核已经有值了，并且值为1（审核通过），则不需要insert到人员表
            register.setTrainingState(registerTrainCheckDTO.getTrainingState());
        }else{
            register.setTrainingState(registerTrainCheckDTO.getTrainingState());

            if(registerTrainCheckDTO.getTrainingState()==Constants.PASS){

                Personnel personnel = new Personnel();
                personnel.setName(register.getName());
                personnel.setGender(register.getGender());
                personnel.setIdnumber(register.getIdnumber());
                personnel.setWorkUnit(register.getWorkUnit());
                personnel.setGraduate(register.getGraduate());
                personnel.setMajor(register.getMajor());
                personnel.setHealthTitle(register.getHealthTitle());
                personnel.setGraduateImg(register.getGraduateImg());
                personnel.setHealthImg(register.getHealthImg());
                personnel.setAddress(register.getAddress());
                personnel.setPhone(register.getPhone());
                personnel.setContactPerson(register.getContactPerson());
                personnel.setContactPhone(register.getContactPhone());

                personnel.setTrainingUnit(register.getTrainingUnit());
                personnel.setArrangeUnit(register.getTrainingUnit());

                personnel.setBank(register.getBank());
                personnel.setBankNumber(register.getBankNumber());
                personnel.setHireDate(new Date());
                personnel.setIsOnJob(true);
                personnel.setCreatedBy(username);
                personnel.setCreatedDate(new Date());
                personnelMapper.insert(personnel);
            }


        }
        this.registerMapper.updateByPrimaryKeySelective(register);

    }


    @Transactional(rollbackFor = Exception.class)
    public void batchTrainCheck(RegisterTrainCheckDTO registerTrainCheckDTO,String token) {

        if(registerTrainCheckDTO.getTrainingState()!=Constants.PASS&&registerTrainCheckDTO.getTrainingState()!=Constants.NOT_PASS){
            throw new ApiException(ApiException.PASS_OR_NOT);
        }
        String username = JwtUtil.getUsername(token);
        List<Long> ids = registerTrainCheckDTO.getIds();

        //根据ids 查询register 的list
        List<Register> registerList = registerMapper.selectRegisterListByIds(ids);
        List<Personnel> personnelList = new ArrayList<>();
        for(Register register:registerList){
            if(register.getTrainingState()!=null&&register.getTrainingState()==Constants.PASS){//如果培训审核已经有值了，并且值为1（审核通过），则不需要insert到人员表
                register.setTrainingState(registerTrainCheckDTO.getTrainingState());
            }else{
                register.setTrainingState(registerTrainCheckDTO.getTrainingState());

                if(registerTrainCheckDTO.getTrainingState()==Constants.PASS){
                    Personnel personnel = new Personnel();
                    personnel.setName(register.getName());
                    personnel.setGender(register.getGender());
                    personnel.setIdnumber(register.getIdnumber());
                    personnel.setWorkUnit(register.getWorkUnit());
                    personnel.setGraduate(register.getGraduate());
                    personnel.setMajor(register.getMajor());
                    personnel.setHealthTitle(register.getHealthTitle());
                    personnel.setGraduateImg(register.getGraduateImg());
                    personnel.setHealthImg(register.getHealthImg());
                    personnel.setAddress(register.getAddress());
                    personnel.setPhone(register.getPhone());
                    personnel.setContactPerson(register.getContactPerson());
                    personnel.setContactPhone(register.getContactPhone());

                    personnel.setTrainingUnit(register.getTrainingUnit());
                    personnel.setArrangeUnit(register.getTrainingUnit());
                    personnel.setBank(register.getBank());
                    personnel.setBankNumber(register.getBankNumber());
                    personnel.setHireDate(new Date());
                    personnel.setIsOnJob(true);
                    personnel.setCreatedBy(username);
                    personnel.setCreatedDate(new Date());
                    personnelList.add(personnel);
                }


            }
        }
        registerTrainCheckDTO.setTrainingBy(username);
        registerTrainCheckDTO.setTrainingDate(new Date());
        int i = registerMapper.batchUpdateTrainCheckState(ids,registerTrainCheckDTO);


        if(personnelList.size()>0){
            // TODO 将数据insert到个人信息表
            personnelMapper.insertList(personnelList);
        }
    }

    public void importTrainInfor(MultipartFile file) {

        if (file == null) {
            throw new ApiException(ApiException.E_FILE_UPLOAD_ERROR);
        }
        String filename = file.getOriginalFilename();
        if (filename == null) {
            throw new ApiException(ApiException.E_FILE_UPLOAD_ERROR);
        }
        String a = "";
        try {
            // 调用解析文件方法
            a = parseRowCell(filename, file.getInputStream());


        } catch (Exception e) {
            log.error(ExceptionUtils.getStackTrace(e));
            if (e instanceof ApiException) {
                ApiException exception = (ApiException) e;
                throw exception;
            } else {
                throw new ApiException(ApiException.E_EXCEL_UPLOAD_ERROR);
            }
        }
    }


    /**
     * 解析文件中的数据
     */
    private String parseRowCell(String filename, InputStream is) {
        List<RegisterTrainImportDTO> list = new ArrayList<>();

        //查询所有身份证号

        //查询所有联系方式
        try {
            Workbook workbook = null;
            // 判断excel的后缀，不同的后缀用不同的对象去解析
            // xls是低版本的Excel文件
//            if (filename.endsWith(".xls")) {
//                workbook = new HSSFWorkbook(is);
//            }
//            // xlsx是高版本的Excel文件
//            if (filename.endsWith(".xlsx")) {
//                workbook = new XSSFWorkbook(is);
//            }
            workbook = WorkbookFactory.create(is);
            if (workbook == null) {
                throw new NullArgumentException();
            }

            // 取到excel 中的第一张工作表
            Sheet sheet = workbook.getSheetAt(0);
            if (sheet == null) {
                throw new NullArgumentException();
            }

            // 工作表中第一行是表头，不获取，从第二行开始获取
            for (int rowNum = 1; rowNum <= sheet.getLastRowNum(); rowNum++) {
                // 获取到这一行的数据
                Row row = sheet.getRow(rowNum);
                if (row == null) {
                    continue;
                }

                RegisterTrainImportDTO registerTrainImportDTO = new RegisterTrainImportDTO();


                if (row.getCell(1) != null) {
                    registerTrainImportDTO.setPhone((String)ImportExcelUtil.getCellValue(row.getCell(1)));
                }

                if (row.getCell(2) != null) {
                    registerTrainImportDTO.setIdnumber((String)ImportExcelUtil.getCellValue(row.getCell(2)));
                }

                registerTrainImportDTO.setTrainingContent((String)ImportExcelUtil.getCellValue(row.getCell(3)));

                String trainingResult = (String)ImportExcelUtil.getCellValue(row.getCell(4));
                if(StringUtils.isNotBlank(trainingResult)){
                    if(canparseint(trainingResult)){
                        if(trainingResult.equals("0")||trainingResult.equals("1")){
                            registerTrainImportDTO.setTrainingResult(Integer.valueOf(trainingResult));
                        }else{
                            throw new ApiException(ApiException.TRAINING_RESULT);
                        }

                    }else{
                        throw new ApiException(ApiException.TRAINING_RESULT);
                    }
                }else{
                    registerTrainImportDTO.setTrainingResult(0);
                }



                registerTrainImportDTO.setTrainingAdvice((String)ImportExcelUtil.getCellValue(row.getCell(5)));

                String trainingState = (String)ImportExcelUtil.getCellValue(row.getCell(6));
                if(StringUtils.isNotBlank(trainingState)){
                    if(canparseint(trainingState)){
                        if(trainingState.equals("0")||trainingState.equals("1")){
                            registerTrainImportDTO.setTrainingAdvice(trainingState);
                        }else{
                            throw new ApiException(ApiException.TRAINING_STATE);
                        }

                    }else{
                        throw new ApiException(ApiException.TRAINING_STATE);
                    }
                }


                if(StringUtils.isNotBlank(registerTrainImportDTO.getPhone())&&StringUtils.isNotBlank(registerTrainImportDTO.getIdnumber())){

                    list.add(registerTrainImportDTO);
                }
            }

            int i = registerMapper.batchUpdateCheckResult(list);
            if(i>0){
                return "true";
            }
            return "false";
        } catch (IOException e) {
            return e.getMessage();
        }

    }

    public boolean canparseint(String str){
        if(str == null){ //验证是否为空
            return false;

        }
        return str.matches("\\d+"); //使用正则表达式判断该字符串是否为数字，第一个\是转义符，\d+表示匹配1个或 //多个连续数字，"+"和"*"类似，"*"表示0个或多个

    }

    public void batchTrainResult(RegisterTrainCheckDTO registerTrainCheckDTO, String token) {
        List<Long> ids = registerTrainCheckDTO.getIds();
        String username = JwtUtil.getUsername(token);
        int i = registerMapper.batchTrainResult(ids,registerTrainCheckDTO);
    }
}
