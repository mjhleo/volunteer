package com.netmarch.volunteer.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.netmarch.volunteer.dao.EntityLogMapper;
import com.netmarch.volunteer.domain.EntityLog;
import com.netmarch.volunteer.domain.enumeration.EntityAction;

import lombok.extern.slf4j.Slf4j;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.weekend.WeekendSqls;

/**
 * <操作日志>
 *
 * @author Huangguochen
 * @create 2020/11/24 14:52
 */
@Service
@Slf4j
public class EntityLogService {
    private EntityLogMapper entityLogMapper;

    @Autowired
    public EntityLogService(EntityLogMapper entityLogMapper) {
        this.entityLogMapper = entityLogMapper;
    }

    public void insert(EntityLog entityLog) {
        this.entityLogMapper.insertSelective(entityLog);
    }

    public PageInfo<EntityLog> getEntityLogs(EntityAction action, Date startDate, Date endDate, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        Example.Builder builder = new Example.Builder(EntityLog.class);
        WeekendSqls<EntityLog> custom = WeekendSqls.custom();
        if (null != action) {
            custom.andEqualTo(EntityLog::getAction, action);
        }
        if (null != startDate && null != endDate) {
            custom.andBetween(EntityLog::getTimestamp, startDate, endDate);
        }
        return new PageInfo<>(this.entityLogMapper.selectByExample(builder.where(custom).orderByDesc("timestamp").build()));
    }

    @Scheduled(cron = "0 10 2 ? * *")
    public void delete() {
        this.entityLogMapper.deleteLogExceed7Day();
    }
}
