package com.netmarch.volunteer.service;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import com.netmarch.volunteer.config.Constants;
import com.netmarch.volunteer.dao.MenuMapper;
import com.netmarch.volunteer.dao.PermissionMapper;
import com.netmarch.volunteer.dao.RoleMapper;
import com.netmarch.volunteer.dao.UserMapper;
import com.netmarch.volunteer.dao.UserVolunteerMapper;
import com.netmarch.volunteer.domain.Role;
import com.netmarch.volunteer.domain.User;
import com.netmarch.volunteer.domain.command.UserQueryCommand;
import com.netmarch.volunteer.domain.dto.*;
import com.netmarch.volunteer.domain.enumeration.Platform;
import com.netmarch.volunteer.domain.enumeration.RoleType;
import com.netmarch.volunteer.exception.ApiException;
import com.netmarch.volunteer.mapper.UserDTOMapper;
import com.netmarch.volunteer.service.util.JwtUtil;
import com.netmarch.volunteer.service.util.SubjectUtil;

import lombok.extern.slf4j.Slf4j;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.weekend.WeekendSqls;

/**
 * @author Mr.Wei  2020/9/22 11:07 上午
 */
@Service
@Slf4j
public class UserService {
    private StringRedisTemplate redisTemplate;
    private UserMapper userMapper;
    private PermissionMapper permissionMapper;
    private MenuMapper menuMapper;
    private RoleMapper roleMapper;
    private LoginLogService loginLogService;
    private UserVolunteerMapper userVolunteerMapper;

    public PageInfo<User> getUsers(UserQueryCommand userQueryCommand, Integer pageNum, Integer pageSize) {
        PageMethod.startPage(pageNum, pageSize);
        Example example = new Example(User.class);
        Example.Criteria criteria = example.createCriteria();
        if (StringUtils.isNotBlank(userQueryCommand.getName())) {
            criteria.andLike("name", "%" + userQueryCommand.getName() + "%");
        }
        if (StringUtils.isNotBlank(userQueryCommand.getUsername())) {
            criteria.andLike("username", "%" + userQueryCommand.getUsername() + "%");
        }
        if (userQueryCommand.getActivated() != null) {
            criteria.andEqualTo("activated", userQueryCommand.getActivated());
        }
        return new PageInfo<>(this.userMapper.selectByExample(example));
    }

    public UserInfoDTO getUserInfo(String token) {
        User user = userMapper.selectByUserID(JwtUtil.getId(token));
        String username = JwtUtil.getUsername(token);
        String key = JwtUtil.getPlatform(token).getTokenKey() + username;
        TokenDTO tokenAndAuthsDTO = JSON.parseObject(this.redisTemplate.opsForValue().get(key), TokenDTO.class);
        assert tokenAndAuthsDTO != null;
        return UserDTOMapper.instance.userToUserInfoDTO(user).auth(tokenAndAuthsDTO.getAuth());
    }

    
    public List<User> getAllActiveUsers() {
        Example example = new Example.Builder(User.class).where(WeekendSqls.<User> custom().andEqualTo(User::getActivated, Boolean.TRUE)).build();
        List<User> users = this.userMapper.selectByExample(example);
        return users.stream().filter(user -> {
            Long roleId = user.getRoleId();
            Role role = roleMapper.selectByPrimaryKey(roleId);
            if (Objects.isNull(role)) {
                return false;
            }
            return Objects.equals(RoleType.USER, role.getType());
        }).collect(Collectors.toList());
    }

    @Transactional(rollbackFor = Exception.class)
    public void addOrUpdateUser(UserDTO userDTO) {
        User user = UserDTOMapper.instance.userDTOToUser(userDTO);
        String username = user.getUsername();
        if (Objects.isNull(user.getId())) {
            if (isExistByName(username)) {
                throw new ApiException(ApiException.E_ACCOUNT_EXIST);
            }
            this.setPassword(user, Constants.DEFAULT_ZERO);
            userMapper.insert(user.secure(Boolean.FALSE));
        } else {
            Long id = user.getId();
            User oldUser = this.userMapper.selectByPrimaryKey(id);
            boolean equals = Objects.equals(username, oldUser.getUsername());
            if (!equals && isExistByName(username)) {
                throw new ApiException(ApiException.E_ACCOUNT_EXIST);
            }
            this.userMapper.updateByPrimaryKeySelective(user);
        }
    }

    /**
     * 重置密码
     */
    public void reset(long id) {
        User user = new User().id(id).secure(Boolean.FALSE);
        this.setPassword(user, Constants.DEFAULT_ZERO);
        this.userMapper.updateByPrimaryKeySelective(user);
    }

    /**
     * 设置密码
     */
    public void setPassword(User user, String password) {
        String salt = new SecureRandomNumberGenerator().nextBytes().toHex();
        user.initPassword(salt, new Md5Hash(password, salt, 2).toHex());
    }

    public void password(long id, PasswordDTO passwordDTO) {
        if (!passwordDTO.getNewPassword().equals(passwordDTO.getRePassword())) {
            throw new ApiException(ApiException.E_PASSWORD_NOT_SAME);
        }
        User user = new User().id(id).secure(Boolean.TRUE);
        this.setPassword(user, passwordDTO.getNewPassword());
        this.userMapper.updateByPrimaryKeySelective(user);
    }

    public void modifyPassword(PasswordModifyDTO modifyPasswordDTO, String token) {
        User user = userMapper.selectByPrimaryKey(JwtUtil.getId(token));
        if (!StringUtils.equals(user.getPassword(), new Md5Hash(modifyPasswordDTO.getOriginPassword(), user.getSalt(), 2).toHex())) {
            throw new ApiException(ApiException.E_LOGIN_INCORRECT_ORIPASSWORD);
        }
        this.password(user.getId(), new PasswordDTO(modifyPasswordDTO.getNewPassword(), modifyPasswordDTO.getRePassword()));
        this.redisTemplate.delete(Platform.PC.getTokenKey());
    }

    public String login(LoginDTO loginDTO, String jwtSecret, Integer tokenExpire) {
        Subject subject = SubjectUtil.getSubject(loginDTO.getUsername(), loginDTO.getPassword(), loginDTO.getPlatform().getRealmName());
        if (!subject.isAuthenticated()) {
            log.warn("UserService.login subject isAuthenticated is false");
            throw new ApiException(ApiException.E_LOGIN_NOT_AUTH);
        }
        return this.createToken(jwtSecret, tokenExpire, loginDTO.getPlatform());
    }

    public String volounterLogin(UserVolunteerLoginDTO loginDTO, Platform platform, String jwtSecret, Integer tokenExpire) {
        Subject subject = SubjectUtil.getSubject(loginDTO.getUsername(), loginDTO.getPassword(), platform.getRealmName());
        if (!subject.isAuthenticated()) {
            log.warn("UserService.login subject isAuthenticated is false");
            throw new ApiException(ApiException.E_LOGIN_NOT_AUTH);
        }
        return this.createToken(jwtSecret, tokenExpire, platform);
    }


    private String createToken(String jwtSecret, Integer tokenExpire, Platform platform) {
        UserShiroDTO user = (UserShiroDTO) SecurityUtils.getSubject().getPrincipals().getPrimaryPrincipal();
        Role role = this.roleMapper.selectByPrimaryKey(user.getRoleId());
        String accessToken = JwtUtil.create(user.getId(), platform, user.getName(), Objects.isNull(role) ? null : role.getType(), user.getName(), user.getUsername(), jwtSecret);
        this.redisTemplate.opsForValue().set(platform.getTokenKey() + user.getUsername(), JSON.toJSONString(new TokenDTO(accessToken, null, null)), tokenExpire, TimeUnit.DAYS);
        this.userMapper.updateByPrimaryKeySelective(new User().id(user.getId()).loginDate());
        this.loginLogService.insert(accessToken);
        return accessToken;
    }

    public boolean verifyToken(String token, String jwtSecret) {
        if (StringUtils.isBlank(token)) {
            return false;
        }
        if (!JwtUtil.verify(token, jwtSecret)) {
            return false;
        }
        String username = JwtUtil.getUsername(token);
        if (null == username) {
            return false;
        }
        String tokenDTO = this.getToken(username, JwtUtil.getPlatform(token));
        TokenDTO tokenAndAuthsDTO = JSON.parseObject(tokenDTO, TokenDTO.class);
        return tokenAndAuthsDTO != null && token.equals(tokenAndAuthsDTO.getToken());
    }

    private String getToken(String username, Platform platform) {
        return this.redisTemplate.opsForValue().get(platform.getTokenKey() + username);
    }

    public void logout(String token) {
        String username = JwtUtil.getUsername(token);
        String key = JwtUtil.getPlatform(token).getTokenKey() + username;
        this.redisTemplate.delete(key);
    }
    
    public boolean isExistByName(String username) {
        Example example = new Example.Builder(User.class).where(
                WeekendSqls.<User>custom().andEqualTo(User::getUsername, username)).build();
        int i = this.userMapper.selectCountByExample(example);
        return i > 0;
    }


    @Autowired
    public UserService(StringRedisTemplate redisTemplate, UserMapper userMapper, PermissionMapper permissionMapper, MenuMapper menuMapper,
                       RoleMapper roleMapper, LoginLogService loginLogService) {
        this.redisTemplate = redisTemplate;
        this.userMapper = userMapper;
        this.permissionMapper = permissionMapper;
        this.menuMapper = menuMapper;
        this.roleMapper = roleMapper;
        this.loginLogService = loginLogService;
    }
}
