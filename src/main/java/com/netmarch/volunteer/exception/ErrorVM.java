package com.netmarch.volunteer.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @author Mr.Wei on 2018/4/8.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ErrorVM implements Serializable {
    private static final long serialVersionUID = 3946218681654376931L;
    private Integer code;
    private String message;
    private List<FieldErrorVM> fieldErrors;

    public ErrorVM(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
