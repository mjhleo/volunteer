package com.netmarch.volunteer.exception;

public class EmailAlreadyUsedException extends BadRequestAlertException { // NOSONAR squid:S110

    private static final long serialVersionUID = 1L;

    public EmailAlreadyUsedException() {
        super(ErrorConstants.EMAIL_ALREADY_USED_TYPE, "Email is already in use!", "userManagement", "emailexists");
    }
}
