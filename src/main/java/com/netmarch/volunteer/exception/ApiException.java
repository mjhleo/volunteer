package com.netmarch.volunteer.exception;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Mr.Wei on 2018/4/8.
 */
@Slf4j
public class ApiException extends RuntimeException {




    public ApiException(ErrorVM errorVM) {
        super(errorVM.getMessage());
        this.errorVM = errorVM;
    }

    @Getter
    private final ErrorVM errorVM;

    public static final ErrorVM E_REQUEST_PARAMETER_ERROR = new ErrorVM(-10000, "请求参数校验失败");

    public static final ErrorVM E_LOGIN_NOT_AUTH = new ErrorVM(-10001, "账号已锁定");
    public static final ErrorVM E_LOGIN_UNKNOWN_ACCOUNT = new ErrorVM(-10003, "账号不存在");
    public static final ErrorVM E_LOGIN_INCORRECT_CREDENTIAL = new ErrorVM(-10004, "手机号或密码错误");
    public static final ErrorVM E_LOGIN_INCORRECT_ORIPASSWORD = new ErrorVM(-10004, "原密码错误");
    public static final ErrorVM E_LOGIN_INCORRECT_CAPTCHA = new ErrorVM(-10004, "验证码错误");
    public static final ErrorVM E_LOGIN_LOCKED_ACCOUNT = new ErrorVM(-10005, "密码已锁定");
    public static final ErrorVM E_LOGIN_EXCESSIVE_ATTEMPTS = new ErrorVM(-10006, "错误次数过多");
    public static final ErrorVM E_LOGIN_EXCEPTION = new ErrorVM(-10007, "登录异常");
    public static final ErrorVM E_USER_CREATE_FAILURE = new ErrorVM(-10008, "账号创建失败");
    public static final ErrorVM E_PASSWORD_NOT_SAME = new ErrorVM(-10009, "两次密码不一致");
    public static final ErrorVM E_USER_EXIST = new ErrorVM(-10010, "用户名已存在");
    public static final ErrorVM E_ACCOUNT_EXIST = new ErrorVM(-10010, "账号已存在");
    public static final ErrorVM E_USER_PERMISSION_DENIED = new ErrorVM(-10010, "该账号权限不足");

    public static final ErrorVM E_REGISTER_EXIST = new ErrorVM(-10010, "用户已注册");
    public static final ErrorVM E_NOT_PASS = new ErrorVM(-10010, "审核未通过的人不能进行分配");



    public static final ErrorVM E_FILE_UPLOAD_NULL = new ErrorVM(-10100, "上传文件名称不能为空");
    public static final ErrorVM E_FILE_CREATE_ERROR = new ErrorVM(-10101, "文件夹创建失败");
    public static final ErrorVM E_FILE_UPLOAD_ERROR = new ErrorVM(-10102, "文件上传失败");
    public static final ErrorVM E_FILE_DOWNLOAD_ERROR = new ErrorVM(-10103, "文件下载失败");
    public static final ErrorVM E_FILE_UPLOAD_FILE_NULL = new ErrorVM(-10104, "上传文件Id不能为空");
    public static final ErrorVM E_API_GENERATE_CODE_ERROR = new ErrorVM(-10105, "生成二维码错误!");
    public static final ErrorVM E_FILE_DOWNLOAD_NULL = new ErrorVM(-10106, "要下载文件未找到");
    public static final ErrorVM E_FILE_EXCEED_ERROR = new ErrorVM(-10107, "单文件大小不能超过10M");
    public static final ErrorVM E_FILE_UPLOAD_EVENT_ID_NULL = new ErrorVM(-10108, "上传文件时，事件ID为空");

    public static final ErrorVM E_MAINTAIN_SYSTEM_NO_PROVIDER = new ErrorVM(-10201, "此运维服务单关联的系统暂未指定运维单位，请及时指定");
    public static final ErrorVM E_MAINTAIN_NO_PROVIDER = new ErrorVM(-10202, "此运维服务单还未指派运维单位");
    public static final ErrorVM E_MAINTAIN_STATE_PROVIDER_USER = new ErrorVM(-10203, "此运维服务单状态不为待处理或处理中状态，不予指派解决人");
    public static final ErrorVM E_MAINTAIN_STATE_EXIST_COMPLAINT = new ErrorVM(-10204, "此运维服务单已经投诉过，请勿重复投诉");
    public static final ErrorVM E_MAINTAIN_SYSTEM_OUT_TIME = new ErrorVM(-10205, "此运维服务单关联的系统运维有效期已失效，请及时更新");
    public static final ErrorVM E_MAINTAIN_STATE_CLOSED = new ErrorVM(-10206, "此运维服务单已关闭");

    public static final ErrorVM E_ASSETVM_APPLY_REPEAT = new ErrorVM(-10301, "申请或修改的虚拟机已存在并且在使用中，请勿重复申请或修改");
    public static final ErrorVM E_ASSETPORT_APPLY_REPEAT = new ErrorVM(-10302, "该虚拟机端口已经申请过，请勿重复申请");
    public static final ErrorVM E_ASSETPORT_IN_USE = new ErrorVM(-10303, "该虚拟机端口已经申请过并且在使用中，请勿重复申请或修改");
    public static final ErrorVM E_ASSETVM_STATE_CHECK = new ErrorVM(-10304, "申请或修改的虚拟机正在审核中，请勿重复申请或修改");
    public static final ErrorVM E_ASSET_VM_IP_REPEAT = new ErrorVM(-10305, "服务器IP地址已存在，请勿重复申请或修改");

    public static final ErrorVM E_MAINTAIN_DUTY_DATE_CONFLICT = new ErrorVM(-10401, "运维排班时间冲突");
    public static final ErrorVM E_MAINTAIN_REPOSITORY_ERROR = new ErrorVM(-10411, "知识库状态不是待发布状态");

    public static final ErrorVM E_EXCEL_UPLOAD_ERROR = new ErrorVM(-10501, "Excel表格导入出错，请及时联系管理员");
    public static final ErrorVM E_EXCEL_CREATE_ERROR = new ErrorVM(-10502, "Excel表格模板生成出错，请及时联系管理员");
    public static final ErrorVM E_EXCEL_DOWNLOAD_ERROR = new ErrorVM(-10502, "Excel表格模板下载出错，请及时联系管理员");
    public static final ErrorVM E_EXCEL_FOMAT_ERROR = new ErrorVM(-10503, "excel文件格式错误!");

    public static final ErrorVM E_MAINTAIN_PHONEBOOK_ERROR = new ErrorVM(-10601, "通讯录号码重复");

    public static final ErrorVM E_CUSTOMER_SYSTEM_NOT_FOUND_ERROR = new ErrorVM(-10701, "当前客户端未关联系统");

    public static final ErrorVM E_CODE_REPEAT = new ErrorVM(-10801, "代码已存在，请勿重复新增或修改");
    public static final ErrorVM E_VERSION_REPEAT = new ErrorVM(-10802, "版本号已存在，请勿重复新增或修改");
    public static final ErrorVM E_NURSE_CODE_REPEAT = new ErrorVM(-10803, "护士工号已存在，请勿重复新增或修改");
    public static final ErrorVM E_WARD_CODE_REPEAT = new ErrorVM(-10804, "病区代码已存在，请勿重复新增或修改");
    public static final ErrorVM E_DOCTOR_CODE_REPEAT = new ErrorVM(-10805, "医生工号已存在，请勿重复新增或修改");
    public static final ErrorVM E_ROOM_CODE_REPEAT = new ErrorVM(-10806, "房间代码已存在，请勿重复新增或修改");
    public static final ErrorVM E_BED_CODE_REPEAT = new ErrorVM(-10807, "床位代码已存在，请勿重复新增或修改");
    public static final ErrorVM E_BED_USED = new ErrorVM(-10808, "该床位已被停用或者占用");


    public static final ErrorVM E_REVIEWER_REPEAT = new ErrorVM(-10902, "当前审核类型下已有该审核人，请勿重复新增或修改");

    public static final ErrorVM E_PERSON_PERSON_TYPE_NULL = new ErrorVM(-11001, "接触者级别为空！");
    public static final ErrorVM E_PERSON_PARENT_ID_NULL = new ErrorVM(-11002, "上级接触人员为空！");
    public static final ErrorVM E_PERSON_DBDATA_NULL = new ErrorVM(-11003, "数据库已不存在该数据记录！");
    public static final ErrorVM E_PERSON_PARAMS_NULL = new ErrorVM(-11003, "数据库已不存在该数据记录！");
    public static final ErrorVM E_PERSON_TYPE_PARENT_NULL = new ErrorVM(-11004, "上级人员类型不存在");
    public static final ErrorVM E_PERSON_NULL = new ErrorVM(-11005, "人员id不存在");
    public static final ErrorVM E_PERSON_TYPE_NULL = new ErrorVM(-11006, "人员类型不存在");

    public static final ErrorVM E_STORAGE_NUMBER_REPEAT = new ErrorVM(-11007, "该试剂编号已入库");

    public static final ErrorVM E_STORAGE_NUMBER_ERROR = new ErrorVM(-11008, "该药店无该试剂编号的入库信息");

    public static final ErrorVM E_CHECKOUT_NUMBER_REPEAT = new ErrorVM(-11009, "该试剂编号已出库");

    public static final ErrorVM E_RESIDENT_REPEAT = new ErrorVM(-20001, "该用户信息已存在!");

    public static final ErrorVM E_CHECKOUT_NUMBER_ERROR = new ErrorVM(-11009, "该试剂编号不存在出库信息");

    public static final ErrorVM E_BIND_NUMBER_REPEAT = new ErrorVM(-11010, "该试剂编号已绑定");

    public static final ErrorVM E_BIND_NUMBER_ERROR = new ErrorVM(-11012, "绑定信息不存在");

    public static final ErrorVM E_TEST_NUMBER_REPEAT = new ErrorVM(-11013, "检测结果已存在");

    public static final ErrorVM TRAINING_RESULT = new ErrorVM(-11014, "培训结果请输入0或1");

    public static final ErrorVM TRAINING_STATE = new ErrorVM(-11018, "培训审核状态请输入0或1");

    public static final ErrorVM EXIT_REASON = new ErrorVM(-11015, "请输入退岗原因");

    public static final ErrorVM HIRE_REASON = new ErrorVM(-11016, "请输入复岗原因");

    public static final ErrorVM UNIT = new ErrorVM(-11017, "请选择分配的单位");

    public static final ErrorVM PASS_OR_NOT = new ErrorVM(-11019, "请选择是否通过审核");
}
