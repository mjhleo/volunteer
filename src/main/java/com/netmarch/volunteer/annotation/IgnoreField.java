package com.netmarch.volunteer.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @author xiehui on 2020/12/10
 * @desc 生成excel时是否忽略该字段
 */
@Target(FIELD)
@Retention(RUNTIME)
public @interface IgnoreField {
}
