package com.netmarch.volunteer.aop;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.netmarch.volunteer.dao.OperationLogMapper;
import com.netmarch.volunteer.domain.OperationLog;
import com.netmarch.volunteer.service.util.HttpContextUtils;
import com.netmarch.volunteer.service.util.IPUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Mr.Wei  2020/11/25 5:00 下午
 */
@Aspect
@Component
@Slf4j
public class ActionAspect {
    private OperationLogMapper operationLogMapper;

    @Pointcut("@annotation(com.netmarch.volunteer.aop.Action)")
    public void actionPointcut() {
        // Method is empty as this is just a Pointcut, the implementations are in the advices.
    }

    @Around("actionPointcut()")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        long beginTime = System.currentTimeMillis();
        Object result = joinPoint.proceed();
        long time = System.currentTimeMillis() - beginTime;
        saveOperationLog(beginTime,joinPoint, time);
        return result;
    }

    public void saveOperationLog(Long beginTime, ProceedingJoinPoint joinPoint, Long time) {
        OperationLog operationLog = new OperationLog();
        try {
            HttpServletRequest request = HttpContextUtils.getHttpServletRequest();
            String token = request.getHeader("Authorization");
            if (StringUtils.isEmpty(token)) {
                return;
            }
            MethodSignature signature = (MethodSignature) joinPoint.getSignature();
            Method method = signature.getMethod();
            Action action = method.getAnnotation(Action.class);
            //操作内容
            operationLog.setOperationContent(action.name());
            //请求方法和参数
            String className = joinPoint.getTarget().getClass().getName();
            String methodName = signature.getName();
            operationLog.setOperationMethod(className + "." + methodName + "()");
            operationLog.setRequestContent(Arrays.toString(joinPoint.getArgs()));
            operationLogMapper.insertSelective(operationLog.initLog(token, IPUtils.getIpAddr(request), new Date(beginTime), time));
        } catch (Exception exception) {
            log.error("saveOperationLog----->操作日志记录失败，OperationLog：{}，Exception：{}", operationLog, exception.getMessage());
        }
    }

    @Autowired
    public void setOperationLogMapper(OperationLogMapper operationLogMapper) {
        this.operationLogMapper = operationLogMapper;
    }
}
