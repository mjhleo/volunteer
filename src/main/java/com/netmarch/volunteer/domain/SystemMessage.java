package com.netmarch.volunteer.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.netmarch.volunteer.domain.enumeration.MessageEnum;
import com.netmarch.volunteer.domain.enumeration.SystemMessageState;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author huangguochen
 * @create 2022/4/28 13:11
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Table(name = "system_message")
@ApiModel("系统消息")
public class SystemMessage extends AbstractAuditingEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(value="标题")
    @Column(name = "title")
    private String title;

    @ApiModelProperty(value="消息类型")
    @Column(name = "message_type")
    private MessageEnum messageType;

    @ApiModelProperty(value="消息内容")
    @Column(name = "content")
    private String content;

    @ApiModelProperty(value="消息接收单位")
    @Column(name = "receive_unit_id")
    private Long receiveUnitId;

    @ApiModelProperty(value="消息接收人手机号")
    @Column(name = "receive_phone")
    private String receivePhone;

    @ApiModelProperty(value="消息接收状态")
    @Column(name = "message_state")
    private SystemMessageState messageState;

    public SystemMessage(Long id, SystemMessageState messageState) {
        this.id = id;
        this.messageState = messageState;
    }

    public SystemMessage state(SystemMessageState messageState) {
        this.messageState = messageState;
        return this;
    }

    public SystemMessage init() {
        this.setCreatedBy("system");
        this.setCreatedDate(new Date());
        this.setLastModifiedBy("system");
        this.setLastModifiedDate(new Date());
        return this;
    }
}