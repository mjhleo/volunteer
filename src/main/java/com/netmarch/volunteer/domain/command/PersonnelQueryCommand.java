package com.netmarch.volunteer.domain.command;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.netmarch.volunteer.domain.enumeration.FinishStatus;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PersonnelQueryCommand {

    @ApiModelProperty("姓名")
    private String name;

    @ApiModelProperty("身份证号")
    private String idnumber;

    @ApiModelProperty("联系方式")
    private String phone;

    @ApiModelProperty("所学专业")
    private String major;

    @ApiModelProperty("家庭住址")
    private String address;

    @ApiModelProperty("单位")
    private String trainingUnit;

    @ApiModelProperty("是否在职")
    private Integer isOnJob;

    @ApiModelProperty("入职开始时间")
    private Date onJobStartDate;

    @ApiModelProperty("入职结束时间")
    private Date onJobEndDate;

    @ApiModelProperty("退岗开始时间")
    private Date exitStartDate;

    @ApiModelProperty("退岗结束时间")
    private Date exitEndDate;

}
