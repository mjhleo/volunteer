package com.netmarch.volunteer.domain.command;

import java.util.Date;

import com.netmarch.volunteer.domain.enumeration.EntityAction;
import com.netmarch.volunteer.domain.enumeration.Platform;

import lombok.Data;

/**
 * @author Huangguochen
 * @create 2020/11/24 20:09
 */

@Data
public class EntityLogCommand {
    private EntityAction action;

    private Platform platform;

    private Date startDate;

    private Date endDate;
}
