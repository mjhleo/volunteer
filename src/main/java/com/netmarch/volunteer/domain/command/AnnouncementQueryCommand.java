package com.netmarch.volunteer.domain.command;

import com.netmarch.volunteer.domain.enumeration.AnnouncementType;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author huangguochen
 * @create 2022/4/28 16:50
 */

@Data
public class AnnouncementQueryCommand {

    @ApiModelProperty(value="标题")
    private String title;

    @ApiModelProperty(value="类型")
    private AnnouncementType type;

}
