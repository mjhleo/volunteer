package com.netmarch.volunteer.domain.command;

import com.netmarch.volunteer.domain.enumeration.FinishStatus;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RegisterQueryCommand {

    @ApiModelProperty("姓名")
    private String name;

    @ApiModelProperty("身份证号")
    private String idnumber;

    @ApiModelProperty("联系方式")
    private String phone;

    @ApiModelProperty("所学专业")
    private String major;

    @ApiModelProperty("家庭住址")
    private String address;

    @ApiModelProperty("审核状态")
    private String checkState;

    @ApiModelProperty("培训内容")
    private String trainingContent;

    @ApiModelProperty("培训的审核状态")
    private String trainingState;

    @ApiModelProperty("培训单位")
    private String trainingUnit;

    @ApiModelProperty("是否分配")
    private FinishStatus finishStatus;

}
