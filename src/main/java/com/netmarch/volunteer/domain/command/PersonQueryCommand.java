package com.netmarch.volunteer.domain.command;

import com.netmarch.volunteer.domain.enumeration.PersonStatus;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

/**
 * @author yh
 * @date 2022/1/17 13:20
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PersonQueryCommand {

    @ApiModelProperty("人员管控类型")
    private Long personTypeId;

    @ApiModelProperty("姓名")
    private String name;

    @ApiModelProperty("身份证号")
    private String identityNumber;

    @ApiModelProperty("关联事件")
    private Long eventId;

    @ApiModelProperty("待处理")
    private Boolean handle;

    @Enumerated(EnumType.STRING)
    @ApiModelProperty("状态,可选值TO_CONFIRMED, CENTRALIZED_ISOLATION, HOME_ISOLATION,HEALTH_MONITOR,REMOVE")
    private PersonStatus status;

    @ApiModelProperty("导入数据")
    private Boolean imported;

}
