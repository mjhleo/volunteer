package com.netmarch.volunteer.domain.command;

import com.netmarch.volunteer.domain.enumeration.MessageEnum;
import com.netmarch.volunteer.domain.enumeration.SystemMessageState;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author huangguochen
 * @create 2022/4/28 14:14
 */
@Data
public class SystemMessageQueryCommand {
    @ApiModelProperty(value="消息接收状态")
    private SystemMessageState messageState;
    @ApiModelProperty(value="消息类型(报名审核结果, 任务通知, 疫情防控)")
    private MessageEnum messageType;
    @ApiModelProperty(value="消息接收单位")
    private Long receiveUnitId;
    @ApiModelProperty(value="消息接收人手机号")
    private String receivePhone;
}
