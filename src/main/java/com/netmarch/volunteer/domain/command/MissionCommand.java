package com.netmarch.volunteer.domain.command;

import com.alibaba.excel.annotation.ExcelProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.validation.constraints.NotBlank;
import java.util.Date;



@Data
public class MissionCommand {

    @ApiModelProperty("姓名")
    private String name;

    @ApiModelProperty("联系电话")
    private String phone;

    @ApiModelProperty("事件编号")
    private Long eventId;

    @ApiModelProperty("单位编号")
    private Long deptId;

    @ApiModelProperty("单位名称")
    private String deptName;

    @ApiModelProperty("区镇编号")
    private String townId;

    @ApiModelProperty("区镇")
    private String town;

}
