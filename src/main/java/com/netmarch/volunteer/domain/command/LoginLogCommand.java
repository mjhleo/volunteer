package com.netmarch.volunteer.domain.command;

import java.util.Date;

import com.netmarch.volunteer.domain.enumeration.Platform;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zhuguanming
 * @date 2020/12/7
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("登陆日志列表查询条件")
public class LoginLogCommand {

    private Platform platform;

    private String name;

    private Date startDate;

    private Date endDate;

    private Integer pageNum;

    private Integer pageSize;

}
