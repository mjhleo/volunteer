package com.netmarch.volunteer.domain.command;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;


@Data
public class MissionEventCommand {


    @ApiModelProperty("事件编号")
    private Long eventId;

    @ApiModelProperty("单位编号")
    private Long deptId;

    @ApiModelProperty("单位名称")
    private String deptName;

    @ApiModelProperty("事件名称")
    private String eventName;


}
