package com.netmarch.volunteer.domain.command;

import java.util.Date;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Huangguochen
 * @create 2020/11/24 20:09
 */

@Data
public class DisinfectQueryCommand {

    @ApiModelProperty("事件")
    private Long eventId;

    @ApiModelProperty("消杀开始时间")
    private Date startDate;

    @ApiModelProperty("消杀结束时间")
    private Date endDate;

    @ApiModelProperty("被指派人")
    private Long userId;
}
