package com.netmarch.volunteer.domain.command;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Huangguochen
 * @create 2020/9/25 14:26
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserQueryCommand {

    @ApiModelProperty("用户名")
    private String name;

    @ApiModelProperty("账号")
    private String username;

    @ApiModelProperty("是否可用")
    private Boolean activated;

}
