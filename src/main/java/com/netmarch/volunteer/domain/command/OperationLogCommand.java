package com.netmarch.volunteer.domain.command;

import java.util.Date;

import com.netmarch.volunteer.domain.enumeration.Platform;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zhuguanming
 * @date 2020/12/8
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("操作日志查询条件")
public class OperationLogCommand {

    private Platform platform;

    private String name;

    private Date startDate;

    private Date endDate;

    private Integer pageNum;

    private Integer pageSize;
}
