package com.netmarch.volunteer.domain.command;

import java.util.Date;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Huangguochen
 * @create 2020/11/24 20:09
 */

@Data
public class CheckQueryCommand {

    @ApiModelProperty("事件")
    private Long eventId;

    @ApiModelProperty("报告上传开始时间")
    private Date startDate;

    @ApiModelProperty("报告上传结束时间")
    private Date endDate;

}
