package com.netmarch.volunteer.domain.command;

import java.util.Date;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Huangguochen
 * @create 2020/11/24 20:09
 */

@Data
public class EventQueryCommand {

    @ApiModelProperty("事件名称")
    private String name;

    @ApiModelProperty("事件发布开始时间")
    private Date startDate;

    @ApiModelProperty("事件发布结束时间")
    private Date endDate;
}
