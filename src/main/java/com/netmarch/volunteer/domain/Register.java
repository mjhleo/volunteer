package com.netmarch.volunteer.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Table(name = "register")
@ApiModel("注册表")
public class Register extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    //1.注册部分
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty("姓名")
    @Column(name = "name")
    private String name;

    @ApiModelProperty("性别")
    @Column(name = "gender")
    private Integer gender;

    @ApiModelProperty("身份证号")
    @Column(name = "idnumber")
    private String idnumber;

    @ApiModelProperty("工作单位")
    @Column(name = "work_unit")
    private String workUnit;

    @ApiModelProperty("毕业院校")
    @Column(name = "graduate")
    private String graduate;

    @ApiModelProperty("学位")
    @Column(name = "edu_degree")
    private String eduDegree;


    @ApiModelProperty("所学专业")
    @Column(name = "major")
    private String major;

    @ApiModelProperty("卫生技术职称")
    @Column(name = "health_title")
    private String healthTitle;

    @ApiModelProperty("毕业证图片")
    @Column(name = "graduate_img")
    private String graduateImg;

    @ApiModelProperty("卫生专业证书")
    @Column(name = "health_img")
    private String healthImg;


    @ApiModelProperty("毕业证附件")
    @Column(name = "graduate_file")
    private String graduateFile;

    @ApiModelProperty("卫生专业证书附件")
    @Column(name = "health_file")
    private String healthFile;

    @ApiModelProperty("家庭住址区镇")
    @Column(name = "address_area")
    private Integer addressArea;

    @ApiModelProperty("家庭住址详情")
    @Column(name = "address")
    private String address;

    @ApiModelProperty("联系方式")
    @Column(name = "phone")
    private String phone;

    @ApiModelProperty("紧急联系人")
    @Column(name = "contact_person")
    private String contactPerson;

    @ApiModelProperty("紧急联系方式")
    @Column(name = "contact_phone")
    private String contactPhone;

    @ApiModelProperty("开户行")
    @Column(name = "bank")
    private String bank;

    @ApiModelProperty("开户行卡号")
    @Column(name = "bank_number")
    private String bankNumber;

    @ApiModelProperty("是否已被其他区镇招募")
    @Column(name = "is_registered")
    private Boolean isRegistered;

    @ApiModelProperty("招募区镇")
    @Column(name = "area")
    private Integer area;

    @ApiModelProperty("备注(希望在哪个板块工作)")
    @Column(name = "remark")
    private String remark;


    @ApiModelProperty("是否年龄在18-50周岁")
    @Column(name = "is_in_age")
    private Boolean isInAge;

    @ApiModelProperty("是否身体健康且无传染性疾病")
    @Column(name = "is_health")
    private Boolean isHealth;

    @ApiModelProperty("是否已至少接种两针疫苗")
    @Column(name = "is_vaccination")
    private Boolean isVaccination;

    @ApiModelProperty("本人及共同居住人健康码为绿码")
    @Column(name = "is_green")
    private Boolean isGreen;

    @ApiModelProperty("大数据行程卡无市外旅居史以及本人48小时内核酸检测阴性")
    @Column(name = "is_negative")
    private Boolean isNegative;

    @ApiModelProperty("是否居住于服务地")
    @Column(name = "is_service_place")
    private Boolean isServicePlace;


    //2.审核部分
    @ApiModelProperty("报名审核状态")
    @Column(name = "check_state")
    private Integer checkState;

    @ApiModelProperty("报名审核意见")
    @Column(name = "check_advice")
    private String checkAdvice;

    @ApiModelProperty("报名审核人")
    @Column(name = "check_by")
    private String checkBy;

    @ApiModelProperty("报名审核时间")
    @Column(name = "check_date")
    private Date checkDate;


    //3.岗前培训部分
    @ApiModelProperty("所分配的单位")
    @Column(name = "training_unit")
    private Long trainingUnit;


    @ApiModelProperty("培训内容")
    @Column(name = "training_content")
    private String trainingContent;


    @ApiModelProperty("培训结果状态")
    @Column(name = "training_result")
    private Integer trainingResult;

    @ApiModelProperty("培训审核状态")
    @Column(name = "training_state")
    private Integer trainingState;

    @ApiModelProperty("培训审核意见")
    @Column(name = "training_advice")
    private String trainingAdvice;

    @ApiModelProperty("培训审核人")
    @Column(name = "training_by")
    private String trainingBy;

    @ApiModelProperty("培训审核时间")
    @Column(name = "training_date")
    private Date trainingDate;




}
