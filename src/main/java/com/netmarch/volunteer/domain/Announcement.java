package com.netmarch.volunteer.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.netmarch.volunteer.domain.enumeration.AnnouncementType;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author huangguochen
 * @create 2022/4/28 16:31
 */
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("公告")
@Table(name = "announcement")
public class Announcement extends AbstractAuditingEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(value="标题")
    @Column(name = "title")
    private String title;

    @ApiModelProperty(value="内容")
    @Column(name = "content")
    private String content;

    @ApiModelProperty(value="附件")
    @Column(name = "uri")
    private String uri;

    @ApiModelProperty(value="类型(通知、视频培训、现场培训)")
    @Column(name = "type")
    private AnnouncementType type;

    @Column(name = "message_id")
    private Long messageId;
}