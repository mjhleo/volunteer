package com.netmarch.volunteer.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.netmarch.volunteer.domain.enumeration.Platform;
import com.netmarch.volunteer.service.util.JwtUtil;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zhuguanming
 * @date 2020/12/8
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("操作日志")
@Table(name = "operation_log")
public class OperationLog {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty("平台")
    private Platform platform;

    @ApiModelProperty("单位Id")
    @Column(name = "unit_id")
    private Long unitId;

    @ApiModelProperty("单位名称")
    @Column(name = "unit_name")
    private String unitName;

    @ApiModelProperty("操作人Id")
    @Column(name = "user_id")
    private Long userId;

    @ApiModelProperty("姓名")
    private String name;

    @ApiModelProperty("账号")
    private String username;

    @ApiModelProperty("ip")
    private String ip;

    @ApiModelProperty("请求方法")
    @Column(name = "operation_method")
    private String operationMethod;

    @ApiModelProperty("操作日期")
    @Column(name = "operation_date")
    private Date operationDate;

    @ApiModelProperty("操作内容")
    @Column(name = "operation_content")
    private String operationContent;

    @ApiModelProperty("请求参数")
    @Column(name = "request_content")
    private String requestContent;

    @ApiModelProperty("执行时长(毫秒)")
    private Long time;

    public OperationLog initLog(String token, String ipAddr, Date date, Long time) {
        this.platform = JwtUtil.getPlatform(token);
        this.unitId = 0L;
        this.userId = JwtUtil.getId(token);
        this.name = JwtUtil.getName(token);
        this.username = JwtUtil.getUsername(token);
        this.ip = ipAddr;
        this.operationDate = date;
        this.time = time;
        return this;
    }
}
