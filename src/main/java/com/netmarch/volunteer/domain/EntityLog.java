package com.netmarch.volunteer.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.netmarch.volunteer.domain.enumeration.EntityAction;
import com.netmarch.volunteer.domain.enumeration.Platform;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * A EntityLog.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "entity_log")
@ApiModel("操作日志")
public class EntityLog  {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "ip")
    private String ip;

    @Column(name = "name")
    private String name;

    @Column(name = "platform")
    @Enumerated(EnumType.STRING)
    private Platform platform;

    @Column(name = "method")
    private String method;

    @Column(name = "action")
    @Enumerated(EnumType.STRING)
    private EntityAction action;

    @Column(name = "value")
    private String value;

    @Column(name = "waitTime")
    private Long waitTime;

    @Column(name = "timestamp")
    private Date timestamp;

    public EntityLog(String ip, String name, String method, EntityAction action, String value, Long waitTime) {
        this.ip = ip;
        this.name = name;
        this.method = method;
        this.action = action;
        this.value = value;
        this.waitTime = waitTime;
        this.timestamp = new Date();
    }

    public EntityLog(String ip, String name, String method, Platform platform, EntityAction action, String value, Long waitTime) {
        this.ip = ip;
        this.name = name;
        this.method = method;
        this.platform = platform;
        this.action = action;
        this.value = value;
        this.waitTime = waitTime;
        this.timestamp = new Date();
    }
}
