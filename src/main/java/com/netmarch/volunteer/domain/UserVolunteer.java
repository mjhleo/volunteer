package com.netmarch.volunteer.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

/**
 * @author huangguochen
 * @create 2022/4/27 16:45
 */
@Data
@Table(name = "user_volunteer")
@ApiModel("志愿者用户")
@Builder
public class UserVolunteer implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "role_id")
    private Long roleId;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "salt")
    private String salt;

    @Column(name = "idcard")
    private String idcard;

    @Column(name = "user_password")
    private String userPassword;

    @Column(name = "user_salt")
    private String userSalt;

    @Column(name = "created_date")
    @ApiModelProperty("创建时间")
    private Date createdDate;

}