package com.netmarch.volunteer.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Table(name = "personnel")
@ApiModel("人员表")
public class Personnel extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty("姓名")
    @Column(name = "name")
    private String name;

    @ApiModelProperty("性别")
    @Column(name = "gender")
    private Integer gender;

    @ApiModelProperty("身份证号")
    @Column(name = "idnumber")
    private String idnumber;

    @ApiModelProperty("工作单位")
    @Column(name = "work_unit")
    private String workUnit;

    @ApiModelProperty("毕业院校")
    @Column(name = "graduate")
    private String graduate;

    @ApiModelProperty("所学专业")
    @Column(name = "major")
    private String major;

    @ApiModelProperty("卫生技术职称")
    @Column(name = "health_title")
    private String healthTitle;

    @ApiModelProperty("毕业证图片")
    @Column(name = "graduate_img")
    private String graduateImg;

    @ApiModelProperty("卫生专业证书")
    @Column(name = "health_img")
    private String healthImg;


    @ApiModelProperty("家庭住址")
    @Column(name = "address")
    private String address;

    @ApiModelProperty("联系方式")
    @Column(name = "phone")
    private String phone;

    @ApiModelProperty("紧急联系人")
    @Column(name = "contact_person")
    private String contactPerson;

    @ApiModelProperty("紧急联系方式")
    @Column(name = "contact_phone")
    private String contactPhone;

    @ApiModelProperty("开户行")
    @Column(name = "bank")
    private String bank;

    @ApiModelProperty("开户行卡号")
    @Column(name = "bank_number")
    private String bankNumber;


    @ApiModelProperty("入职时间")
    @Column(name = "hire_date")
    private Date hireDate;

    @ApiModelProperty("退岗时间")
    @Column(name = "exit_date")
    private Date exitDate;

    @ApiModelProperty("是否在职")
    @Column(name = "is_on_job")
    private Boolean isOnJob;

    @ApiModelProperty("培训单位")
    @Column(name = "training_unit")
    private Long trainingUnit;

    @ApiModelProperty("在岗人员分配的单位")
    @Column(name = "arrange_unit")
    private Long arrangeUnit;

    @ApiModelProperty("复岗原因")
    @Column(name = "hire_reason")
    private String hireReason;

    @ApiModelProperty("退岗原因")
    @Column(name = "exit_reason")
    private String exitReason;






}
