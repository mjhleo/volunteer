package com.netmarch.volunteer.domain;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.netmarch.volunteer.domain.enumeration.RoleType;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * A Role.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Table(name = "role")
@ApiModel("角色")
public class Role extends AbstractAuditingEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty("角色名称")
    @NotBlank(message = "角色名称不能为空")
    @Column(name = "name")
    private String name;

    @ApiModelProperty("角色类型")
    @NotNull(message = "角色类型不能为空")
    @Column(name = "type")
    private RoleType type;

    @ApiModelProperty("描述")
    @Column(name = "description")
    private String description;

    public Role id(Long roleId) {
        this.id = roleId;
        return this;
    }
}
