package com.netmarch.volunteer.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author huangguochen
 * @create 2022/4/28 19:14
 */
@Data
@Table(name = "event_dept")
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class EventDept implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(value="事件Id")
    @Column(name = "event_id")
    private Long eventId;

    @ApiModelProperty(value="分配单位")
    @Column(name = "dept_id")
    private Long deptId;

    @ApiModelProperty(value="单位负责人数")
    @Column(name = "charge_number")
    private Long chargeNumber;

    public EventDept init(Long eventId) {
        this.eventId = eventId;
        return this;
    }

    public EventDept(Long eventId) {
        this.eventId = eventId;
    }
}