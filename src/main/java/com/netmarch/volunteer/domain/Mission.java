package com.netmarch.volunteer.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;


@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("任务管理")
@Table(name = "mission")
public class Mission extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @ApiModelProperty("姓名")
    @NotBlank(message = "姓名不能为空")
    @Column(name = "name")
    private String name;

    @ApiModelProperty("联系电话")
    @NotBlank(message = "联系电话不能为空")
    @Column(name = "phone")
    private String phone;

    @ApiModelProperty("事件编号")
    @NotNull(message = "事件编号不能为空")
    @Column(name = "event_id")
    private Long event_id;

    @ApiModelProperty("单位编号")
    @NotNull(message = "单位编号不能为空")
    @Column(name = "dept_id")
    private Long dept_id;

    @ApiModelProperty("区镇编号")
    @NotBlank(message = "区镇编号不能为空")
    @Column(name = "town_id")
    private String town_id;

    @ApiModelProperty("考勤ID")
    @Column(name = "attendance_id")
    private Long attendanceId;

    @ApiModelProperty("状态")
    @Column(name = "state")
    private Boolean state;


    public Mission id(Long id) {
        this.id = id;
        return this;
    }

    public Mission attendance(Long attendanceId, Long missionId) {
        this.id = missionId;
        this.attendanceId = attendanceId;
        return this;
    }

    public Mission finish(Long missionId) {
        this.id = missionId;
        this.state = Boolean.TRUE;
        return this;
    }
}
