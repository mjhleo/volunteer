package com.netmarch.volunteer.domain;

import com.netmarch.volunteer.domain.enumeration.DictType;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * A Dict.
 */
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("每日评估")
@Table(name = "dict")
public class Dict extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "编码不能为空")
    @Column(name = "code")
    private String code;

    @NotBlank(message = "名称不能为空")
    @Column(name = "name")
    private String name;

    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private DictType type;

    @Column(name = "content")
    private String content;

    public Dict id(Long id) {
        this.id = id;
        return this;
    }
}
