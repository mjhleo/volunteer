package com.netmarch.volunteer.domain.dto;

import com.netmarch.volunteer.domain.enumeration.DictType;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotBlank;

/**
 * @author huangguochen
 * @create 2021/6/28 9:39
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("字典")
public class DictDTO {

    private Long id;

    @NotBlank(message = "编码不能为空")
    private String code;

    @NotBlank(message = "名称不能为空")
    private String name;

    @Enumerated(EnumType.STRING)
    private DictType type;
}
