package com.netmarch.volunteer.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

/**
 * @author zhuguanming
 * @date 2020/10/13
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("令牌与权限")
public class TokenDTO {
    @ApiModelProperty("令牌")
    private String token;
    @ApiModelProperty("权限集合")
    private Set<String> auth;
    @ApiModelProperty("角色")
    private String role;

    public TokenDTO(String token, Set<String> auth) {
        this.token = token;
        this.auth = auth;
    }
}
