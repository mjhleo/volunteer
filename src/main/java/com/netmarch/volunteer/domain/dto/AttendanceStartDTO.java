package com.netmarch.volunteer.domain.dto;

import java.util.Date;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author huangguochen
 * @create 2022/4/28 21:56
 */

@Data
@ApiModel("任务开始参数")
public class AttendanceStartDTO {

    @ApiModelProperty(value="任务ID", required = true)
    @NotNull(message = "任务ID不能为空")
    private Long missionId;

    @ApiModelProperty(value="任务开始地址", required = true)
    @NotEmpty(message = "任务开始地址不能为空")
    private String startAddress;

    @ApiModelProperty(value="任务开始照片", required = true)
    @NotEmpty(message = "任务开始地址不能为空")
    private String startUri;

    @ApiModelProperty(value="任务开始时间", required = true)
    @NotNull(message = "任务开始时间不能为空")
    private Date startDate;

}
