package com.netmarch.volunteer.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("注册DTO")
public class RegisterDTO {

    private Long id;


    @ApiModelProperty("姓名")
    @NotEmpty(message = "姓名不能为空")
    private String name;

    @ApiModelProperty("性别")
    @NotNull(message = "性别不能为空")
    private Integer gender;

    @ApiModelProperty("身份证号")
    @NotEmpty(message = "身份证号不能为空")
    private String idnumber;

    @ApiModelProperty("工作单位")
    private String workUnit;

    @ApiModelProperty("毕业院校")
    @NotEmpty(message = "毕业院校不能为空")
    private String graduate;

    @ApiModelProperty("学历")
    private String eduDegree;

    @ApiModelProperty("所学专业")
    @NotEmpty(message = "所学专业不能为空")
    private String major;

    @ApiModelProperty("卫生技术职称")
    private String healthTitle;

    @ApiModelProperty("毕业证图片")
    private String graduateImg;

    @ApiModelProperty("卫生专业证书图片")
    private String healthImg;

    @ApiModelProperty("毕业证附件")
    private String graduateFile;

    @ApiModelProperty("卫生专业证书附件")
    private String healthFile;


    @ApiModelProperty("家庭住址区镇")
    @NotNull(message = "家庭住址区镇不能为空")
    private Integer addressArea;

    @ApiModelProperty("家庭住址详情")
    private String address;

    @ApiModelProperty("联系方式")
    @NotEmpty(message = "联系方式")
    @Pattern(regexp = "^[0-9]{11}$", message = "手机号码格式不匹配")
    private String phone;

    @ApiModelProperty("紧急联系人")
    private String contactPerson;

    @ApiModelProperty("紧急联系方式")
    private String contactPhone;

    @ApiModelProperty("开户行")
    private String bank;

    @ApiModelProperty("开户行卡号")
    private String bankNumber;

    @ApiModelProperty("是否已被其他区镇招募")
    private Boolean isRegistered;

    @ApiModelProperty("招募区镇")
    private Integer area;

    @ApiModelProperty("备注(希望在哪个板块工作)")
    private String remark;

    @ApiModelProperty("是否年龄在18-50周岁")
    @Column(name = "is_in_age")
    private Boolean isInAge;

    @ApiModelProperty("是否身体健康且无传染性疾病")
    @Column(name = "is_health")
    private Boolean isHealth;

    @ApiModelProperty("是否已至少接种两针疫苗")
    @Column(name = "is_vaccination")
    private Boolean isVaccination;

    @ApiModelProperty("本人及共同居住人健康码为绿码")
    @Column(name = "is_green")
    private Boolean isGreen;

    @ApiModelProperty("大数据行程卡无市外旅居史以及本人48小时内核酸检测阴性")
    @Column(name = "is_negative")
    private Boolean isNegative;

    @ApiModelProperty("是否居住于服务地")
    @Column(name = "is_service_place")
    private Boolean isServicePlace;
}
