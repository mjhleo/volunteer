package com.netmarch.volunteer.domain.dto;

import java.util.Date;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author huangguochen
 * @create 2022/4/28 22:20
 */

@Data
@ApiModel("任务完成参数")
public class AttendanceEndDTO {

    @ApiModelProperty(value="任务ID", required = true)
    @NotNull(message = "任务ID不能为空")
    private Long missionId;

    @ApiModelProperty(value="考勤ID", required = true)
    @NotNull(message = "考勤ID不能为空")
    private Long attendanceId;

    @ApiModelProperty(value="任务完成地址", required = true)
    @NotEmpty(message = "任务完成地址不能为空")
    private String endAddress;

    @ApiModelProperty(value="任务完成照片", required = true)
    @NotEmpty(message = "任务完成照片不能为空")
    private String endUri;

    @ApiModelProperty(value="任务完成时间", required = true)
    @NotNull(message = "任务完成时间不能为空")
    private Date endDate;

}
