package com.netmarch.volunteer.domain.dto;

import java.util.Date;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Mr.Wei  2021/9/6 9:10 上午
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserShiroDTO {
    private Long id;

    private Long roleId;

    @ApiModelProperty("用户名")
    private String username;

    @ApiModelProperty("密码")
    private String password;

    @ApiModelProperty("密码")
    private String salt;

    @ApiModelProperty("姓名")
    private String name;

    @ApiModelProperty("头像")
    private String uri;

    private String idcard;

    private Date loginDate;

    private Boolean activated;

    private Long deptId;

    private String userPassword;

    private String userSalt;

    @ApiModelProperty("账户口令是否安全")
    private Boolean secure;
}
