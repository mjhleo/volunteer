package com.netmarch.volunteer.domain.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.alibaba.excel.annotation.ExcelProperty;
import com.netmarch.volunteer.domain.AbstractAuditingEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;


@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("任务管理")
@Table(name = "mission")
public class MissionQryDto extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @ApiModelProperty("姓名")
    @NotBlank(message = "姓名不能为空")
    @Column(name = "name")
    private String name;

    @ApiModelProperty("联系电话")
    @NotBlank(message = "联系电话不能为空")
    @Column(name = "phone")
    private String phone;

    @ApiModelProperty("事件编号")
    @NotNull(message = "事件编号不能为空")
    private Long eventId;

    @ApiModelProperty("单位编号")
    @NotNull(message = "单位编号不能为空")
    private Long deptId;

    @ApiModelProperty("区镇编号")
    @NotBlank(message = "区镇编号不能为空")
    private String townId;

    @ExcelProperty(index=1,value = "单位")
    @NotBlank(message = "单位不能为空")
    @ApiModelProperty("分配单位")
    private String deptName;

    @ExcelProperty(index=4,value = "区镇")
    @NotBlank(message = "区镇不能为空")
    private String town;

    @ApiModelProperty("考勤ID")
    private Long attendanceId;

    @ApiModelProperty("是否完成")
    private String state;

    @ApiModelProperty("分配事项")
    private String eventName;

    @ApiModelProperty("分配时间")
    private Date releaseDate;

    public MissionQryDto id(Long id) {
        this.id = id;
        return this;
    }
}
