package com.netmarch.volunteer.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("个人管理DTO")
public class PersonnelDTO {

    private Long id;

    private List<Long> ids;

    @ApiModelProperty("姓名")
    private String name;

    @ApiModelProperty("身份证号")
    private String idnumber;

    @ApiModelProperty("联系方式")
    private String phone;

    @ApiModelProperty("所学专业")
    private String major;

    @ApiModelProperty("家庭住址")
    private String address;

    @ApiModelProperty("单位")
    private String trainingUnit;

    @ApiModelProperty("在职人员分配单位")
    private String arrangeUnit;

    @ApiModelProperty("是否在职")
    private Boolean isOnJob;

    @ApiModelProperty("入职时间")
    private Date hireDate;

    @ApiModelProperty("退岗时间")
    private Date exitDate;

    @ApiModelProperty("复岗原因")
    private String hireReason;

    @ApiModelProperty("退岗原因")
    private String exitReason;

    @ApiModelProperty("最后修改者")
    private String lastModifiedBy;

    @ApiModelProperty("最后修改时间")
    private Date lastModifiedDate;


}
