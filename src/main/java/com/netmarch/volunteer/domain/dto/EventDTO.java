package com.netmarch.volunteer.domain.dto;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.netmarch.volunteer.domain.EventDept;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author huangguochen
 * @create 2022/4/27 22:16
 */

@Data
public class EventDTO {
    private Long id;

    @ApiModelProperty(value = "事件名称", required = true)
    @NotEmpty(message = "事件名称不能为空")
    private String name;

    @ApiModelProperty(value = "事件简介", required = true)
    @NotEmpty(message = "事件简介不能为空")
    private String content;

    @ApiModelProperty(value = "事件发布时间", required = true)
    @NotNull(message = "事件发布时间不能为空")
    private Date releaseDate;

    @ApiModelProperty("文件")
    private String uri;

    @ApiModelProperty(value = "保障人数", required = true)
    @NotNull(message = "保障人数不能为空")
    private Long assureNumber;

    private List<EventDept> depts;

    public EventDTO id(Long id) {
        this.id = id;
        return this;
    }

    public EventDTO depts(List<EventDept> depts) {
        this.depts = depts;
        return this;
    }
}
