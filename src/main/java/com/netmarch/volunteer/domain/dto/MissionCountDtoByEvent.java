package com.netmarch.volunteer.domain.dto;

import com.alibaba.excel.annotation.ExcelProperty;
import com.netmarch.volunteer.domain.AbstractAuditingEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;


@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("根据取各单位统计")
public class MissionCountDtoByEvent extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @ApiModelProperty("事件编号")
    @NotNull(message = "事件编号不能为空")
    private Long eventId;

    @ApiModelProperty("事件名称")
    private String eventName;

    @ApiModelProperty("单位编号")
    @NotNull(message = "单位编号不能为空")
    private Long deptId;


    @ExcelProperty(index=1,value = "单位")
    @NotBlank(message = "单位不能为空")
    @ApiModelProperty("分配单位")
    private String deptName;


    @ApiModelProperty(value="单位负责人数")
    @Column(name = "charge_number")
    private Long chargeNumber;

    @ApiModelProperty("人员数量")
    private Integer personNum;

    public MissionCountDtoByEvent id(Long id) {
        this.id = id;
        return this;
    }
}
