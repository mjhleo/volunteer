package com.netmarch.volunteer.domain.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PasswordDTO {

    @ApiModelProperty("新密码")
    @NotBlank(message = "密码不能为空")
    @Size(min = 6, max = 18, message = "密码长度不符合规则")
    private String newPassword;

    @ApiModelProperty("确认密码")
    @NotBlank(message = "确认密码不能为空")
    @Size(min = 6, max = 18, message = "密码长度不符合规则")
    private String rePassword;
}
