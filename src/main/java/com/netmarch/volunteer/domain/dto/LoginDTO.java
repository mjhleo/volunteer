package com.netmarch.volunteer.domain.dto;

import javax.validation.constraints.Size;

import com.netmarch.volunteer.domain.enumeration.Platform;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

/**
 * @author Mr.Wei on 2018/4/2.
 */
@Data
@ApiModel("用户登录")
@Builder
public class LoginDTO {
    //@Pattern(regexp = "^[0-9]{11}$", message = "手机号码格式不匹配")
    @ApiModelProperty("用户名")
    private String username;
    @Size(min = 6, max = 18, message = "密码长度不符合规则")
    @ApiModelProperty("密码")
    private String password;
    @ApiModelProperty("登陆来源")
    private Platform platform = Platform.PC;
}
