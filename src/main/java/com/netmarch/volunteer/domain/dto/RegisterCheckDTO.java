package com.netmarch.volunteer.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("审核DTO")
public class RegisterCheckDTO {

    private Long id;

    private List<Long> ids;

    //2.审核部分
    @ApiModelProperty("报名审核状态")
    @Column(name = "check_state")
    private Integer checkState;

    @ApiModelProperty("报名审核意见")
    @Column(name = "check_advice")
    private String checkAdvice;

    @ApiModelProperty("报名审核人")
    @Column(name = "check_by")
    private String checkBy;

    @ApiModelProperty("报名审核时间")
    @Column(name = "check_date")
    private Date checkDate;
}
