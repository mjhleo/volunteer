package com.netmarch.volunteer.domain.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <管理员信息>
 *
 * @author Huangguochen
 * @create 2020/9/25 13:44
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("用户")
public class UserDTO {

    private Long id;

    @NotNull(message = "角色不能为空")
    private Long roleId;

    @ApiModelProperty("账号")
    @NotEmpty(message = "账号不能为空")
    @Pattern(regexp = "^[0-9]{11}$", message = "手机号码格式不匹配")
    private String username;

    @ApiModelProperty("用户名")
    @NotEmpty(message = "用户名不能为空")
    private String name;

    @ApiModelProperty("可用/不可用")
    @NotNull(message = "状态不能为空")
    private Boolean activated;

    @ApiModelProperty("机构号")
    @NotNull(message = "机构号不能为空")
    private Long deptId;

    public UserDTO id(long id) {
        this.id = id;
        return this;
    }
}
