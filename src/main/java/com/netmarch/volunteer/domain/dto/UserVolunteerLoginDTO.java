package com.netmarch.volunteer.domain.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author huangguochen
 * @create 2022/5/5 11:49
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("志愿者用户登录")
public class UserVolunteerLoginDTO {

    @ApiModelProperty("手机号")
    @NotEmpty(message = "手机号不能为空")
    @Pattern(regexp = "^[0-9]{11}$", message = "手机号码格式不匹配")
    private String username;

    @ApiModelProperty("密码")
    @NotEmpty(message = "密码不能为空")
    private String password;


    @ApiModelProperty("验证码")
    @NotEmpty(message = "验证码不能为空")
    private String code;

}
