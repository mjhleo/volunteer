package com.netmarch.volunteer.domain.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.netmarch.volunteer.domain.enumeration.AnnouncementType;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author huangguochen
 * @create 2022/4/28 16:40
 */

@Data
public class AnnouncementDTO {

    private Long id;

    @ApiModelProperty(value="标题")
    @NotEmpty(message = "标题不能为空")
    private String title;

    @ApiModelProperty(value="内容")
    @NotEmpty(message = "内容不能为空")
    private String content;

    @ApiModelProperty(value="附件")
    private String uri;

    @ApiModelProperty(value="类型[通知和现场培训(附件只能上传word、pdf或者excel)、视频培训(附件只能上传mp4,大小不超过100M)]")
    @NotNull(message = "类型不能为空")
    private AnnouncementType type;

    public AnnouncementDTO id(Long id) {
        this.id = id;
        return this;
    }
}
