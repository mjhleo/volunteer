package com.netmarch.volunteer.domain.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.netmarch.volunteer.domain.enumeration.MenuType;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zhuguanming
 * @date 2020/10/12
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MenuDTO {

    private Long id;

    @ApiModelProperty("父级菜单Id")
    private Long pid;

    @ApiModelProperty("菜单名称")
    private String name;

    @ApiModelProperty("菜单图标")
    private String icon;

    @ApiModelProperty("菜单链接")
    private String url;

    @ApiModelProperty("排序")
    private Integer sort;

    @ApiModelProperty("权限")
    private String auth;

    @ApiModelProperty("类型(SYSTEM:系统菜单,VOLUNTEER:志愿者菜单)")
    private MenuType type;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @ApiModelProperty("子菜单")
    private List<MenuDTO> children;

}
