package com.netmarch.volunteer.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("密码修改")
public class PasswordModifyDTO {

    @ApiModelProperty("原密码")
    @NotBlank(message = "原密码不能为空")
    private String originPassword;

    @ApiModelProperty("新密码")
    @NotBlank(message = "密码不能为空")
    @Size(min = 6, max = 18, message = "密码长度不符合规则")
    private String newPassword;

    @ApiModelProperty("确认密码")
    @NotBlank(message = "确认密码不能为空")
    @Size(min = 6, max = 18, message = "密码长度不符合规则")
    private String rePassword;
}
