package com.netmarch.volunteer.domain.dto;

import com.alibaba.excel.annotation.ExcelProperty;
import com.netmarch.volunteer.domain.enumeration.DictType;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotBlank;

/**
 * @author huangguochen
 * @create 2021/6/28 9:39
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("任务人员导入")
public class MissionImportDTO {

    @ExcelProperty(index=0,value = "编号")
    @NotBlank(message = "社区名称不能为空")
    private String code;

    @ExcelProperty(index=1,value = "单位")
    @NotBlank(message = "单位不能为空")
    private String deptName;

    @ExcelProperty(index=2,value = "姓名")
    @NotBlank(message = "姓名不能为空")
    private String name;

    @ExcelProperty(index=3,value = "电话")
    @NotBlank(message = "电话不能为空")
    private String phone;

    @ExcelProperty(index=4,value = "区镇")
    @NotBlank(message = "区镇不能为空")
    private String town;
}
