package com.netmarch.volunteer.domain.dto;

import java.util.Date;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author huangguochen
 * @create 2022/4/29 19:21
 */

@Data
public class MissionCountDTO {

    @ApiModelProperty("未完成任务数量")
    private Long unfinishedCount;

    @ApiModelProperty("已完成任务数量")
    private Long finishedCount;

    @ApiModelProperty("总任务数")
    private Long total;

    @ApiModelProperty("任务发布时间")
    private Date releaseDate;

    @ApiModelProperty("机构名称")
    private String deptName;
}
