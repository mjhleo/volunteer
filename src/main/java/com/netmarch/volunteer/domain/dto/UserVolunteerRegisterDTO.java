package com.netmarch.volunteer.domain.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author huangguochen
 * @create 2022/5/5 9:39
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("志愿者用户注册")
public class UserVolunteerRegisterDTO {

    @ApiModelProperty("手机号")
    @NotEmpty(message = "手机号不能为空")
    @Pattern(regexp = "^[0-9]{11}$", message = "手机号码格式不匹配")
    private String username;

    @ApiModelProperty("密码")
    @NotEmpty(message = "密码不能为空")
    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[^a-zA-Z0-9]).{8,30}$", message = "密码格式不匹配")
    private String password;

    @ApiModelProperty("身份证号")
    @NotEmpty(message = "身份证号不能为空")
    @Pattern(regexp = "(^\\d{15}$)|(^\\d{18}$)|(^\\d{17}([\\dXx])$)", message = "身份证号格式不匹配")
    private String idcard;
}
