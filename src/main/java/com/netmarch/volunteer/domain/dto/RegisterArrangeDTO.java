package com.netmarch.volunteer.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("单位分配DTO")
public class RegisterArrangeDTO {

    @ApiModelProperty("分配单位")
    @NotNull(message = "分配单位不能为空")
    private Long trainingUnit;

    @ApiModelProperty("人员")
    @NotEmpty(message = "人员不能为空")
    List<Long> ids;
}
