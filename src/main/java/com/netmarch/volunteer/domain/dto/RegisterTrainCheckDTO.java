package com.netmarch.volunteer.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("审核DTO")
public class RegisterTrainCheckDTO {

    private Long id;

    private List<Long> ids;

    @ApiModelProperty("培训内容")
    @Column(name = "training_content")
    private String trainingContent;

    @ApiModelProperty("培训完成状态")
    @Column(name = "training_result")
    private Integer trainingResult;

    @ApiModelProperty("培训审核状态")
    @Column(name = "training_state")
    private Integer trainingState;

    @ApiModelProperty("培训审核意见")
    @Column(name = "training_advice")
    private String trainingAdvice;

    @ApiModelProperty("培训审核人")
    @Column(name = "training_by")
    private String trainingBy;

    @ApiModelProperty("培训审核时间")
    @Column(name = "training_date")
    private Date trainingDate;
}
