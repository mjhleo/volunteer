package com.netmarch.volunteer.domain.dto;

import com.netmarch.volunteer.domain.enumeration.MessageEnum;
import com.netmarch.volunteer.domain.enumeration.SystemMessageState;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author huangguochen
 * @create 2022/4/28 13:24
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SystemMessageDTO {

    private Long id;

    @ApiModelProperty(value="标题")
    private String title;

    @ApiModelProperty(value="消息类型")
    private MessageEnum messageType;

    @ApiModelProperty(value="消息内容")
    private String content;

    @ApiModelProperty(value="消息接收单位")
    private Long receiveUnitId;

    @ApiModelProperty(value="消息接收人手机号")
    private String receivePhone;

    @ApiModelProperty(value="消息接收状态")
    private SystemMessageState messageState;

    public SystemMessageDTO init(MessageEnum messageEnum, Object[] params) {
        this.title = messageEnum.getProfile();
        this.content = null != params ? String.format(messageEnum.getContent(), params) : messageEnum.getContent();
        this.messageType = messageEnum;
        this.messageState = SystemMessageState.UNREAD;
        return this;
    }

}
