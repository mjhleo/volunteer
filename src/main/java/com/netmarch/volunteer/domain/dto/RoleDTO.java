package com.netmarch.volunteer.domain.dto;

import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.netmarch.volunteer.domain.enumeration.RoleType;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zhuguanming
 * @date 2020/10/12
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RoleDTO {

    private Long id;

    @ApiModelProperty("角色名称")
    @NotBlank(message = "角色名称不能为空")
    private String name;

    @ApiModelProperty("描述")
    private String description;

    @NotNull(message = "角色类型不能为空")
    private RoleType type;

    private List<PermissionDTO> auth;

    public RoleDTO id(Long roleId) {
        this.id = roleId;
        return this;
    }

    public RoleDTO initAuthDTO(List<PermissionDTO> leafMenus) {
        this.auth = leafMenus;
        return this;
    }
}
