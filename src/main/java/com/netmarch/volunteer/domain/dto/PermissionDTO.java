package com.netmarch.volunteer.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Huangguochen
 * @create 2021/2/5 16:22
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PermissionDTO {
    private Long roleId;

    private Long menuId;

    private String auth;

}
