package com.netmarch.volunteer.domain.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.Set;

/**
 * <用户信息>
 *
 * @author Huangguochen
 * @create 2021/3/29 11:03
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserInfoDTO {

    private Long id;

    private Long roleId;

    @ApiModelProperty("账号")
    private String username;

    @ApiModelProperty("用户名")
    private String name;

    @ApiModelProperty("可用/不可用")
    private Boolean activated;

    @ApiModelProperty("角色名称")
    private String roleName;

    @ApiModelProperty("登录时间")
    private Date loginDate;

    @ApiModelProperty("登录时间")
    private Long deptId;

    private String uri;

    private String filePrefix;

    private Set<String> auth;

    public UserInfoDTO initUrl(String filePrefix) {
        this.filePrefix = filePrefix;
        return this;
    }

    public UserInfoDTO auth(Set<String> auth) {
        this.auth = auth;
        return this;
    }

}
