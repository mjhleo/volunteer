package com.netmarch.volunteer.domain.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author huangguochen
 * @create 2022/4/29 19:21
 */

@Data
public class AttendanceCountDTO {

    @ApiModelProperty("未审核考勤数量")
    private Long unfinishedCount;

    @ApiModelProperty("已审核考勤数量")
    private Long finishedCount;

    @ApiModelProperty("总考勤数量")
    private Long total;

    @ApiModelProperty("考勤开始时间")
    private Date releaseDate;

    @ApiModelProperty("机构名称")
    private String deptName;
}
