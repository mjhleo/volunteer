package com.netmarch.volunteer.domain;

import com.netmarch.volunteer.domain.enumeration.DictType;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;


@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("组织机构")
@Table(name = "dept")
public class Dept extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;



    @NotBlank(message = "名称不能为空")
    @Column(name = "name")
    private String name;

    public Dept id(Long id) {
        this.id = id;
        return this;
    }
}
