package com.netmarch.volunteer.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import java.io.Serializable;
import java.util.Date;

@Data
public class AbstractAuditingEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "created_by")
    @ApiModelProperty("创建者")
    private String createdBy;

    @Column(name = "created_date")
    @ApiModelProperty("创建时间")
    private Date createdDate;

    @Column(name = "last_modified_by")
    @ApiModelProperty("最后修改者")
    private String lastModifiedBy;

    @Column(name = "last_modified_date")
    @ApiModelProperty("最后修改时间")
    private Date lastModifiedDate;
}
