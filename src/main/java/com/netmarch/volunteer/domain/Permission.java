package com.netmarch.volunteer.domain;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * A Permission.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Table(name = "permission")
@ApiModel("权限")
public class Permission {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty("角色")
    @Column(name = "role_id")
    private Long roleId;

    @ApiModelProperty("权限")
    @Column(name = "menu_id")
    private Long menuId;

    @Column(name = "auth")
    private String auth;

    public Permission(Long roleId) {
        this.roleId = roleId;
    }

    public Permission(Long roleId, Long menuId, String auth) {
        this.roleId = roleId;
        this.menuId = menuId;
        this.auth = auth;
    }
}
