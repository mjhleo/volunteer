package com.netmarch.volunteer.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * A User.
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Table(name = "user")
@ApiModel("用户")
public class User extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "role_id")
    private Long roleId;

    @ApiModelProperty("账号")
    @Column(name = "username")
    private String username;

    @JsonIgnore
    @ApiModelProperty("密码")
    @Column(name = "password")
    private String password;

    @JsonIgnore
    @ApiModelProperty("密码")
    @Column(name = "salt")
    private String salt;

    @ApiModelProperty("用户名")
    @Column(name = "name")
    private String name;

    @ApiModelProperty("头像")
    @Column(name = "uri")
    private String uri;

    @Column(name = "login_date")
    private Date loginDate;

    @Column(name = "activated")
    private Boolean activated;

    @ApiModelProperty("账户口令是否安全")
    @Column(name = "secure")
    private Boolean secure;

    @ApiModelProperty("机构号")
    @Column(name = "dept_id")
    private Long deptId;

    @Transient
    private String roleName;

    public User(Long id, Boolean activated) {
        this.id = id;
        this.activated = activated;
    }

    public User login(Long id) {
        this.id = id;
        this.loginDate = new Date();
        return this;
    }

    public void initPassword(String salt, String password) {
        this.salt = salt;
        this.password = password;
    }

    public User id(long id) {
        this.id = id;
        return this;
    }

    public User secure(Boolean secure) {
        this.secure = secure;
        return this;
    }

    public User uri(String url) {
        this.uri = url;
        return this;
    }

    public User loginDate() {
        this.loginDate = new Date();
        return this;
    }


    public User(String name) {
        this.name = name;
    }

    public User activated(Boolean activated) {
        this.activated = activated;
        return this;
    }
}
