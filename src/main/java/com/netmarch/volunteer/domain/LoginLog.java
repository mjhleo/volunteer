package com.netmarch.volunteer.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.netmarch.volunteer.domain.enumeration.Platform;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zhuguanming
 * @date 2020/12/7
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("登陆日志")
@Table(name = "login_log")
public class LoginLog {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty("平台")
    private Platform platform;

    @ApiModelProperty("单位Id")
    @Column(name = "unit_id")
    private Long unitId;

    @ApiModelProperty("单位名称")
    @Column(name = "unit_name")
    private String unitName;

    @ApiModelProperty("登陆人Id")
    @Column(name = "user_id")
    private Long userId;

    @ApiModelProperty("姓名")
    private String name;

    @ApiModelProperty("账号")
    private String username;

    @ApiModelProperty("登陆ip")
    private String ip;

    @ApiModelProperty("登陆日期")
    @Column(name = "login_date")
    private Date loginDate;

    @ApiModelProperty("执行时长(毫秒)")
    private Long time;

    public LoginLog(Long userId, Platform platform, String name, String username, String ip) {
        this.userId = userId;
        this.platform = platform;
        this.name = name;
        this.username = username;
        this.ip = ip;
        this.loginDate = new Date();
    }

    public LoginLog update(Long id, Date date) {
        this.id = id;
        this.loginDate = date;
        return this;
    }
}
