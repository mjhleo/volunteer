package com.netmarch.volunteer.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author huangguochen
 * @create 2022/4/28 21:50
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("考勤信息")
@Table(name = "attendance")
@Builder
public class Attendance implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(value="任务开始地址")
    @Column(name = "start_address")
    private String startAddress;

    @ApiModelProperty(value="任务开始照片")
    @Column(name = "start_uri")
    private String startUri;

    @ApiModelProperty(value="任务开始时间")
    @Column(name = "start_date")
    private Date startDate;

    @ApiModelProperty(value="任务完成地址")
    @Column(name = "end_address")
    private String endAddress;

    @ApiModelProperty(value="任务完成照片")
    @Column(name = "end_uri")
    private String endUri;

    @ApiModelProperty(value="任务完成时间")
    @Column(name = "end_date")
    private Date endDate;

    @ApiModelProperty(value="审核状态")
    @Column(name = "check_state")
    private Boolean checkState;

    @ApiModelProperty(value="审核意见")
    @Column(name = "check_advice")
    private String checkAdvice;

}