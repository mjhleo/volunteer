package com.netmarch.volunteer.domain;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.netmarch.volunteer.domain.enumeration.MenuType;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zhuguanming
 * @date 2020/10/12
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("菜单")
@Table(name = "menu")
public class Menu {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty("父级菜单Id")
    @NotNull(message = "父级菜单Id不能为空")
    private Long pid;

    @ApiModelProperty("菜单名称")
    @NotNull(message = "菜单名称不能为空")
    private String name;

    @ApiModelProperty("菜单图标")
    private String icon;

    @ApiModelProperty("菜单链接")
    private String url;

    @ApiModelProperty("排序")
    @NotNull(message = "排序字段不能为空")
    private Integer sort;

    @ApiModelProperty("权限")
    private String auth;

    @ApiModelProperty("类型(SYSTEM:系统菜单,VOLUNTEER:志愿者菜单)")
    @NotNull(message = "类型不能为空")
    private MenuType type;

    public Menu id(Long menuId) {
        this.id = menuId;
        return this;
    }
}
