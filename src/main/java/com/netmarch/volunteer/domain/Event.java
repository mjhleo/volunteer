package com.netmarch.volunteer.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author huangguochen
 * @create 2022/4/27 22:04
 */
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("事件")
@Table(name = "event")
public class Event extends AbstractAuditingEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    @ApiModelProperty("事件名称")
    private String name;

    @Column(name = "content")
    @ApiModelProperty("事件简介")
    private String content;

    @Column(name = "release_date")
    @ApiModelProperty("事件发布时间")
    private Date releaseDate;

    @Column(name = "uri")
    @ApiModelProperty("文件")
    private String uri;

    @Column(name = "assure_number")
    @ApiModelProperty("保障人数")
    private Long assureNumber;

}