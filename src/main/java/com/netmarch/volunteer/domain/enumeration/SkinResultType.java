package com.netmarch.volunteer.domain.enumeration;

/**
 * <皮试结果>
 *
 * @author huangguochen
 * @create 2021/6/23 14:32
 */

public enum SkinResultType {
    /**
     * 阴性、阳性
     */
    NEGATIVE, MASCULINE
}
