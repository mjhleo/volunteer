package com.netmarch.volunteer.domain.enumeration;


import com.netmarch.volunteer.config.Constants;

/**
 * The Platform enumeration.
 */
public enum Platform {
    PC("Auth"), APP("Auth"),VOLUNTEER_WECHAT("App"), VOLUNTEER_PC("Volunteer");

    private String realmName;

    Platform(String realmName) {
        this.realmName = realmName;
    }

    public String getRealmName() {
        return realmName;
    }

    public String getTokenKey() {
        switch (this) {
            case PC:
                return Constants.REDIS_PC_TOKEN_KEY;
            case APP:
                return Constants.REDIS_APP_TOKEN_KEY;
            case VOLUNTEER_WECHAT:
                return Constants.REDIS_VOLUNTEER_WECHAT_TOKEN_KEY;
            case VOLUNTEER_PC:
                return Constants.REDIS_VOLUNTEER_PC_TOKEN_KEY;
            default:
                return "";
        }
    }
}
