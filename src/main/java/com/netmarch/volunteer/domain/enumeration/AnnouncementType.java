package com.netmarch.volunteer.domain.enumeration;

/**
 * @author huangguochen
 * @create 2022/4/28 16:37
 */

public enum AnnouncementType {

    /**
     * 通知、视频培训、现场培训
     */
    NOTICE, TRAINING_VIDEO, TRAINING_ON_SITE;

}
