package com.netmarch.volunteer.domain.enumeration;

/**
 * The MedicalType enumeration.
 */
public enum MedicalType {
    /**
     * 自费、医保
     */
    SELF, MEDICAL
}
