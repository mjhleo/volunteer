package com.netmarch.volunteer.domain.enumeration;

/**
 * @author Mr.Wei  2021/3/3 11:02 上午
 */
public enum RoleType {
    USER, LEADER, ADMIN
}
