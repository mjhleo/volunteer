package com.netmarch.volunteer.domain.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author huangguochen
 * @create 2022/4/28 11:18
 */
@Getter
@AllArgsConstructor
public enum MessageEnum {

    /**
     * 报名审核结果, 任务通知, 疫情防控
     */
    REGISTRATION_RESULT("【审核通知】报名审核结果通知", "您的报名审核结果为  %s"),
    TASK_NOTICE("【系统通知】新任务通知", "您有新的任务，请至任务工单中查看"),
    EPIDEMIC_NOTICE("【系统通知】疫情防控通知", "%s"),
    ;

    private String profile;
    private String content;

}
