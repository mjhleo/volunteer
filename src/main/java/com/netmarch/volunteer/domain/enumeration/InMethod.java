package com.netmarch.volunteer.domain.enumeration;

/**
 * <入院方式>
 *
 * @author Huangguochen
 * @create 2021/4/19 9:58
 */

public enum InMethod {
    /*急诊，门诊，其他*/
    EMERGENCY, OUTPATIENT, OTHER
}
