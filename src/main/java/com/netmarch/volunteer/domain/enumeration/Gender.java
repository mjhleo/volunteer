package com.netmarch.volunteer.domain.enumeration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * The Gender enumeration.
 */
public enum Gender {
    MALE("1"),
    FEMALE("2"),
    UNKNOWN("0");


    private String code;

    Gender(String code){
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public static Gender getEnum(String name) {
        Gender[] arry = Gender.values();
        for (int i = 0; i < arry.length; i++) {
            if (arry[i].name().equals(name)) {
                return arry[i];
            }
        }
        return null;
    }

    public static Gender getEnumByCode(String code) {
        Gender[] arry = Gender.values();
        for (int i = 0; i < arry.length; i++) {
            if (arry[i].getCode().equals(code)) {
                return arry[i];
            }
        }
        return null;
    }

    public static HashMap<String, String> getMap() {
        HashMap<String, String> map = new HashMap<String, String>();
        Gender[] ary = Gender.values();
        List list = new ArrayList();
        for (int i = 0; i < ary.length; i++) {
            map.put(ary[i].name(), ary[i].getCode());
        }
        return map;
    }


}
