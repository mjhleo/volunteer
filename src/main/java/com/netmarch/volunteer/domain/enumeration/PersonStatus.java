package com.netmarch.volunteer.domain.enumeration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * The PersonStatus enumeration.
 */
public enum PersonStatus {
    /**
     * 待确认
     * 集中隔离
     * 居家隔离
     * 健康监测
     * 解除
     */
    TO_CONFIRMED("待确认"),
    CENTRALIZED_ISOLATION("集中隔离"),
    HOME_ISOLATION("居家隔离"),
    HEALTH_MONITOR("健康监测"),
    REMOVE("解除");

    private String desc;

    PersonStatus(String desc){
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }

    public static PersonStatus getEnum(String name) {
        PersonStatus[] arry = PersonStatus.values();
        for (int i = 0; i < arry.length; i++) {
            if (arry[i].name().equals(name)) {
                return arry[i];
            }
        }
        return null;
    }

    public static PersonStatus getEnumByDesc(String desc) {
        PersonStatus[] arry = PersonStatus.values();
        for (int i = 0; i < arry.length; i++) {
            if (arry[i].getDesc().equals(desc)) {
                return arry[i];
            }
        }
        return null;
    }

    public static HashMap<String, String> getMap() {
        HashMap<String, String> map = new HashMap<String, String>();
        PersonStatus[] ary = PersonStatus.values();
        List list = new ArrayList();
        for (int i = 0; i < ary.length; i++) {
            map.put(ary[i].name(), ary[i].getDesc());
        }
        return map;
    }

}
