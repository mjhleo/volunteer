package com.netmarch.volunteer.domain.enumeration;

/**
 * @author jichao
 * @date 2022/4/7 22:43
 */
public enum FamilyType {

    FATHER,MOTHER,SON,DAUGHTER,GRANDMA,GRANDPA

}
