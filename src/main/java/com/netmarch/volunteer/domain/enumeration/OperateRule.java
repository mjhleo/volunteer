package com.netmarch.volunteer.domain.enumeration;

/**
 * @author huangguochen
 * @create 2021/12/2 15:55
 */
public enum OperateRule {
    /**
     * 小于，小于等于，等于，大于等于，大于, 不等于
     */
    LESS      (true,  false, false),
    LESSEQUAL (true,  true,  false),
    EQUAL     (false, true,  false),
    GREATEQUAL(false, true,  true),
    GREAT     (false, false, true),
    NOTEQUAL  (true,  false, true);

    private boolean isNotLess = false;
    private boolean isNotEqual = false;
    private boolean isNotGreat = false;

    private OperateRule(boolean isLess, boolean isEqual, boolean isGreat){
        this.isNotLess = !isLess;
        this.isNotEqual = !isEqual;
        this.isNotGreat = !isGreat;
    }

    public boolean execute(Comparable a, Comparable b) {
        //获得比较值
        int k = -1;
        if (a == null) {
            if (b == null)
                k = 0;
        } else {
            k = a.compareTo(b);
        }
        //执行比较,如果k值与基本比较要求的不一致，则不符合本比较的条件，返回false
        if (this.isNotLess && k < 0)
            return false;
        if (this.isNotEqual && k == 0)
            return false;
        if (this.isNotGreat && k > 0)
            return false;
        //只有完全通过了这三次基本比较要求，那么才算通过了本方式的比较
        return true;
    }
}
