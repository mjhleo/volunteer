package com.netmarch.volunteer.domain.enumeration;

/**
 * The EventStatus enumeration.
 */
public enum EventStatus {
    /**
     * 进行中
     */
    UNDER_WAY,
    /**
     * 已结束
     */
    FINISHED
}
