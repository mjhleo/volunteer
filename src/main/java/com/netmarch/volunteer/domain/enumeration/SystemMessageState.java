package com.netmarch.volunteer.domain.enumeration;

/**
 * @author huangguochen
 * @create 2022/4/28 13:13
 */

public enum SystemMessageState {
    /**
     * 已读、未读
     */
    READ,UNREAD
}
