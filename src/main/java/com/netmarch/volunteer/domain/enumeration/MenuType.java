package com.netmarch.volunteer.domain.enumeration;

/**
 * <菜单类型>
 *
 * @author huangguochen
 * @create 2022/4/27 20:30
 */

public enum MenuType {

    /**
     * <系统>
     */
    SYSTEM,

    /**
     * <志愿者>
     */
    VOLUNTEER
}
