package com.netmarch.volunteer.domain.enumeration;

/**
 * The EntityAction enumeration.
 */
public enum EntityAction {
    /**
     * 查询，插入，删除，更新
     */
    SELECT, INSERT, DELETE, UPDATE
}
