package com.netmarch.volunteer.interceptor;


import java.util.Date;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.binding.MapperMethod;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Invocation;
import org.apache.ibatis.plugin.Plugin;
import org.apache.ibatis.plugin.Signature;
import org.apache.ibatis.session.Configuration;
import org.springframework.stereotype.Component;

import com.netmarch.volunteer.domain.AbstractAuditingEntity;
import com.netmarch.volunteer.domain.EntityLog;
import com.netmarch.volunteer.domain.OperationLog;
import com.netmarch.volunteer.domain.enumeration.EntityAction;
import com.netmarch.volunteer.interceptor.util.InterceptorSqlUtil;
import com.netmarch.volunteer.service.EntityLogService;
import com.netmarch.volunteer.interceptor.thead.PlatformStorage;
import com.netmarch.volunteer.interceptor.thead.Storage;
import com.netmarch.volunteer.service.util.SpringBeanHelper;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Mr.Wei on 2018/4/17.
 */
@Intercepts({
    @Signature(args = {MappedStatement.class, Object.class}, type = Executor.class, method = "update")
})
@Component
@Slf4j
public class UpdateInterceptor implements Interceptor {
    private static final String SYSTEM = "SYSTEM";
    private static final String RECORD = "record";

    private EntityLogService entityLogService;

    @Override
    public Object intercept(Invocation invocation) throws Throwable {
        if (null == entityLogService) {
            entityLogService = SpringBeanHelper.getBean(EntityLogService.class);
        }
        EntityLog entityLog;
        Object[] args = invocation.getArgs();
        MappedStatement ms = (MappedStatement) args[0];
        Object parameter = args[1];
        EntityAction action = this.initEntity(parameter, ms);
        if (Objects.nonNull(parameter) && Objects.nonNull(PlatformStorage.getPlatform()) && ! (parameter instanceof OperationLog) && ! (parameter instanceof EntityLog ) ) {
            BoundSql boundSql = ms.getBoundSql(parameter);
            Configuration configuration = ms.getConfiguration();
            long start = System.currentTimeMillis();
            entityLog = new EntityLog("", parameter.getClass().getName(),
                                      ms.getId(), PlatformStorage.getPlatform(), action,
                                      InterceptorSqlUtil.getSql(configuration, boundSql),
                                      System.currentTimeMillis() - start);
            this.entityLogService.insert(entityLog);
        }
        return invocation.proceed();

    }

    @Override
    public Object plugin(Object o) {
        return Plugin.wrap(o, this);
    }

    private EntityAction initEntity(Object object, MappedStatement ms) {
        if (ms.getSqlCommandType().name().startsWith("INSERT")) {
            return this.insertEntity(object);
        } else if (ms.getSqlCommandType().name().startsWith("UPDATE")) {
            return this.updateEntity(object);
        } else if (ms.getSqlCommandType().name().startsWith("DELETE")) {
            return EntityAction.DELETE;
        } else {
            return null;
        }
    }

    private EntityAction insertEntity(Object object) {
        if (object instanceof AbstractAuditingEntity) {
            AbstractAuditingEntity auditingEntity = (AbstractAuditingEntity) object;
            if (StringUtils.isEmpty(auditingEntity.getCreatedBy())) {
                auditingEntity.setCreatedBy(null == Storage.getOperator() ? SYSTEM : Storage.getOperator());
            }
            auditingEntity.setCreatedDate(new Date());
            auditingEntity.setLastModifiedBy(Storage.getOperator());
            auditingEntity.setLastModifiedDate(new Date());
        }
        return EntityAction.INSERT;
    }

    private EntityAction updateEntity(Object object) {
        AbstractAuditingEntity auditingEntity = null;
        if (object instanceof AbstractAuditingEntity) {
            auditingEntity = (AbstractAuditingEntity) object;
        } else if (object instanceof MapperMethod.ParamMap && ((MapperMethod.ParamMap) object).containsKey(RECORD)
            && ((MapperMethod.ParamMap) object).get(RECORD) instanceof AbstractAuditingEntity) {
            auditingEntity = (AbstractAuditingEntity) ((MapperMethod.ParamMap) object).get(RECORD);
        }
        if (null != auditingEntity) {
            auditingEntity.setLastModifiedBy(Storage.getOperator());
            auditingEntity.setLastModifiedDate(new Date());
        }
        return EntityAction.UPDATE;
    }

}
