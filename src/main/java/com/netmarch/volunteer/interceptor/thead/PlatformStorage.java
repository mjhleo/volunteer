package com.netmarch.volunteer.interceptor.thead;


import com.netmarch.volunteer.domain.enumeration.Platform;

/**
 * @author Huangguochen
 * @create 2020/11/24 15:50
 */
public class PlatformStorage {
    private PlatformStorage() {
        throw new IllegalStateException();
    }

    private static ThreadLocal<Platform> operator = new ThreadLocal<>();

    public static Platform getPlatform() {
        return operator.get();
    }

    public static void setPlatform(Platform platform) {
        operator.set(platform);
    }

    public static void remove() {
        operator.remove();
    }
}
