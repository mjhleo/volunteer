package com.netmarch.volunteer.interceptor.thead;

/**
 * @author Mr.Wei on 2018/7/18
 */
public class Storage {
    private Storage() {
        throw new IllegalStateException();
    }

    private static ThreadLocal<String> operator = new ThreadLocal<>();

    public static String getOperator() {
        return operator.get();
    }

    public static void setOperator(String value) {
        operator.set(value);
    }

    public static void remove() {
        operator.remove();
    }
}
