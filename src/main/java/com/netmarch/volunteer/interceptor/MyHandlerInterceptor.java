package com.netmarch.volunteer.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;

import com.netmarch.volunteer.config.Constants;
import com.netmarch.volunteer.interceptor.thead.PlatformStorage;
import com.netmarch.volunteer.interceptor.thead.Storage;
import com.netmarch.volunteer.service.util.JwtUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Mr.Wei on 2018/7/18
 */
@Slf4j
public class MyHandlerInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object o) {
    if (null != request.getHeader(Constants.REQUEST_HEADER)) {
            Storage.setOperator(JwtUtil.getUsername(request.getHeader(Constants.REQUEST_HEADER)));
            PlatformStorage.setPlatform(JwtUtil.getPlatform(request.getHeader(Constants.REQUEST_HEADER)));
        }
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) {
        Storage.remove();
        PlatformStorage.remove();
    }
}
