package com.netmarch.volunteer.shiro;

import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;

import com.netmarch.volunteer.dao.UserMapper;
import com.netmarch.volunteer.domain.User;
import com.netmarch.volunteer.domain.dto.UserShiroDTO;
import com.netmarch.volunteer.mapper.UserDTOMapper;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.weekend.WeekendSqls;

/**
 * @author Mr.Wei on 2018/1/23.
 */
public class AuthRealm extends AuthorizingRealm {
    private UserMapper userMapper;

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        return null;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) {
        CaptchaUsernamePasswordToken token = (CaptchaUsernamePasswordToken) authenticationToken;
        Example example = new Example.Builder(User.class).where(WeekendSqls.<User> custom().andEqualTo(User::getUsername, token.getUsername())).build();
        User one = this.userMapper.selectOneByExample(example);
        UserShiroDTO user = UserDTOMapper.instance.userToUserShiroDTO(one);
        if (null == user) {
            throw new UnknownAccountException();
        }
        if (user.getActivated() == null || !user.getActivated()) {
            throw new LockedAccountException();
        }
        SimpleAuthenticationInfo authenticationInfo = new SimpleAuthenticationInfo(user, user.getPassword(), getName());
        authenticationInfo.setCredentialsSalt(ByteSource.Util.bytes(user.getSalt()));
        return authenticationInfo;
    }


    @Autowired
    public void setUserMapper(UserMapper userMapper) {
        this.userMapper = userMapper;
    }
}
