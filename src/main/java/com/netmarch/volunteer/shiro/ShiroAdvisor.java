package com.netmarch.volunteer.shiro;

import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

/**
 * @author Mr.Wei  2020/12/8 1:54 下午
 */
public class ShiroAdvisor extends AuthorizationAttributeSourceAdvisor {
    private static final Class<? extends Annotation>[] AUTHZ_ANNOTATION_CLASSES = new Class[]{JwtRoles.class};

    public ShiroAdvisor(StringRedisTemplate redisTemplate) {
        super.setAdvice(new ShiroAopInterceptor(redisTemplate));
    }

    @SuppressWarnings({"unchecked"})
    @Override
    public boolean matches(Method method, Class targetClass) {
        Method m = method;
        if (targetClass != null) {
            try {
                m = targetClass.getMethod(m.getName(), m.getParameterTypes());
                return this.isAuthAnnotationPresent(m) || this.isAuthAnnotationPresent(targetClass);
            } catch (NoSuchMethodException ignored) {
                //default return value is false.  If we can't find the method, then obviously
                //there is no annotation, so just use the default return value.
            }
        }
        return super.matches(method, targetClass);
    }

    @SuppressWarnings({"unchecked"})
    private boolean isAuthAnnotationPresent(Class<?> targetClazz) {
        Class[] var2 = AUTHZ_ANNOTATION_CLASSES;
        int var3 = var2.length;

        for (int var4 = 0; var4 < var3; ++var4) {
            Class<? extends Annotation> annClass = var2[var4];
            Annotation a = AnnotationUtils.findAnnotation(targetClazz, annClass);
            if (a != null) {
                return true;
            }
        }

        return false;
    }

    @SuppressWarnings({"unchecked"})
    private boolean isAuthAnnotationPresent(Method method) {
        Class[] var2 = AUTHZ_ANNOTATION_CLASSES;
        int var3 = var2.length;

        for (int var4 = 0; var4 < var3; ++var4) {
            Class<? extends Annotation> annClass = var2[var4];
            Annotation a = AnnotationUtils.findAnnotation(method, annClass);
            if (a != null) {
                return true;
            }
        }

        return false;
    }
}
