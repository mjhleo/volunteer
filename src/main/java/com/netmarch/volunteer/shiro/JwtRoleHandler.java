package com.netmarch.volunteer.shiro;

import java.lang.annotation.Annotation;

import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.aop.AuthorizingAnnotationHandler;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.netmarch.volunteer.config.Constants;
import com.netmarch.volunteer.domain.dto.TokenDTO;
import com.netmarch.volunteer.exception.ApiException;
import com.netmarch.volunteer.interceptor.thead.Storage;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Mr.Wei  2020/12/8 1:48 下午
 */
@Slf4j
@Component
public class JwtRoleHandler extends AuthorizingAnnotationHandler {
    private StringRedisTemplate redisTemplate;

    public JwtRoleHandler(StringRedisTemplate redisTemplate) {
        super(JwtRoles.class);
        this.redisTemplate = redisTemplate;
    }

    protected String[] getAnnotationValue(Annotation a) {
        JwtRoles rpAnnotation = (JwtRoles) a;
        return rpAnnotation.value();
    }

    @Override
    public void assertAuthorized(Annotation a) {
        if (a instanceof JwtRoles) {
            JwtRoles rpAnnotation = (JwtRoles) a;
            String[] perms = this.getAnnotationValue(a);
            TokenDTO token = JSON.parseObject(this.redisTemplate.opsForValue().get(
                    Constants.REDIS_PC_TOKEN_KEY + Storage.getOperator()), TokenDTO.class);
            if (null == token) {
                throw new AuthorizationException();
            }
            if (perms.length == 1) {
                if (!token.getRole().equals(perms[0])) {
                    throw new ApiException(ApiException.E_USER_PERMISSION_DENIED);
                }
            } else {
                this.checkLeastOneRole(rpAnnotation, perms, token);
            }
        }
    }

    private void checkLeastOneRole(JwtRoles jwtRoles, String[] perms, TokenDTO token) {
        if (Logical.OR.equals(jwtRoles.logical())) {
            boolean hasAtLeastOneRole = false;
            for (String perm : perms) {
                if (token.getRole().equals(perm)) {
                    hasAtLeastOneRole = true;
                    break;
                }
            }
            if (!hasAtLeastOneRole) {
                throw new ApiException(ApiException.E_USER_PERMISSION_DENIED);
            }
        }
    }
}
