package com.netmarch.volunteer.shiro;

import javax.annotation.Resource;

import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;

import com.netmarch.volunteer.dao.UserVolunteerMapper;
import com.netmarch.volunteer.domain.UserVolunteer;
import com.netmarch.volunteer.domain.dto.UserShiroDTO;
import com.netmarch.volunteer.mapper.UserDTOMapper;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.weekend.WeekendSqls;

/**
 * @author huangguochen
 * @create 2022/5/6 14:03
 */

public class VolunteerRealm extends AuthorizingRealm {

    @Resource
    private UserVolunteerMapper userVolunteerMapper;

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        return null;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) {
        CaptchaUsernamePasswordToken token = (CaptchaUsernamePasswordToken) authenticationToken;
        Example example = new Example.Builder(UserVolunteer.class)
                .where(WeekendSqls.<UserVolunteer> custom().andEqualTo(UserVolunteer::getUsername, token.getUsername())).build();
        UserVolunteer one = userVolunteerMapper.selectOneByExample(example);
        if (null == one) {
            throw new UnknownAccountException();
        }
        UserShiroDTO user = UserDTOMapper.instance.userVolunteerToUserShiroDTO(one);
        SimpleAuthenticationInfo authenticationInfo = new SimpleAuthenticationInfo(user, user.getUserPassword(), getName());
        authenticationInfo.setCredentialsSalt(ByteSource.Util.bytes(user.getUserSalt()));
        return authenticationInfo;
    }
}
