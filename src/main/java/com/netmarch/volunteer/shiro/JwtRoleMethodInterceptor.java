package com.netmarch.volunteer.shiro;

import org.apache.shiro.aop.AnnotationResolver;
import org.apache.shiro.aop.MethodInvocation;
import org.apache.shiro.authz.aop.AuthorizingAnnotationMethodInterceptor;
import org.springframework.data.redis.core.StringRedisTemplate;

/**
 * @author Mr.Wei  2020/12/8 1:50 下午
 */
public class JwtRoleMethodInterceptor extends AuthorizingAnnotationMethodInterceptor {
    public JwtRoleMethodInterceptor(AnnotationResolver resolver, StringRedisTemplate redisTemplate) {
        super(new JwtRoleHandler(redisTemplate), resolver);
    }

    @Override
    public void assertAuthorized(MethodInvocation mi) {
        ((JwtRoleHandler) getHandler()).assertAuthorized(getAnnotation(mi));
    }
}
