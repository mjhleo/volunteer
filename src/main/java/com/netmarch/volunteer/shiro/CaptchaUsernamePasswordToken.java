package com.netmarch.volunteer.shiro;

import org.apache.shiro.authc.UsernamePasswordToken;

/**
 * @author huangguochen
 * @create 2022/5/5 11:13
 */

public class CaptchaUsernamePasswordToken extends UsernamePasswordToken {

    private String captcha;

    private String loginType;


    public CaptchaUsernamePasswordToken(String username, char[] password, String loginType) {
        super(username,password);
        this.loginType = loginType;
    }

    public CaptchaUsernamePasswordToken(String username, char[] password, String captcha, String loginType) {
        super(username,password);
        this.captcha = captcha;
        this.loginType = loginType;
    }


    public String getLoginType() {
        return loginType;
    }

    public void setLoginType(String loginType) {
        this.loginType = loginType;
    }

    public String getCaptcha() {
        return captcha;
    }

    public void setCaptcha(String captcha) {
        this.captcha = captcha;
    }
}
