package com.netmarch.volunteer.shiro;

import org.apache.shiro.spring.aop.SpringAnnotationResolver;
import org.apache.shiro.spring.security.interceptor.AopAllianceAnnotationsAuthorizingMethodInterceptor;
import org.springframework.data.redis.core.StringRedisTemplate;

/**
 * @author Mr.Wei  2020/12/8 1:52 下午
 */
public class ShiroAopInterceptor extends AopAllianceAnnotationsAuthorizingMethodInterceptor {
    public ShiroAopInterceptor(StringRedisTemplate redisTemplate) {
        super();
        this.methodInterceptors.add(new JwtRoleMethodInterceptor(new SpringAnnotationResolver(), redisTemplate));
    }
}
