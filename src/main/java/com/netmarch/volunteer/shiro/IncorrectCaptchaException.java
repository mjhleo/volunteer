package com.netmarch.volunteer.shiro;

import org.apache.shiro.authc.AuthenticationException;

/**
 * @author huangguochen
 * @create 2022/5/5 11:25
 */

public class IncorrectCaptchaException extends AuthenticationException {
    private static final long serivalVersionUID = 1L;

    public IncorrectCaptchaException() {
        super();
    }

    public IncorrectCaptchaException(String message, Throwable cause) {
        super(message, cause);
    }

    public IncorrectCaptchaException(String message) {
        super(message);
    }

    public IncorrectCaptchaException(Throwable cause) {
        super(cause);
    }
}
