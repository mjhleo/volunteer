package com.netmarch.volunteer.shiro;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.web.filter.authc.BasicHttpAuthenticationFilter;
import org.springframework.stereotype.Component;

import com.netmarch.volunteer.config.ApplicationProperties;
import com.netmarch.volunteer.service.UserService;

/**
 * @author Mr.Wei on 2018/4/9.
 */
@Component
public class TokenFilter extends BasicHttpAuthenticationFilter { // NOSONAR squid:S110
    private static final String HEADER_OPTIONS = "OPTIONS";
    private UserService userService;
    private ApplicationProperties applicationProperties;

    @Override
    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) {
        if (HEADER_OPTIONS.equals(((HttpServletRequest) request).getMethod())) {
            return true;
        }
        return this.userService.verifyToken(this.getAuthzHeader(request), this.applicationProperties.getJwtSecret());
    }

    public TokenFilter(UserService userService, ApplicationProperties applicationProperties) {
        this.userService = userService;
        this.applicationProperties = applicationProperties;
    }
}
