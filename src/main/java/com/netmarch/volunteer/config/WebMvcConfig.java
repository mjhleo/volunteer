package com.netmarch.volunteer.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * <配置映射文件路径>
 *
 * @author Huangguochen
 * @create 2020/10/21 13:42
 */
@Slf4j
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

    private ApplicationProperties applicationProperties;

    @Autowired
    public WebMvcConfig(ApplicationProperties applicationProperties) {
        this.applicationProperties = applicationProperties;
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        String fileAddress = applicationProperties.getUploadFileAddress();
        fileAddress = fileAddress.replaceAll("\\\\\\\\","/");
        if (!fileAddress.startsWith("file:")) {
            fileAddress = "file:" + fileAddress;
        }
        registry.addResourceHandler("/file/**").addResourceLocations(fileAddress);
        registry.addResourceHandler("/excel/**").addResourceLocations("classpath:/excel/");
        log.info("文件资源存放地址：" + fileAddress);
    }
}
