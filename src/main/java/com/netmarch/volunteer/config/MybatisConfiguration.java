package com.netmarch.volunteer.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.netmarch.volunteer.interceptor.UpdateInterceptor;

/**
 * @author Mr.Wei on 2018/4/17.
 */
@Configuration
public class MybatisConfiguration {
    @Bean
    public UpdateInterceptor updateInterceptor() {
        return new UpdateInterceptor();
    }
}
