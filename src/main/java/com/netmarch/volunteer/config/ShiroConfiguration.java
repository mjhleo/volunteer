package com.netmarch.volunteer.config;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.Filter;

import org.apache.shiro.authc.credential.CredentialsMatcher;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.authc.pam.AtLeastOneSuccessfulStrategy;
import org.apache.shiro.authc.pam.ModularRealmAuthenticator;
import org.apache.shiro.mgt.DefaultSessionStorageEvaluator;
import org.apache.shiro.mgt.DefaultSubjectDAO;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.StringRedisTemplate;

import com.netmarch.volunteer.service.UserService;
import com.netmarch.volunteer.shiro.AppRealm;
import com.netmarch.volunteer.shiro.AuthRealm;
import com.netmarch.volunteer.shiro.CustomizedModularRealmAuthenticator;
import com.netmarch.volunteer.shiro.ShiroAdvisor;
import com.netmarch.volunteer.shiro.TokenFilter;
import com.netmarch.volunteer.shiro.VolunteerRealm;



/**
 * @author Mr.Wei on 2018/1/19.
 */
@Configuration
public class ShiroConfiguration {
    private UserService userService;
    private ApplicationProperties applicationProperties;
    private StringRedisTemplate redisTemplate;

    /**
     * 配置过滤
     */
    @Bean
    public ShiroFilterFactoryBean shiroFilter(org.apache.shiro.mgt.SecurityManager manager) {
        ShiroFilterFactoryBean bean = new ShiroFilterFactoryBean();
        Map<String, Filter> filterMap = bean.getFilters();
        filterMap.put("jwt", new TokenFilter(this.userService, this.applicationProperties));
        bean.setFilters(filterMap);
        bean.setSecurityManager(manager);
        LinkedHashMap<String, String> filterChainDefinitionMap = new LinkedHashMap<>();
        filterChainDefinitionMap.put("/doc.html", "anon");
        filterChainDefinitionMap.put("/swagger-resources/**", "anon");
        filterChainDefinitionMap.put("/v2/api-docs", "anon");
        filterChainDefinitionMap.put("/webjars/**", "anon");
        filterChainDefinitionMap.put("/api/volunteers/**", "anon");
        filterChainDefinitionMap.put("/api/wx/login", "anon");
        filterChainDefinitionMap.put("/api/users/login", "anon");
        filterChainDefinitionMap.put("/file/**", "anon");
        filterChainDefinitionMap.put("/excel/**", "anon");
        filterChainDefinitionMap.put("/instances/**", "anon");
        filterChainDefinitionMap.put("/actuator/**", "anon");
        filterChainDefinitionMap.put("/api/app/**", "anon");
        filterChainDefinitionMap.put("/**", "jwt");
        bean.setFilterChainDefinitionMap(filterChainDefinitionMap);
        return bean;
    }

    /**
     * 配置核心安全事务管理器
     */
    @Bean
    public org.apache.shiro.mgt.SecurityManager securityManager(@Qualifier("authRealm") AuthRealm authRealm, @Qualifier("appRealm") AppRealm appRealm, @Qualifier("volunteerRealm") VolunteerRealm volunteerRealm) {
        DefaultWebSecurityManager manager = new DefaultWebSecurityManager();
        manager.setAuthenticator(modularRealmAuthenticator());
        manager.setRealms(Arrays.asList(authRealm,appRealm, volunteerRealm));
        DefaultSubjectDAO subjectDAO = new DefaultSubjectDAO();
        DefaultSessionStorageEvaluator defaultSessionStorageEvaluator = new DefaultSessionStorageEvaluator();
        defaultSessionStorageEvaluator.setSessionStorageEnabled(false);
        subjectDAO.setSessionStorageEvaluator(defaultSessionStorageEvaluator);
        manager.setSubjectDAO(subjectDAO);
        return manager;
    }

    /**
     * 身份认证
     */
    @Bean
    public AuthRealm authRealm(@Qualifier("hashedCredentialsMatcher") CredentialsMatcher matcher) {
        AuthRealm authRealm = new AuthRealm();
        authRealm.setCredentialsMatcher(matcher);
        return authRealm;
    }

    /**
     * 微信身份认证
     */
    @Bean
    public AppRealm appRealm(@Qualifier("hashedCredentialsMatcher") CredentialsMatcher matcher) {
        AppRealm appRealm = new AppRealm();
        appRealm.setCredentialsMatcher(matcher);
        return appRealm;
    }

    @Bean
    public VolunteerRealm volunteerRealm(@Qualifier("hashedCredentialsMatcher") CredentialsMatcher matcher) {
        VolunteerRealm volunteerRealm = new VolunteerRealm();
        volunteerRealm.setCredentialsMatcher(matcher);
        return volunteerRealm;
    }

    /**
     * 系统自带的Realm管理，主要针对多realm
     * */
    @Bean
    public ModularRealmAuthenticator modularRealmAuthenticator(){
        //自己重写的ModularRealmAuthenticator
        CustomizedModularRealmAuthenticator modularRealmAuthenticator = new CustomizedModularRealmAuthenticator();
        modularRealmAuthenticator.setAuthenticationStrategy(new AtLeastOneSuccessfulStrategy());
        return modularRealmAuthenticator;
    }


    /**
     * 密码匹配器
     */
    @Bean
    public HashedCredentialsMatcher hashedCredentialsMatcher() {
        HashedCredentialsMatcher hashedCredentialsMatcher = new HashedCredentialsMatcher();
        hashedCredentialsMatcher.setHashAlgorithmName("md5");
        hashedCredentialsMatcher.setHashIterations(2);
        return hashedCredentialsMatcher;
    }

    //@Bean
    public AuthorizationAttributeSourceAdvisor getAuthorizationAttributeSourceAdvisor(org.apache.shiro.mgt.SecurityManager securityManager) {
        AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor = new ShiroAdvisor(redisTemplate);
        authorizationAttributeSourceAdvisor.setSecurityManager(securityManager);
        return authorizationAttributeSourceAdvisor;
    }

    @Autowired
    public ShiroConfiguration(UserService userService, ApplicationProperties applicationProperties, StringRedisTemplate redisTemplate) {
        this.userService = userService;
        this.applicationProperties = applicationProperties;
        this.redisTemplate = redisTemplate;
    }
}
