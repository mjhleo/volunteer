package com.netmarch.volunteer.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

/**
 * Properties specific to Maintain.
 * <p>
 * Properties are configured in the {@code application.yml} file.
 * See {@link io.github.jhipster.config.JHipsterProperties} for a good example.
 */
@Data
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class ApplicationProperties {
    private String jwtSecret;
    private Integer tokenExpire;
    private String uploadFileAddress;
    private Integer uploadFileMaxSize;
    private String apkDownloadAddress;
    private String filePrefix;
    private String orcAuthHost;
    private String orcAk;
    private String orcSk;
    private String orcHost;
    private String mapKey;
}
