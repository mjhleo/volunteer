package com.netmarch.volunteer.config;

/**
 * Application constants.
 */
public final class Constants {

    // Regex for acceptable logins
    public static final String LOGIN_REGEX = "^(?>[a-zA-Z0-9!$&*+=?^_`{|}~.-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*)|(?>[_.@A-Za-z0-9-]+)$";
    public static final String PHONE_REGEX = "^1([38][0-9]|4[579]|5[0-35-9]|6[6]|7[0135678]|9[89])\\d{8}$";

    public static final String SYSTEM_ACCOUNT = "system";
    public static final String DEFAULT_LANGUAGE = "en";
    public static final String ANONYMOUS_USER = "anonymoususer";

    public static final String REDIS_PC_TOKEN_KEY = "pc-token:";
    public static final String REDIS_APP_TOKEN_KEY = "app-token:";
    public static final String REDIS_VOLUNTEER_WECHAT_TOKEN_KEY = "volunteer-wechat-token:";
    public static final String REDIS_VOLUNTEER_PC_TOKEN_KEY = "volunteer-pc-token:";

    public static final String WEBSOCKET_URI = "/websocket";
    public static final String SOCKET_URI = "/socket";

    public static final String REDIS_VM_KEY = "VM:";
    public static final String REDIS_S_VM_KEY = "S_VM:";
    public static final String REDIS_WS_VM_KEY = "WS_VM:";


    public static final String DEFAULT_ZERO = "000000";

    public static final String REQUEST_HEADER = "Authorization";

    public static final String SPRING_PROFILE_DEVELOPMENT = "dev";
    public static final String SPRING_PROFILE_RELEASE = "release";
    public static final String SPRING_PROFILE_PRODUCTION = "prod";

    /** 【资源申请】初次提交 **/
    public static final String SMS_TEMPLATE_ID_ASSET_FIRST_APPLY_CHECK = "02db0235ffe249abaecbec447353c351";
    /** 【资源申请】上传复审材料后 **/
    public static final String SMS_TEMPLATE_ID_ASSET_RE_APPLY = "9abd90e25336401497467ac61ec5608c";
    /** 【运维知识库】提交审核后 **/
    public static final String SMS_TEMPLATE_ID_REPOSITORY_TO_APPLY = "789fbae9f7d34a0cbf70698822697e33";

    public static final String CONSTANT_MONTH_FORMAT = "yyyyMM";
    public static final String CONSTANT_DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String CONSTANT_DATE_FORMAT = "yyyy-MM-dd";
    public static final String CONSTANT_IME_FORMAT = "HH:mm:ss";

    public static final String WEBSOCKET_CHAT_URI = "/ws";
    public static final String WEBSOCKET_MESSAGE_FORMAT = "[%s][%s][%s][%s]%s";

    public static final String REDIS_WEBSOCKET_CHANNEL_KEY = "websocket_channel:";
    public static final String REDIS_WEBSOCKET_USER_KEY = "websocket_channel_user:";
    public static final String IMAGE_BASE64_HEAD = "data:image/png;base64,";

    public static final String ROLE_ADMIN = "ADMIN";
    public static final String ROLE_LEADER = "LEADER";
    public static final String ROLE_USER = "USER";
    public static final int BASE_WORK_LENGTH = 8;
    public static final String FLTER_JWT_WARD = "jwt,ward";

    public static final int PATROL_WARNING_TIME = 10;

    public static final String URL = "https://api.weixin.qq.com/sns/jscode2session";
    public static final String APPID = "";
    public static final String SECRET = "";
    public static final String GRANT_TYPE= "authorization_code";

    public static final int NOT_PASS=0;

    public static final int PASS=1;

    public static final String UNCHECK="2";

    private Constants() {
    }
}
