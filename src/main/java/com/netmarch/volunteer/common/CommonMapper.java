package com.netmarch.volunteer.common;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

/**
 * @author Mr.Wei on 2018/3/21.
 */
public interface CommonMapper<T> extends Mapper<T>,MySqlMapper<T> {
}
