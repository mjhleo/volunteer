package com.netmarch.volunteer.mapper;

import com.netmarch.volunteer.domain.Register;
import com.netmarch.volunteer.domain.dto.RegisterCheckDTO;
import com.netmarch.volunteer.domain.dto.RegisterDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface RegisterDTOMapper {
    RegisterDTOMapper instance = Mappers.getMapper(RegisterDTOMapper.class);

    Register registerDTOToRegister(RegisterDTO registerDTO);

    Register registerCheckDTOToRegister(RegisterCheckDTO registerCheckDTO);
}
