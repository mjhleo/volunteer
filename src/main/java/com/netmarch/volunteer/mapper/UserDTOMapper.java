package com.netmarch.volunteer.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.netmarch.volunteer.domain.User;
import com.netmarch.volunteer.domain.UserVolunteer;
import com.netmarch.volunteer.domain.dto.UserDTO;
import com.netmarch.volunteer.domain.dto.UserInfoDTO;
import com.netmarch.volunteer.domain.dto.UserShiroDTO;


/**
 * @author Huangguochen
 * @create 2020/10/9 14:45
 */

@Mapper
public interface UserDTOMapper {
    UserDTOMapper instance = Mappers.getMapper(UserDTOMapper.class);

    User userDTOToUser(UserDTO userDTO);

    UserShiroDTO userToUserShiroDTO(User user);


    UserShiroDTO userVolunteerToUserShiroDTO(UserVolunteer userVolunteer);


    UserInfoDTO userToUserInfoDTO(User user);
}
