package com.netmarch.volunteer.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.netmarch.volunteer.domain.Announcement;
import com.netmarch.volunteer.domain.dto.AnnouncementDTO;

/**
 * @author huangguochen
 * @create 2022/4/28 16:42
 */
@Mapper
public interface AnnouncementDTOMapper {
    AnnouncementDTOMapper instance = Mappers.getMapper(AnnouncementDTOMapper.class);
    Announcement dTO2Domain(AnnouncementDTO announcementDTO);
}
