package com.netmarch.volunteer.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.netmarch.volunteer.domain.Role;
import com.netmarch.volunteer.domain.dto.RoleDTO;



/**
 * @author zhuguanming
 * @date 2020/9/21
 */
@Mapper
public interface RoleDTOMapper {
    RoleDTOMapper INSTANCE = Mappers.getMapper(RoleDTOMapper.class);

    RoleDTO domain2DTO(Role role);

    Role dTO2Domain(RoleDTO roleDto);

    List<RoleDTO> domain2DTOList(List<Role> role);
}
