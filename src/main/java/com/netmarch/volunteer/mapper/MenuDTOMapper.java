package com.netmarch.volunteer.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.netmarch.volunteer.domain.Menu;
import com.netmarch.volunteer.domain.dto.MenuDTO;


/**
 * @author zhuguanming
 * @date 2020/9/21
 */
@Mapper
public interface MenuDTOMapper {
    MenuDTOMapper INSTANCE = Mappers.getMapper(MenuDTOMapper.class);

    MenuDTO domain2DTO(Menu menu);

    List<MenuDTO> domain2DTOList(List<Menu> menu);
}
