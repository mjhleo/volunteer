package com.netmarch.volunteer.mapper;


import com.netmarch.volunteer.domain.Dict;
import com.netmarch.volunteer.domain.dto.DictDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface DictDTOMapper {
    DictDTOMapper instance = Mappers.getMapper(DictDTOMapper.class);

    Dict dictDTOToDict(DictDTO dictDTO);


    DictDTO dictToDictDTO(Dict dict);

    List<DictDTO> dictsToDictDTOs(List<Dict> dicts);

}
