package com.netmarch.volunteer.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.netmarch.volunteer.domain.SystemMessage;
import com.netmarch.volunteer.domain.dto.SystemMessageDTO;

/**
 * @author huangguochen
 * @create 2022/4/28 13:52
 */
@Mapper
public interface SystemMessageDTOMapper {
    SystemMessageDTOMapper INSTANCE = Mappers.getMapper(SystemMessageDTOMapper.class);

    SystemMessageDTO domain2DTO(SystemMessage systemMessage);

    SystemMessage dto2Domain(SystemMessageDTO systemMessageDTO);

    List<SystemMessage> dto2DomainList(List<SystemMessageDTO> systemMessageDTOs);

}
