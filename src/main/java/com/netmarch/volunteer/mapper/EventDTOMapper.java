package com.netmarch.volunteer.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.netmarch.volunteer.domain.Event;
import com.netmarch.volunteer.domain.dto.EventDTO;

/**
 * @author huangguochen
 * @create 2022/4/27 22:18
 */
@Mapper
public interface EventDTOMapper {
    EventDTOMapper instance = Mappers.getMapper(EventDTOMapper.class);

    Event eventDTOToEvent(EventDTO eventDTO);

    EventDTO eventToEventDTO(Event event);

}
