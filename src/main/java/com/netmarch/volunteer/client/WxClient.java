package com.netmarch.volunteer.client;

import com.dtflys.forest.annotation.GetRequest;
import com.dtflys.forest.annotation.Query;
import com.dtflys.forest.annotation.Var;

/**
 * @author huangguochen
 * @create 2022/4/27 13:50
 */
public interface WxClient {

    @GetRequest("https://www.ks-ihealth.cn/medical/api/user-info/{token}")
    String getUserInfo(@Var("token") String token);


    @GetRequest("https://apis.map.qq.com/ws/geocoder/v1/")
    String getTencentaddress(@Query("location") String location, @Query("key") String key);
}
