package com.netmarch.volunteer.dao;


import java.util.Date;
import java.util.List;

import com.netmarch.volunteer.domain.command.MissionEventCommand;
import com.netmarch.volunteer.domain.dto.MissionCountDtoByEvent;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import com.netmarch.volunteer.common.CommonMapper;
import com.netmarch.volunteer.domain.Mission;
import com.netmarch.volunteer.domain.command.MissionCommand;
import com.netmarch.volunteer.domain.dto.MissionCountDTO;
import com.netmarch.volunteer.domain.dto.MissionQryDto;


@Mapper
@Component
public interface MissionMapper extends CommonMapper<Mission> {
    List<MissionQryDto> getList(MissionCommand missionCommand);

    List<MissionCountDTO> getCountByDept(@Param("deptId") Long deptId);

    List<MissionCountDTO> getCountByEventDate(@Param("startDate") Date startDate, @Param("endDate") Date endDate);


    List<MissionCountDtoByEvent> getMissionCountByEvent(MissionEventCommand eventId);
}
