package com.netmarch.volunteer.dao;

import com.netmarch.volunteer.domain.dto.AttendanceCountDTO;
import org.apache.ibatis.annotations.Mapper;

import com.netmarch.volunteer.common.CommonMapper;
import com.netmarch.volunteer.domain.Attendance;

import java.util.Date;
import java.util.List;

/**
 * @author huangguochen
 * @create 2022/4/28 21:50
 */
@Mapper
public interface AttendanceMapper extends CommonMapper<Attendance> {
    List<AttendanceCountDTO> getCountByDept(Long deptId);

    List<AttendanceCountDTO> getCountByEventDate(Date startDate, Date endDate);
}