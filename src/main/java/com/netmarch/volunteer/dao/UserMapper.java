package com.netmarch.volunteer.dao;

import org.springframework.stereotype.Component;

import com.netmarch.volunteer.common.CommonMapper;
import com.netmarch.volunteer.domain.User;

/**
 * @author Mr.Wei  2020/9/22 11:06 上午
 */
@Component
public interface UserMapper extends CommonMapper<User> {

    User selectByUserID(Long id);
}
