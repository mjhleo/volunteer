package com.netmarch.volunteer.dao;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import com.netmarch.volunteer.domain.command.OperationLogCommand;
import com.netmarch.volunteer.common.CommonMapper;
import com.netmarch.volunteer.domain.OperationLog;

/**
 * @author zhuguanming
 * @date 2020/12/8
 */
@Component
@Mapper
public interface OperationLogMapper extends CommonMapper<OperationLog> {

    @Select({
            "<script>",
            "   select ",
            "       operationLog.* ",
            "   FROM ",
            "       operation_log operationLog",
            "   <where> ",
            "     <if test='command.name != null and command.name != \"\"'> ",
            "       AND operationLog.name like concat('%',#{command.name},'%') ",
            "     </if> ",
            "     <if test='command.platform != null'> ",
            "       AND operationLog.platform = #{command.platform} ",
            "     </if> ",
            "     <if test='command.startDate != null and command.endDate != null'> ",
            "       AND operationLog.operation_date between #{command.startDate} and #{command.endDate} ",
            "     </if> ",
            "   </where> ",
            "</script>"
    })
    List<OperationLog> getOperationLogs(@Param("command") OperationLogCommand command);

    @Select({
            "<script>",
            "   delete ",
            "   FROM ",
            "       operation_log ",
            "   where operation_date &lt;= #{date}",
            "</script>"
    })
    void deleteLogExceed7Day(@Param("date") Date date);
}
