package com.netmarch.volunteer.dao;

import org.springframework.stereotype.Component;

import com.netmarch.volunteer.common.CommonMapper;
import com.netmarch.volunteer.domain.Announcement;

/**
 * @author huangguochen
 * @create 2022/4/28 16:31
 */
@Component
public interface AnnouncementMapper extends CommonMapper<Announcement> {

}