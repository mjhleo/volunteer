package com.netmarch.volunteer.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import com.netmarch.volunteer.domain.command.LoginLogCommand;
import com.netmarch.volunteer.common.CommonMapper;
import com.netmarch.volunteer.domain.LoginLog;

/**
 * @author zhuguanming
 * @date 2020/12/7
 */
@Component
@Mapper
public interface LoginLogMapper extends CommonMapper<LoginLog> {

    @Select({
            "<script>",
            "   select ",
            "       loginLog.* ",
            "   FROM ",
            "       login_log loginLog",
            "   <where> ",
            "     <if test='command.name != null and command.name != \"\"'> ",
            "       AND loginLog.name like concat('%',#{command.name},'%') ",
            "     </if> ",
            "     <if test='command.platform != null'> ",
            "       AND loginLog.platform = #{command.platform} ",
            "     </if> ",
            "     <if test='command.startDate != null and command.endDate != null'> ",
            "       AND loginLog.login_date between #{command.startDate} and #{command.endDate} ",
            "     </if> ",
            "   </where> ",
            "</script>"
    })
    List<LoginLog> getLoginLogs(@Param("command") LoginLogCommand loginLogCommand);
}
