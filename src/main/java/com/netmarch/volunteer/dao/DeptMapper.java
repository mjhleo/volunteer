package com.netmarch.volunteer.dao;


import com.netmarch.volunteer.common.CommonMapper;
import com.netmarch.volunteer.domain.Dept;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;


@Mapper
@Component
public interface DeptMapper extends CommonMapper<Dept> {
}
