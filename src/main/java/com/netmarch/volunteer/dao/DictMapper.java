package com.netmarch.volunteer.dao;


import com.netmarch.volunteer.common.CommonMapper;
import com.netmarch.volunteer.domain.Dict;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author Mr.Wei  2021/6/11 8:59 上午
 */
@Mapper
@Component
public interface DictMapper extends CommonMapper<Dict> {

    List<Dict> findByType(String type);
}
