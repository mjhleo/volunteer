package com.netmarch.volunteer.dao;

import com.netmarch.volunteer.domain.dto.RegisterCheckDTO;
import com.netmarch.volunteer.domain.dto.RegisterTrainCheckDTO;
import com.netmarch.volunteer.domain.dto.RegisterTrainImportDTO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import com.netmarch.volunteer.common.CommonMapper;
import com.netmarch.volunteer.domain.Register;

import java.util.List;

@Component
public interface RegisterMapper extends CommonMapper<Register> {
    @Select("select check_state from register where idNumber=#{idNumber}")
    Integer getcheckState(@Param("idNumber") String idNumber);

    int batchUpdateCheckState(@Param("ids") List<Long> ids, @Param("registerCheckDTO")RegisterCheckDTO registerCheckDTO);

    int batchUpdateTrainCheckState(@Param("ids")List<Long> ids, @Param("registerTrainCheckDTO")RegisterTrainCheckDTO registerTrainCheckDTO);

    List<Register> selectRegisterListByIds(@Param("ids")List<Long> ids);

    int batchUpdateCheckResult(@Param("list") List<RegisterTrainImportDTO> list);

    int batchTrainResult(@Param("ids")List<Long> ids, @Param("registerTrainCheckDTO")RegisterTrainCheckDTO registerTrainCheckDTO);
}
