package com.netmarch.volunteer.dao;

import org.springframework.stereotype.Component;

import com.netmarch.volunteer.common.CommonMapper;
import com.netmarch.volunteer.domain.SystemMessage;

/**
 * @author huangguochen
 * @create 2022/4/28 13:11
 */
@Component
public interface SystemMessageMapper extends CommonMapper<SystemMessage> {
}