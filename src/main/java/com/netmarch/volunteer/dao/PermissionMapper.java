package com.netmarch.volunteer.dao;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import com.netmarch.volunteer.common.CommonMapper;
import com.netmarch.volunteer.domain.Permission;


@Mapper
@Component
public interface PermissionMapper extends CommonMapper<Permission> {
}
