package com.netmarch.volunteer.dao;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import com.netmarch.volunteer.common.CommonMapper;
import com.netmarch.volunteer.domain.Role;

/**
 * @author Mr.Wei  2021/4/7 1:48 下午
 */
@Component
@Mapper
public interface RoleMapper extends CommonMapper<Role> {
}
