package com.netmarch.volunteer.dao;

import org.springframework.stereotype.Component;

import com.netmarch.volunteer.common.CommonMapper;
import com.netmarch.volunteer.domain.EventDept;

/**
 * @author huangguochen
 * @create 2022/4/28 19:14
 */
@Component
public interface EventDeptMapper extends CommonMapper<EventDept> {
}