package com.netmarch.volunteer.dao;

import com.netmarch.volunteer.common.CommonMapper;
import com.netmarch.volunteer.domain.Personnel;
import com.netmarch.volunteer.domain.dto.PersonnelDTO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface PersonnelMapper extends CommonMapper<Personnel> {

    int batchExitJob(@Param("ids")List<Long> ids, @Param("personnelDTO")PersonnelDTO personnelDTO);

    int batchOnJob(@Param("ids")List<Long> ids, @Param("personnelDTO")PersonnelDTO personnelDTO);

    int arrange(@Param("ids")List<Long> ids, @Param("personnelDTO")PersonnelDTO personnelDTO);
}
