package com.netmarch.volunteer.dao;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import com.netmarch.volunteer.common.CommonMapper;
import com.netmarch.volunteer.domain.EntityLog;

@Mapper
@Component
public interface EntityLogMapper extends CommonMapper<EntityLog> {
    int deleteLogExceed7Day();
}
