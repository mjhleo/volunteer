package com.netmarch.volunteer.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import com.netmarch.volunteer.common.CommonMapper;
import com.netmarch.volunteer.domain.Menu;
import com.netmarch.volunteer.domain.dto.PermissionDTO;


/**
 * @author zhuguanming
 * @date 2020/10/12
 */
@Component
@Mapper
public interface MenuMapper extends CommonMapper<Menu> {

    /**
     * 获取菜单集合
     */
    List<Menu> selectMenus(@Param("roleId") Long roleId);

    /**
     * 获取当前角色已选中的菜单叶子节点Id集合
     */
    List<PermissionDTO> selectLeafMenus(@Param("roleId") Long roleId);


    List<Menu> selectVolunteerMenus();
}
