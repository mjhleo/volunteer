package com.netmarch.volunteer.dao;

import org.springframework.stereotype.Component;

import com.netmarch.volunteer.common.CommonMapper;
import com.netmarch.volunteer.domain.UserVolunteer;

/**
 * @author huangguochen
 * @create 2022/4/27 16:45
 */
@Component
public interface UserVolunteerMapper extends CommonMapper<UserVolunteer> {

}