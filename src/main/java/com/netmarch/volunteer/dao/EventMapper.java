package com.netmarch.volunteer.dao;

import org.springframework.stereotype.Component;

import com.netmarch.volunteer.common.CommonMapper;
import com.netmarch.volunteer.domain.Event;

/**
 * @author huangguochen
 * @create 2022/4/27 22:04
 */
@Component
public interface EventMapper extends CommonMapper<Event> {
}